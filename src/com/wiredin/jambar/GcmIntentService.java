 package com.wiredin.jambar;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

public class GcmIntentService extends IntentService {
	public static final int NOTIFICATION_ID = 1;
	//private NotificationManager mNotificationManager;
	NotificationCompat.Builder builder;

	public GcmIntentService() {
		super("GcmIntentService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Bundle extras = intent.getExtras();
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
		// The getMessageType() intent parameter must be the intent you received
		// in your BroadcastReceiver.
		String messageType = gcm.getMessageType(intent);

		if (!extras.isEmpty()) { // has effect of unparcelling Bundle
			/*
			 * Filter messages based on message type. Since it is likely that
			 * GCM will be extended in the future with new message types, just
			 * ignore any message types you're not interested in, or that you
			 * don't recognize.
			 */
			if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR
					.equals(messageType)) {
				sendNotification("Send error: " + extras.toString());
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED
					.equals(messageType)) {
				sendNotification("Deleted messages on server: "
						+ extras.toString());
				// If it's a regular GCM message, do some work.
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE
					.equals(messageType)) {

				Log.i("GCM",
						"Completed work @ " + SystemClock.elapsedRealtime());
				
				if (extras.getString("message").equals("Close All")){
					Intent homeIntent = new Intent(Intent.ACTION_MAIN);
					homeIntent.addCategory( Intent.CATEGORY_HOME );
					homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//					homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(homeIntent);
				}else{
					sendNotification(extras.getString("message"));
				}
				
				// Post notification of received message.
				
				Log.i("GCM", "Received: " + extras.toString());
			}
		}
		// Release the wake lock provided by the WakefulBroadcastReceiver.
		GcmBroadcastReceiver.completeWakefulIntent(intent);
	}

	// Put the message into a notification and post it.
	// This is just one simple example of what you might choose to do with
	// a GCM message.

	private void sendNotification(String msg) {
		int icon = R.drawable.ic_launcher;
		//long when = System.currentTimeMillis();
		String title = this.getString(R.string.app_name);
		
		NotificationManager notificationManager = (NotificationManager) this
				.getSystemService(Context.NOTIFICATION_SERVICE);
		// set intent so it does not start a new activity
		Intent notificationIntent = new Intent(this, SplashScreen.class);
		PendingIntent intent = PendingIntent.getActivity(this, 0,
				notificationIntent, 0);
		
		android.support.v4.app.NotificationCompat.Builder notification = new android.support.v4.app.NotificationCompat.Builder(this)
		.setSmallIcon(icon)
        .setContentTitle(title)
        .setContentIntent(intent)
        .setContentText(msg)
        .setDefaults(Notification.DEFAULT_ALL)
        .setStyle(new android.support.v4.app.NotificationCompat.BigTextStyle()
                .bigText(msg));
		
		Notification notify = notification.build();
		
		notificationManager.notify(01, notify);
		
		Intent notiActivity = new Intent(this, NotifyActivity.class);
		notiActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		notiActivity.putExtra("msg", msg);
		startActivity(notiActivity);
	}
}
