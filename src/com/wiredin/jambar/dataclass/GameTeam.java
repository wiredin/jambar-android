package com.wiredin.jambar.dataclass;

public class GameTeam {
	private int _id;
	private String name;
	private String code;

	public GameTeam() {
		
	}
	
	public GameTeam(int id, String name, String code) {
		this._id = id;
		this.name = name;
		this.code = code;
	}
	
	public GameTeam(String name, String code) {
		this.name = name;
		this.code = code;
	}
	
	public void setId(int id) {
		this._id = id;
	}
	public void setName(String text){
		this.name = text;
	}
	public void setCode(String text){
		this.code = text;
	}
	
	
	public int getId() {
		return this._id;
	}
	public String getName(){
		return this.name;
	}
	public String getCode(){
		return this.code;
	}


}