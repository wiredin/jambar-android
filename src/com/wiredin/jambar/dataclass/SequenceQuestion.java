package com.wiredin.jambar.dataclass;

public class SequenceQuestion {
	private int _id;
	private String questionid;
	private String title;
	private String text;
	private String type;
	private String ans1;
	private String ans2;
	private String ans3;
	private String ans4;

	public SequenceQuestion() {
		
	}
	
	public SequenceQuestion(int id, String questionid, String title, String text, String type,
			String ans1, String ans2, String ans3, String ans4) {
		this._id = id;
		this.questionid = questionid;
		this.title = title;
		this.text = text;
		this.type = type;
		this.ans1 = ans1;
		this.ans2 = ans2;
		this.ans3 = ans3;
		this.ans4 = ans4;
	}
	
	public SequenceQuestion(String questionid, String title, String text, String type,
			String ans1, String ans2, String ans3, String ans4) {
		this.questionid = questionid;
		this.title = title;
		this.text = text;
		this.type = type;
		this.ans1 = ans1;
		this.ans2 = ans2;
		this.ans3 = ans3;
		this.ans4 = ans4;
	}
	
	public void setId(int id) {
		this._id = id;
	}
	public void setQuestionid(String text){
		this.questionid = text;
	}
	public void setTitle(String text){
		this.title = text;
	}
	public void setText(String text){
		this.text = text;
	}
	public void setType(String text){
		this.type = text;
	}
	public void setAns1(String text) {
		this.ans1 = text;
	}
	public void setAns2(String text){
		this.ans2 = text;
	}
	public void setAns3(String text){
		this.ans3 = text;
	}
	public void setAns4(String text){
		this.ans4 = text;
	}
	
	
	public int getId(){
		return this._id;
	}
	public String getQuestionid(){
		return this.questionid;
	}
	public String getTitle(){
		return this.title;
	}
	public String getText(){
		return this.text;
	}
	public String getType(){
		return this.type;
	}
	public String getAns1() {
		return this.ans1;
	}
	public String getAns2(){
		return this.ans2;
	}
	public String getAns3(){
		return this.ans3;
	}
	public String getAns4(){
		return this.ans4;
	}

}