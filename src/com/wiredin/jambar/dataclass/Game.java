package com.wiredin.jambar.dataclass;

public class Game {
	private int _id;
	private String gameid;
	private String title;
	private String desc;
	private String total;
	private String time;

	public Game() {
		
	}
	
	public Game(int id, String gameid, String title, String desc, 
			String time,String total) {
		this._id = id;
		this.gameid = gameid;
		this.title = title;
		this.desc = desc;
		this.total = total;
		this.time = time;
	}
	
	public Game(String gameid, String title, String desc, 
			String total, String time) {
		this.gameid = gameid;
		this.title = title;
		this.desc = desc;
		this.total = total;
		this.time = time;
	}
	
	public void setId(int id) {
		this._id = id;
	}
	public void setGameid(String text){
		this.gameid = text;
	}
	public void setTitle(String text){
		this.title = text;
	}
	public void setDesc(String text){
		this.desc = text;
	}
	public void setTotal(String text){
		this.total = text;
	}
	public void setTime(String text){
		this.time = text;
	}
	
	
	public int getId() {
		return this._id;
	}
	public String getGameid(){
		return this.gameid;
	}
	public String getTitle(){
		return this.title;
	}
	public String getDesc(){
		return this.desc;
	}
	public String getTotal(){
		return this.total;
	}
	public String getTime(){
		return this.time;
	}

}