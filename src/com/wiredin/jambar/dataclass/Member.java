package com.wiredin.jambar.dataclass;

public class Member {
	private int _id;
	private String memberid;
	private String name;
	private String email;

	public Member() {
		
	}
	
	public Member(int id, String memberid, String name, String email) {
		this._id = id;
		this.memberid = memberid;
		this.name = name;
		this.email = email;
	}
	
	public Member(String memberid, String name, String email) {
		this.memberid = memberid;
		this.name = name;
		this.email = email;
	}
	
	public void setId(int id) {
		this._id = id;
	}
	public void setMemberid(String text){
		this.memberid = text;
	}
	public void setFullname(String text){
		this.name = text;
	}
	public void setEmail(String text){
		this.email = text;
	}
	
	
	public int getId(){
		return this._id;
	}
	public String getMemberid(){
		return this.memberid;
	}
	public String getName(){
		return this.name;
	}
	public String getEmail(){
		return this.email;
	}

}