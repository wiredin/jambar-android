package com.wiredin.jambar.dataclass;

public class Answer {
	private int _id;
	private String questionid;
	private String answer;
	private String correct;
	private String start;
	private String stop;

	public Answer() {
		
	}
	
	public Answer(int id, String questionid, String answer, String correct, String start, String stop) {
		this._id = id;
		this.questionid = questionid;
		this.answer = answer;
		this.correct = correct;
		this.start = start;
		this.stop = stop;
	}
	
	public Answer(String questionid, String answer, String correct, String start, String stop) {
		this.questionid = questionid;
		this.answer = answer;
		this.correct = correct;
		this.start = start;
		this.stop = stop;
	}
	
	
	public void setId(int id) {
		this._id = id;
	}
	public void setQuestionid(String text){
		this.questionid = text;
	}
	public void setAnswer(String text){
		this.answer = text;
	}
	public void setCorrect(String text){
		this.correct = text;
	}
	public void setStart(String text){
		this.start = text;
	}
	public void setStop(String text){
		this.stop = text;
	}
	
	
	public int getId(){
		return this._id;
	}
	public String getQuestionid(){
		return this.questionid;
	}
	public String getAnswer(){
		return this.answer;
	}
	public String getCorrect(){
		return this.correct;
	}
	public String getStart(String text){
		return this.start;
	}
	public String getStop(String text){
		return this.stop;
	}

}