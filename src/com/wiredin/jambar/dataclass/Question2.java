package com.wiredin.jambar.dataclass;

public class Question2 {
	private int _id;
	private String questionid;
	private String type;
	private String point_success;
	private String point_fail;
	private String trigger_event;
	private String answer1;
	private String answer2;
	private String answer3;
	private String answer4;
	private String correct_answer;
	private String question;
	private String title;
	private String login_id;
	private String notify_wrong_text;
	private String notify_correct_text;
	private String timeout;
	private String seqid;

	public Question2() {

	}

	public Question2(int id, String questionid, String type,
			String point_seccess, String point_fail, String trigger_event,
			String answer1, String answer2, String answer3, String answer4,
			String correct_answer, String question, String title,
			String login_id, String notify_wrong_text,
			String notify_correct_text, String timeout) {
		this._id = id;
		this.questionid = questionid;
		this.type = type;
		this.point_success = point_seccess;
		this.point_fail = point_fail;
		this.trigger_event = trigger_event;
		this.answer1 = answer1;
		this.answer2 = answer2;
		this.answer3 = answer3;
		this.answer4 = answer4;
		this.correct_answer = correct_answer;
		this.question = question;
		this.title = title;
		this.login_id = login_id;
		this.notify_wrong_text = notify_wrong_text;
		this.notify_correct_text = notify_correct_text;
		this.timeout = timeout;
	}

	public Question2(String questionid, String type, String point_seccess,
			String point_fail, String trigger_event, String answer1,
			String answer2, String answer3, String answer4,
			String correct_answer, String question, String title,
			String login_id, String notify_wrong_text,
			String notify_correct_text, String timeout) {
		this.questionid = questionid;
		this.type = type;
		this.point_success = point_seccess;
		this.point_fail = point_fail;
		this.trigger_event = trigger_event;
		this.answer1 = answer1;
		this.answer2 = answer2;
		this.answer3 = answer3;
		this.answer4 = answer4;
		this.correct_answer = correct_answer;
		this.question = question;
		this.title = title;
		this.login_id = login_id;
		this.notify_wrong_text = notify_wrong_text;
		this.notify_correct_text = notify_correct_text;
		this.timeout = timeout;
	}

	public void setId(int id) {
		this._id = id;
	}

	public void setQuestionid(String text) {
		this.questionid = text;
	}

	public void setType(String text) {
		this.type = text;
	}

	public void setPoint_success(String text) {
		this.point_success = text;
	}

	public void setPoint_fail(String text) {
		this.point_fail = text;
	}

	public void setTrigger_event(String text) {
		this.trigger_event = text;
	}

	public void setAnswer1(String text) {
		this.answer1 = text;
	}

	public void setAnswer2(String text) {
		this.answer2 = text;
	}

	public void setAnswer3(String text) {
		this.answer3 = text;
	}

	public void setAnswer4(String text) {
		this.answer4 = text;
	}

	public void setCorrect_answer(String text) {
		this.correct_answer = text;
	}

	public void setQuestion(String text) {
		this.question = text;
	}

	public void setTitle(String text) {
		this.title = text;
	}

	public void setLogin_id(String text) {
		this.login_id = text;
	}

	public void setNotify_wrong_text(String text) {
		this.notify_wrong_text = text;
	}

	public void setNotify_correct_text(String text) {
		this.notify_correct_text = text;
	}

	public void setTimeout(String text) {
		this.timeout = text;
	}
	
	public void setSeqId(String text) {
		this.seqid = text;
	}

	// ########################

	public int getId() {
		return this._id;
	}

	public String getQuestionid() {
		return this.questionid;
	}

	public String getType() {
		return this.type;
	}

	public String getPoint_success() {
		return this.point_success;
	}

	public String getPoint_fail() {
		return this.point_fail;
	}

	public String getTrigger_event() {
		return this.trigger_event;
	}

	public String getAnswer1() {
		return this.answer1;
	}

	public String getAnswer2() {
		return this.answer2;
	}

	public String getAnswer3() {
		return this.answer3;
	}

	public String getAnswer4() {
		return this.answer4;
	}

	public String getCorrect_answer() {
		return this.correct_answer;
	}

	public String getQuestion() {
		return this.question;
	}

	public String getTitle() {
		return this.title;
	}

	public String getLogin_id() {
		return this.login_id;
	}

	public String getNotify_wrong_text() {
		return this.notify_wrong_text;
	}

	public String getNotify_correct_text() {
		return this.notify_correct_text;
	}

	public String getTimeout() {
		return this.timeout;
	}
	
	public String getSeqId() {
		return this.seqid;
	}

}