package com.wiredin.jambar.dataclass;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHandler extends SQLiteOpenHelper {

	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "jambar.db";
	private static final String TABLE_GAME = "game";
	private static final String TABLE_TEAM = "team";
	private static final String TABLE_MEMBER = "member";
	private static final String TABLE_QUESTION2 = "question2";
	private static final String TABLE_GCM = "gcm";
	private static final String TABLE_SEQUENCE = "sequence";
	private static final String TABLE_SEQUENCEQUESTION = "sequence_question";
	private static final String TABLE_ANSWER = "answer";
	private static final String TABLE_SUMMARY = "summary";
	private static final String TABLE_GT = "gt";

	public static final String SUMMARY_ID = "summary_id";
	public static final String SUMMARY_POINT = "summary_point";
	public static final String SUMMARY_CORRECT = "summary_correct";

	public static final String ANSWER_ID = "answer_id";
	public static final String ANSWER_QUESTIONID = "answer_questionid";
	public static final String ANSWER_ANSWER = "answer_answer";
	public static final String ANSWER_CORRECT = "answer_correct";
	public static final String ANSWER_START = "answer_start";
	public static final String ANSWER_STOP = "answer_stop";

	public static final String GAME_TABLEID = "_id";
	public static final String GAME_ID = "game_id";
	public static final String GAME_TITLE = "game_title";
	public static final String GAME_DESC = "game_desc";
	public static final String GAME_TIME = "game_time";
	public static final String GAME_TOTAL = "game_total_question";

	public static final String TEAM_TABLEID = "_id";
	public static final String TEAM_ID = "team_id";
	public static final String TEAM_NAME = "team_name";
	public static final String TEAM_PICTURE = "team_picture";
	public static final String TEAM_GAME = "team_game";

	public static final String MEMBER_TABLEID = "_id";
	public static final String MEMBER_ID = "member_id";
	public static final String MEMBER_NAME = "member_fullname";
	public static final String MEMBER_EMAIL = "member_email";

	public static final String QUESTION2_TABLEID = "_id";
	public static final String QUESTION2_ID = "questionid";
	public static final String QUESTION2_TYPE = "type";
	public static final String QUESTION2_POINT_SUCCESS = "point_success";
	public static final String QUESTION2_POINT_FAIL = "point_fail";
	public static final String QUESTION2_TRIGGER = "trigger_event";
	public static final String QUESTION2_ANSWER1 = "answer1";
	public static final String QUESTION2_ANSWER2 = "answer2";
	public static final String QUESTION2_ANSWER3 = "answer3";
	public static final String QUESTION2_ANSWER4 = "answer4";
	public static final String QUESTION2_CORRECT_ANSWER = "correct_answer";
	public static final String QUESTION2_QUESTION = "question";
	public static final String QUESTION2_TITLE = "title";
	public static final String QUESTION2_LOGIN_ID = "login_id";
	public static final String QUESTION2_NOTIFY_WRONG = "notify_wrong_text";
	public static final String QUESTION2_NOTIFY_CORRECT = "notify_correct_text";
	public static final String QUESTION2_TIMEOUT = "timeout";

	public static final String SEQUENCE_TABLEID = "_id";
	public static final String SEQUENCE_ID = "seqid";
	public static final String SEQUENCE_TITLE = "title";
	public static final String SEQUENCE_NOTIFY = "notification";
	public static final String SEQUENCE_STARTREQ = "start_req";
	public static final String SEQUENCE_STOPREQ = "stop_req";
	public static final String SEQUENCE_STARTPOINT = "start_point";
	public static final String SEQUENCE_STOPPOINT = "stop_point";
	public static final String SEQUENCE_STARTCODE = "start_code";
	public static final String SEQUENCE_STOPCODE = "stop_code";
	public static final String SEQUENCE_STARTLAT = "start_lat";
	public static final String SEQUENCE_STARTLONG = "start_long";
	public static final String SEQUENCE_STARTRADIUS = "start_radius";
	public static final String SEQUENCE_STOPLAT = "stop_lat";
	public static final String SEQUENCE_STOPLONG = "stop_long";
	public static final String SEQUENCE_STOPRADIUS = "stop_radius";
	public static final String SEQUENCE_STARTQUESTIONID = "start_questionid";
	public static final String SEQUENCE_STARTQUESTIONTIME = "start_questiontime";
	public static final String SEQUENCE_STOPQUESTIONID = "stop_questionid";
	public static final String SEQUENCE_STOPQUESTIONTIME = "stop_questiontime";

	public static final String GCM_ID = "_id";
	public static final String GCM_GCMREGID = "gcm_regid";
	public static final String GCM_APPVERSION = "appversion";

	public static final String GT_ID = "_id";
	public static final String GT_NAME = "teamname";
	public static final String GT_CODE = "teamcode";

	public DatabaseHandler(Context context, String name, CursorFactory factory,
			int version) {
		super(context, DATABASE_NAME, factory, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_TABLE_GAME = "CREATE TABLE " + TABLE_GAME + "("
				+ GAME_TABLEID + " INTEGER PRIMARY KEY," + GAME_ID + " TEXT,"
				+ GAME_TITLE + " TEXT," + GAME_DESC + " TEXT," + GAME_TIME
				+ " TEXT," + GAME_TOTAL + " TEXT" + ")";
		db.execSQL(CREATE_TABLE_GAME);
		String CREATE_TABLE_SUMMARY = "CREATE TABLE " + TABLE_SUMMARY + "("
				+ SUMMARY_ID + " INTEGER PRIMARY KEY," + SUMMARY_POINT
				+ " INTEGER," + SUMMARY_CORRECT + " INTEGER" + ")";
		db.execSQL(CREATE_TABLE_SUMMARY);
		String CREATE_TABLE_TEAM = "CREATE TABLE " + TABLE_TEAM + "("
				+ TEAM_TABLEID + " INTEGER PRIMARY KEY," + TEAM_ID + " TEXT,"
				+ TEAM_NAME + " TEXT," + TEAM_PICTURE + " TEXT," + TEAM_GAME
				+ " TEXT" + ")";
		db.execSQL(CREATE_TABLE_TEAM);
		String CREATE_TABLE_MEMBER = "CREATE TABLE " + TABLE_MEMBER + "("
				+ MEMBER_TABLEID + " INTEGER PRIMARY KEY," + MEMBER_ID
				+ " TEXT," + MEMBER_NAME + " TEXT," + MEMBER_EMAIL + " TEXT"
				+ ")";
		db.execSQL(CREATE_TABLE_MEMBER);
		String CREATE_TABLE_QUESTION2 = "CREATE TABLE " + TABLE_QUESTION2 + "("
				+ QUESTION2_TABLEID + " INTEGER PRIMARY KEY," + QUESTION2_ID
				+ " TEXT," + QUESTION2_TYPE + " TEXT,"
				+ QUESTION2_POINT_SUCCESS + " TEXT," + QUESTION2_POINT_FAIL
				+ " TEXT," + QUESTION2_TRIGGER + " TEXT," + QUESTION2_ANSWER1
				+ " TEXT," + QUESTION2_ANSWER2 + " TEXT," + QUESTION2_ANSWER3
				+ " TEXT," + QUESTION2_ANSWER4 + " TEXT,"
				+ QUESTION2_CORRECT_ANSWER + " TEXT," + QUESTION2_QUESTION
				+ " TEXT," + QUESTION2_TITLE + " TEXT," + QUESTION2_LOGIN_ID
				+ " TEXT," + QUESTION2_NOTIFY_WRONG + " TEXT,"
				+ QUESTION2_NOTIFY_CORRECT + " TEXT," + QUESTION2_TIMEOUT
				+ " TEXT " + ")";
		db.execSQL(CREATE_TABLE_QUESTION2);
		String CREATE_TABLE_GCM = "CREATE TABLE " + TABLE_GCM + "(" + GCM_ID
				+ " INTEGER PRIMARY KEY," + GCM_GCMREGID + " TEXT,"
				+ GCM_APPVERSION + " TEXT" + ")";
		db.execSQL(CREATE_TABLE_GCM);
		String CREATE_TABLE_SEQUENCE = "CREATE TABLE " + TABLE_SEQUENCE + "("
				+ SEQUENCE_TABLEID + " INTEGER PRIMARY KEY," + SEQUENCE_ID
				+ " TEXT," + SEQUENCE_TITLE + " TEXT," + SEQUENCE_NOTIFY
				+ " TEXT," + SEQUENCE_STARTREQ + " TEXT," + SEQUENCE_STOPREQ
				+ " TEXT," + SEQUENCE_STARTPOINT + " TEXT,"
				+ SEQUENCE_STOPPOINT + " TEXT," + SEQUENCE_STARTCODE + " TEXT,"
				+ SEQUENCE_STOPCODE + " TEXT," + SEQUENCE_STARTLAT + " TEXT,"
				+ SEQUENCE_STARTLONG + " TEXT," + SEQUENCE_STARTRADIUS
				+ " TEXT," + SEQUENCE_STOPLAT + " TEXT," + SEQUENCE_STOPLONG
				+ " TEXT," + SEQUENCE_STOPRADIUS + " TEXT,"
				+ SEQUENCE_STARTQUESTIONID + " TEXT,"
				+ SEQUENCE_STARTQUESTIONTIME + " TEXT,"
				+ SEQUENCE_STOPQUESTIONID + " TEXT,"
				+ SEQUENCE_STOPQUESTIONTIME + " TEXT " + ")";
		db.execSQL(CREATE_TABLE_SEQUENCE);
		String CREATE_TABLE_SEQUENCEQUESTION = "CREATE TABLE "
				+ TABLE_SEQUENCEQUESTION + "(" + QUESTION2_TABLEID
				+ " INTEGER PRIMARY KEY," + QUESTION2_ID + " TEXT,"
				+ QUESTION2_TYPE + " TEXT," + QUESTION2_POINT_SUCCESS
				+ " TEXT," + QUESTION2_POINT_FAIL + " TEXT,"
				+ QUESTION2_TRIGGER + " TEXT," + QUESTION2_ANSWER1 + " TEXT,"
				+ QUESTION2_ANSWER2 + " TEXT," + QUESTION2_ANSWER3 + " TEXT,"
				+ QUESTION2_ANSWER4 + " TEXT," + QUESTION2_CORRECT_ANSWER
				+ " TEXT," + QUESTION2_QUESTION + " TEXT," + QUESTION2_TITLE
				+ " TEXT," + QUESTION2_LOGIN_ID + " TEXT,"
				+ QUESTION2_NOTIFY_WRONG + " TEXT," + QUESTION2_NOTIFY_CORRECT
				+ " TEXT," + QUESTION2_TIMEOUT + " TEXT, " + "seqid" + " TEXT "
				+ ")";
		db.execSQL(CREATE_TABLE_SEQUENCEQUESTION);
		String CREATE_TABLE_ANSWER = "CREATE TABLE " + TABLE_ANSWER + "("
				+ ANSWER_ID + " INTEGER PRIMARY KEY," + ANSWER_QUESTIONID
				+ " TEXT," + ANSWER_ANSWER + " TEXT," + ANSWER_CORRECT
				+ " TEXT," + " TEXT," + ANSWER_START + " TEXT," + ANSWER_STOP
				+ " TEXT" + ")";
		db.execSQL(CREATE_TABLE_ANSWER);
		String CREATE_TABLE_GT = "CREATE TABLE " + TABLE_GT + "(" + GT_ID
				+ " INTEGER PRIMARY KEY," + GT_NAME + " TEXT," + GT_CODE
				+ " TEXT" + ")";
		db.execSQL(CREATE_TABLE_GT);
		Log.v("Hadian", "Database Created");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.v("Hadian", "Database Updated form " + oldVersion + " to "
				+ newVersion);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_GAME);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_TEAM);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_MEMBER);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_QUESTION2);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_GCM);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SEQUENCE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SEQUENCEQUESTION);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ANSWER);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SUMMARY);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_GT);
		onCreate(db);
	}

	// database card handler

	public void addGame(String id, String title, String desc, String total,
			String time) {

		ContentValues values = new ContentValues();
		values.put(GAME_TABLEID, 1);
		values.put(GAME_ID, id);
		values.put(GAME_TITLE, title);
		values.put(GAME_DESC, desc);
		values.put(GAME_TOTAL, total);
		values.put(GAME_TIME, time);

		SQLiteDatabase db = this.getWritableDatabase();

		db.insert(TABLE_GAME, null, values);
		db.close();

		Log.v("Hadian", "game Added - " + title);
	}

	public void deleteGame() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DELETE FROM " + TABLE_GAME);
		db.close();
		Log.v("Hadian", "game Deleted");
	}

	public Game getGame() {
		// Select All Query
		int id = 1;

		String query = "Select * " + "FROM " + TABLE_GAME + " WHERE "
				+ GAME_TABLEID + " =  \"" + id + "\"";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(query, null);

		Game game = new Game();

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				game.setId(Integer.parseInt(cursor.getString(0)));
				game.setGameid(cursor.getString(1));
				game.setTitle(cursor.getString(2));
				game.setDesc(cursor.getString(3));
				game.setTime(cursor.getString(4));
				game.setTotal(cursor.getString(5));

			} while (cursor.moveToNext());
		} else {
			game = null;
		}
		db.close();

		// return contact list
		return game;
	}

	public void addTeam(String id, String name, String picture, String game) {

		ContentValues values = new ContentValues();
		values.put(TEAM_TABLEID, 1);
		values.put(TEAM_ID, id);
		values.put(TEAM_NAME, name);
		values.put(TEAM_PICTURE, picture);
		values.put(TEAM_GAME, game);

		SQLiteDatabase db = this.getWritableDatabase();

		db.insert(TABLE_TEAM, null, values);
		db.close();

		Log.v("Hadian", "team Added - " + name + " picture " + picture);
	}

	public void updateTeamPhoto(String id, String picture) {

		ContentValues values = new ContentValues();
		values.put(TEAM_PICTURE, picture);

		SQLiteDatabase db = this.getWritableDatabase();

		Log.d("fared", "path dlm database" + picture);

		db.update(TABLE_TEAM, values, "team_id = " + id, null);
		db.close();

	}

	public void deleteTeam() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DELETE FROM " + TABLE_TEAM);
		db.close();
		Log.v("Hadian", "team Deleted");
	}

	public Team getTeam() {
		// Select All Query
		int id = 1;

		String query = "Select * " + "FROM " + TABLE_TEAM + " WHERE "
				+ TEAM_TABLEID + " =  \"" + id + "\"";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(query, null);

		Team team = new Team();

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				team.setId(Integer.parseInt(cursor.getString(0)));
				team.setTeamid(cursor.getString(1));
				team.setName(cursor.getString(2));
				team.setPicture(cursor.getString(3));
				team.setGame(cursor.getString(4));

			} while (cursor.moveToNext());
		} else {
			team = null;
		}
		db.close();

		// return contact list
		return team;
	}

	public String getTeamPicture(String id) {

		String query = "Select " + TEAM_PICTURE + " FROM " + TABLE_TEAM
				+ " WHERE " + TEAM_ID + " =  \"" + id + "\"";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(query, null);

		String result = null;

		if (cursor.moveToFirst()) {
			cursor.moveToFirst();
			result = cursor.getString(0);
			cursor.close();
		} else {
			result = null;
		}
		db.close();

		Log.d("Fared", "get picture path dari db" + result + "haha");

		return result;
	}

	public void addMember(String id, String name, String email) {

		ContentValues values = new ContentValues();
		values.put(MEMBER_ID, id);
		values.put(MEMBER_NAME, name);
		values.put(MEMBER_EMAIL, email);

		SQLiteDatabase db = this.getWritableDatabase();

		db.insert(TABLE_MEMBER, null, values);
		db.close();

		Log.v("Hadian", "member Added - " + name);
	}

	public void deleteMember() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DELETE FROM " + TABLE_MEMBER);
		db.close();
		Log.v("Hadian", "member Deleted");
	}

	public List<Member> getAllMember() {
		List<Member> memberList = new ArrayList<Member>();
		// Select All Query
		String query = "Select * FROM " + TABLE_MEMBER;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(query, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Member member = new Member();
				member.setId(Integer.parseInt(cursor.getString(0)));
				member.setMemberid(cursor.getString(1));
				member.setFullname(cursor.getString(2));
				member.setEmail(cursor.getString(3));

				// Adding contact to list
				memberList.add(member);
			} while (cursor.moveToNext());
		} else {
			memberList = null;
		}
		db.close();

		// return contact list
		return memberList;
	}

	public void addSeqQuestion(String questionid, String type,
			String point_success, String point_fail, String trigger_event,
			String answer1, String answer2, String answer3, String answer4,
			String correct_answer, String question, String title,
			String login_id, String notify_wrong_text,
			String notify_correct_text, String timeout, String seqid) {

		ContentValues values = new ContentValues();
		values.put(QUESTION2_ID, questionid);
		values.put(QUESTION2_TYPE, type);
		values.put(QUESTION2_POINT_SUCCESS, point_success);
		values.put(QUESTION2_POINT_FAIL, point_fail);
		values.put(QUESTION2_TRIGGER, trigger_event);
		values.put(QUESTION2_ANSWER1, answer1);
		values.put(QUESTION2_ANSWER2, answer2);
		values.put(QUESTION2_ANSWER3, answer3);
		values.put(QUESTION2_ANSWER4, answer4);
		values.put(QUESTION2_CORRECT_ANSWER, correct_answer);
		values.put(QUESTION2_QUESTION, question);
		values.put(QUESTION2_TITLE, title);
		values.put(QUESTION2_LOGIN_ID, login_id);
		values.put(QUESTION2_NOTIFY_WRONG, notify_wrong_text);
		values.put(QUESTION2_NOTIFY_CORRECT, notify_correct_text);
		values.put(QUESTION2_TIMEOUT, timeout);
		values.put("seqid", seqid);

		SQLiteDatabase db = this.getWritableDatabase();

		db.insert(TABLE_SEQUENCEQUESTION, null, values);
		db.close();

		Log.v("Hadian", "sequence Added - " + title + " || type - " + type);
	}

	public void deleteSeqQuestion() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DELETE FROM " + TABLE_SEQUENCEQUESTION);
		db.close();
		Log.v("Hadian", "seq questions Deleted");
	}

	public Question2 getSeqQuestionById(String id) {
		// Select All Query

		String query = "Select * " + "FROM " + TABLE_SEQUENCEQUESTION
				+ " WHERE " + QUESTION2_ID + " =  " + id;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(query, null);

		Question2 question = new Question2();

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				question.setId(Integer.parseInt(cursor.getString(0)));
				question.setQuestionid(cursor.getString(1));
				question.setType(cursor.getString(2));
				question.setPoint_success(cursor.getString(3));
				question.setPoint_fail(cursor.getString(4));
				question.setTrigger_event(cursor.getString(5));
				question.setAnswer1(cursor.getString(6));
				question.setAnswer2(cursor.getString(7));
				question.setAnswer3(cursor.getString(8));
				question.setAnswer4(cursor.getString(9));
				question.setCorrect_answer(cursor.getString(10));
				question.setQuestion(cursor.getString(11));
				question.setTitle(cursor.getString(12));
				question.setLogin_id(cursor.getString(13));
				question.setNotify_wrong_text(cursor.getString(14));
				question.setNotify_correct_text(cursor.getString(15));
				question.setTimeout(cursor.getString(16));
				question.setSeqId(cursor.getString(17));

			} while (cursor.moveToNext());

		} else {
			question = null;
		}
		db.close();

		// return contact list
		return question;
	}

	public List<Question2> getSeqAllQuestion() {
		List<Question2> questionList = new ArrayList<Question2>();
		// Select All Query
		String query = "Select * FROM " + TABLE_SEQUENCEQUESTION;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(query, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Question2 question = new Question2();
				question.setId(Integer.parseInt(cursor.getString(0)));
				question.setQuestionid(cursor.getString(1));
				question.setType(cursor.getString(2));
				question.setPoint_success(cursor.getString(3));
				question.setPoint_fail(cursor.getString(4));
				question.setTrigger_event(cursor.getString(5));
				question.setAnswer1(cursor.getString(6));
				question.setAnswer2(cursor.getString(7));
				question.setAnswer3(cursor.getString(8));
				question.setAnswer4(cursor.getString(9));
				question.setCorrect_answer(cursor.getString(10));
				question.setQuestion(cursor.getString(11));
				question.setTitle(cursor.getString(12));
				question.setLogin_id(cursor.getString(13));
				question.setNotify_wrong_text(cursor.getString(14));
				question.setNotify_correct_text(cursor.getString(15));
				question.setTimeout(cursor.getString(16));
				question.setSeqId(cursor.getString(17));

				Log.d("Hadian", "id:" + question.getId());

				// Adding contact to list
				questionList.add(question);

			} while (cursor.moveToNext());
		} else {
			questionList = null;
		}
		db.close();

		// return contact list
		return questionList;
	}

	public List<String> getSeqQuestionBySeqId(String id) {
		List<String> questionidList = new ArrayList<String>();
		// Select All Query
		String query = "Select " + QUESTION2_ID + " FROM "
				+ TABLE_SEQUENCEQUESTION + " WHERE seqid =  \"" + id + "\"";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(query, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				String questionid = cursor.getString(0);

				// Adding contact to list
				questionidList.add(questionid);

			} while (cursor.moveToNext());
		} else {
			questionidList = null;
		}
		db.close();

		// return contact list
		return questionidList;
	}

	public void addQuestion2(String questionid, String type,
			String point_success, String point_fail, String trigger_event,
			String answer1, String answer2, String answer3, String answer4,
			String correct_answer, String question, String title,
			String login_id, String notify_wrong_text,
			String notify_correct_text, String timeout) {

		ContentValues values = new ContentValues();
		values.put(QUESTION2_ID, questionid);
		values.put(QUESTION2_TYPE, type);
		values.put(QUESTION2_POINT_SUCCESS, point_success);
		values.put(QUESTION2_POINT_FAIL, point_fail);
		values.put(QUESTION2_TRIGGER, trigger_event);
		values.put(QUESTION2_ANSWER1, answer1);
		values.put(QUESTION2_ANSWER2, answer2);
		values.put(QUESTION2_ANSWER3, answer3);
		values.put(QUESTION2_ANSWER4, answer4);
		values.put(QUESTION2_CORRECT_ANSWER, correct_answer);
		values.put(QUESTION2_QUESTION, question);
		values.put(QUESTION2_TITLE, title);
		values.put(QUESTION2_LOGIN_ID, login_id);
		values.put(QUESTION2_NOTIFY_WRONG, notify_wrong_text);
		values.put(QUESTION2_NOTIFY_CORRECT, notify_correct_text);
		values.put(QUESTION2_TIMEOUT, timeout);

		SQLiteDatabase db = this.getWritableDatabase();

		db.insert(TABLE_QUESTION2, null, values);
		db.close();

		Log.v("Hadian", "question2 Added - " + title);
	}

	public void deleteQuestion2() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DELETE FROM " + TABLE_QUESTION2);
		db.close();
		Log.v("Hadian", "questions Deleted");
	}

	public Question2 getQuestionByTableId2(int id) {
		// Select All Query

		String query = "Select * " + "FROM " + TABLE_QUESTION2 + " WHERE "
				+ QUESTION2_TABLEID + " =  \"" + id + "\"";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(query, null);

		Question2 question = new Question2();

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				question.setId(Integer.parseInt(cursor.getString(0)));
				question.setQuestionid(cursor.getString(1));
				question.setType(cursor.getString(2));
				question.setPoint_success(cursor.getString(3));
				question.setPoint_fail(cursor.getString(4));
				question.setTrigger_event(cursor.getString(5));
				question.setAnswer1(cursor.getString(6));
				question.setAnswer2(cursor.getString(7));
				question.setAnswer3(cursor.getString(8));
				question.setAnswer4(cursor.getString(9));
				question.setCorrect_answer(cursor.getString(10));
				question.setQuestion(cursor.getString(11));
				question.setTitle(cursor.getString(12));
				question.setLogin_id(cursor.getString(13));
				question.setNotify_wrong_text(cursor.getString(14));
				question.setNotify_correct_text(cursor.getString(15));
				question.setTimeout(cursor.getString(16));

			} while (cursor.moveToNext());

		} else {
			question = null;
		}
		db.close();

		// return contact list
		return question;
	}

	public Question2 getQuestionByQuestionId2(String id) {
		// Select All Query

		String query = "Select * " + "FROM " + TABLE_QUESTION2 + " WHERE "
				+ QUESTION2_ID + " =  \"" + id + "\"";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(query, null);

		Question2 question = new Question2();

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				question.setId(Integer.parseInt(cursor.getString(0)));
				question.setQuestionid(cursor.getString(1));
				question.setType(cursor.getString(2));
				question.setPoint_success(cursor.getString(3));
				question.setPoint_fail(cursor.getString(4));
				question.setTrigger_event(cursor.getString(5));
				question.setAnswer1(cursor.getString(6));
				question.setAnswer2(cursor.getString(7));
				question.setAnswer3(cursor.getString(8));
				question.setAnswer4(cursor.getString(9));
				question.setCorrect_answer(cursor.getString(10));
				question.setQuestion(cursor.getString(11));
				question.setTitle(cursor.getString(12));
				question.setLogin_id(cursor.getString(13));
				question.setNotify_wrong_text(cursor.getString(14));
				question.setNotify_correct_text(cursor.getString(15));
				question.setTimeout(cursor.getString(16));

			} while (cursor.moveToNext());

		} else {
			question = null;
		}
		db.close();

		// return contact list
		return question;
	}

	public List<Question2> getAllQuestion2() {
		List<Question2> questionList = new ArrayList<Question2>();
		// Select All Query
		String query = "Select * FROM " + TABLE_QUESTION2;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(query, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Question2 question = new Question2();
				question.setId(Integer.parseInt(cursor.getString(0)));
				question.setQuestionid(cursor.getString(1));
				question.setType(cursor.getString(2));
				question.setPoint_success(cursor.getString(3));
				question.setPoint_fail(cursor.getString(4));
				question.setTrigger_event(cursor.getString(5));
				question.setAnswer1(cursor.getString(6));
				question.setAnswer2(cursor.getString(7));
				question.setAnswer3(cursor.getString(8));
				question.setAnswer4(cursor.getString(9));
				question.setCorrect_answer(cursor.getString(10));
				question.setQuestion(cursor.getString(11));
				question.setTitle(cursor.getString(12));
				question.setLogin_id(cursor.getString(13));
				question.setNotify_wrong_text(cursor.getString(14));
				question.setNotify_correct_text(cursor.getString(15));
				question.setTimeout(cursor.getString(16));

				Log.d("Hadian", "id:" + question.getId());

				// Adding contact to list
				questionList.add(question);

			} while (cursor.moveToNext());
		} else {
			questionList = null;
		}
		db.close();

		// return contact list
		return questionList;
	}

	public void addSummary() {
		ContentValues values = new ContentValues();
		values.put(SUMMARY_ID, 1);
		values.put(SUMMARY_POINT, 0);
		values.put(SUMMARY_CORRECT, 0);

		SQLiteDatabase db = this.getWritableDatabase();

		db.insert(TABLE_SUMMARY, null, values);
		db.close();

		Log.v("Hadian", "TABLE_SUMMARY Added");
	}

	public void deleteSummary() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DELETE FROM " + TABLE_SUMMARY);
		db.close();
		Log.v("Hadian", "TABLE_SUMMARY Deleted");
	}

	public void updateSummaryPoint(String point) {

		ContentValues values = new ContentValues();
		values.put(SUMMARY_POINT, point);

		SQLiteDatabase db = this.getWritableDatabase();

		db.update(TABLE_SUMMARY, values, SUMMARY_ID + " = \"" + 1 + "\"", null);
		db.close();

		Log.v("Hadian", "TABLE_SUMMARY Updated");
	}

	public void updateSummaryCorrect(String correct) {

		ContentValues values = new ContentValues();
		values.put(SUMMARY_CORRECT, correct);

		SQLiteDatabase db = this.getWritableDatabase();

		db.update(TABLE_SUMMARY, values, SUMMARY_ID + " = \"" + 1 + "\"", null);
		db.close();

		Log.v("Hadian", "TABLE_SUMMARY Updated");
	}

	public void addGCM(String regid, String appversion) {
		int id = 1;
		ContentValues values = new ContentValues();
		values.put(GCM_ID, id);
		values.put(GCM_GCMREGID, regid);
		values.put(GCM_APPVERSION, appversion);

		SQLiteDatabase db = this.getWritableDatabase();

		db.insert(TABLE_GCM, null, values);
		db.close();

		Log.v("Hadian", "TABLE_GCM Added");
	}

	public String getSummaryPoint() {
		int id = 1;
		String result;
		String query = "Select " + SUMMARY_POINT + " FROM " + TABLE_SUMMARY
				+ " WHERE " + SUMMARY_ID + " =  \"" + id + "\"";

		SQLiteDatabase db = this.getWritableDatabase();

		Cursor cursor = db.rawQuery(query, null);

		if (cursor.moveToFirst()) {
			cursor.moveToFirst();
			result = cursor.getString(0);
			cursor.close();
		} else {
			result = null;
		}
		db.close();
		return result;
	}

	public String getSummaryCorrect() {
		int id = 1;
		String result;
		String query = "Select " + SUMMARY_CORRECT + " FROM " + TABLE_SUMMARY
				+ " WHERE " + SUMMARY_ID + " =  \"" + id + "\"";

		SQLiteDatabase db = this.getWritableDatabase();

		Cursor cursor = db.rawQuery(query, null);

		if (cursor.moveToFirst()) {
			cursor.moveToFirst();
			result = cursor.getString(0);
			cursor.close();
		} else {
			result = null;
		}
		db.close();
		return result;
	}

	public void deleteGCM() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DELETE FROM " + TABLE_GCM);
		db.close();
		Log.v("Hadian", "TABLE_GCM Deleted");
	}

	public String getGCMRegId() {
		int id = 1;
		String result;
		String query = "Select " + GCM_GCMREGID + " FROM " + TABLE_GCM
				+ " WHERE " + GCM_ID + " =  \"" + id + "\"";

		SQLiteDatabase db = this.getWritableDatabase();

		Cursor cursor = db.rawQuery(query, null);

		if (cursor.moveToFirst()) {
			cursor.moveToFirst();
			result = cursor.getString(0);
			cursor.close();
		} else {
			result = null;
		}
		db.close();
		return result;
	}

	public String getGCMVersion() {
		int id = 1;
		String result;
		String query = "Select " + GCM_APPVERSION + " FROM " + TABLE_GCM
				+ " WHERE " + GCM_ID + " =  \"" + id + "\"";

		SQLiteDatabase db = this.getWritableDatabase();

		Cursor cursor = db.rawQuery(query, null);

		if (cursor.moveToFirst()) {
			cursor.moveToFirst();
			result = cursor.getString(0);
			cursor.close();
		} else {
			result = null;
		}
		db.close();
		return result;
	}

	// answer table

	public void addStartAnswer(String questionid, String start) {

		ContentValues values = new ContentValues();
		values.put(ANSWER_QUESTIONID, questionid);
		values.put(ANSWER_START, start);

		SQLiteDatabase db = this.getWritableDatabase();

		db.insert(TABLE_ANSWER, null, values);
		db.close();

		Log.v("Hadian", "TABLE_ANSWER Added");
	}

	public void addStopAnswer(String questionid, String answer, String correct,
			String stop) {

		ContentValues values = new ContentValues();
		values.put(ANSWER_ANSWER, answer);
		values.put(ANSWER_CORRECT, correct);
		values.put(ANSWER_STOP, stop);

		SQLiteDatabase db = this.getWritableDatabase();

		db.update(TABLE_ANSWER, values, ANSWER_QUESTIONID + " = \""
				+ questionid + "\"", null);
		db.close();

		Log.v("Hadian", "TABLE_ANSWER Updated");
	}

	public List<Answer> getAllAnswers() {
		List<Answer> answerList = new ArrayList<Answer>();
		// Select All Query
		String query = "Select * FROM " + TABLE_ANSWER;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(query, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Answer answer = new Answer();
				answer.setId(Integer.parseInt(cursor.getString(0)));
				answer.setQuestionid(cursor.getString(1));
				answer.setAnswer(cursor.getString(2));
				answer.setCorrect(cursor.getString(3));
				answer.setStart(cursor.getString(4));
				answer.setStop(cursor.getString(5));

				// Adding contact to list
				answerList.add(answer);

			} while (cursor.moveToNext());
		} else {
			answerList = null;
		}
		db.close();

		// return contact list
		return answerList;

	}

	public void deleteAllAnswer() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DELETE FROM " + TABLE_ANSWER);
		db.close();
		Log.v("Hadian", "TABLE_ANSWER Deleted");
	}

	public void addSequence(String sequenceid, String title, String notify,
			String startreq, String stopreq, String startpoint,
			String stoppoint, String startcode, String stopcode,
			String startlat, String startlong, String startradius,
			String stoplat, String stoplong, String stopradius,
			String startquestionid, String startquestiontime,
			String stopquestionid, String stopquestiontime) {

		ContentValues values = new ContentValues();
		values.put(SEQUENCE_ID, sequenceid);
		values.put(SEQUENCE_TITLE, title);
		values.put(SEQUENCE_NOTIFY, notify);
		values.put(SEQUENCE_STARTREQ, startreq);
		values.put(SEQUENCE_STOPREQ, stopreq);
		values.put(SEQUENCE_STARTPOINT, startpoint);
		values.put(SEQUENCE_STOPPOINT, stoppoint);
		values.put(SEQUENCE_STARTCODE, startcode);
		values.put(SEQUENCE_STOPCODE, stopcode);
		values.put(SEQUENCE_STARTLAT, startlat);
		values.put(SEQUENCE_STARTLONG, startlong);
		values.put(SEQUENCE_STARTRADIUS, startradius);
		values.put(SEQUENCE_STOPLAT, stoplat);
		values.put(SEQUENCE_STOPLONG, stoplong);
		values.put(SEQUENCE_STOPRADIUS, stopradius);
		values.put(SEQUENCE_STARTQUESTIONID, startquestionid);
		values.put(SEQUENCE_STARTQUESTIONTIME, startquestiontime);
		values.put(SEQUENCE_STOPQUESTIONID, stopquestionid);
		values.put(SEQUENCE_STOPQUESTIONTIME, stopquestiontime);

		SQLiteDatabase db = this.getWritableDatabase();

		db.insert(TABLE_SEQUENCE, null, values);
		db.close();

		Log.v("Hadian", "question2 Added - " + title);
	}

	public void deleteAllSequence() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DELETE FROM " + TABLE_SEQUENCE);
		db.close();
		Log.v("Hadian", "TABLE_ANSWER Deleted");
	}

	public List<Sequence> getAllSequence() {
		List<Sequence> sequenceList = new ArrayList<Sequence>();
		// Select All Query
		String query = "Select * FROM " + TABLE_SEQUENCE;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(query, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Sequence sequence = new Sequence();
				sequence.setId(Integer.parseInt(cursor.getString(0)));
				sequence.setSeqid(cursor.getString(1));
				sequence.setTitle(cursor.getString(2));
				sequence.setNotification(cursor.getString(3));
				sequence.setStart_req(cursor.getString(4));
				sequence.setStop_req(cursor.getString(5));
				sequence.setStart_point(cursor.getString(6));
				sequence.setStop_point(cursor.getString(7));
				sequence.setStart_code(cursor.getString(8));
				sequence.setStop_code(cursor.getString(9));
				sequence.setStart_lat(cursor.getString(10));
				sequence.setStart_long(cursor.getString(11));
				sequence.setStart_radius(cursor.getString(12));
				sequence.setStop_lat(cursor.getString(13));
				sequence.setStop_long(cursor.getString(14));
				sequence.setStop_radius(cursor.getString(15));
				sequence.setStart_questionid(cursor.getString(16));
				sequence.setStart_questiontime(cursor.getString(17));
				sequence.setStop_questionid(cursor.getString(18));
				sequence.setStop_questiontime(cursor.getString(19));

				// Adding contact to list
				sequenceList.add(sequence);

			} while (cursor.moveToNext());
		} else {
			sequenceList = null;
		}
		db.close();

		// return contact list
		return sequenceList;
	}

	public Sequence getSequenceBySeqid(String seqid) {
		// Select All Query
		String query = "Select * FROM " + TABLE_SEQUENCE + " WHERE "
				+ SEQUENCE_ID + " =  \"" + seqid + "\"";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(query, null);

		Sequence sequence = new Sequence();

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				sequence.setId(Integer.parseInt(cursor.getString(0)));
				sequence.setSeqid(cursor.getString(1));
				sequence.setTitle(cursor.getString(2));
				sequence.setNotification(cursor.getString(3));
				sequence.setStart_req(cursor.getString(4));
				sequence.setStop_req(cursor.getString(5));
				sequence.setStart_point(cursor.getString(6));
				sequence.setStop_point(cursor.getString(7));
				sequence.setStart_code(cursor.getString(8));
				sequence.setStop_code(cursor.getString(9));
				sequence.setStart_lat(cursor.getString(10));
				sequence.setStart_long(cursor.getString(11));
				sequence.setStart_radius(cursor.getString(12));
				sequence.setStop_lat(cursor.getString(13));
				sequence.setStop_long(cursor.getString(14));
				sequence.setStop_radius(cursor.getString(15));
				sequence.setStart_questionid(cursor.getString(16));
				sequence.setStart_questiontime(cursor.getString(17));
				sequence.setStop_questionid(cursor.getString(18));
				sequence.setStop_questiontime(cursor.getString(19));

			} while (cursor.moveToNext());
		} else {
			sequence = null;
		}
		db.close();

		// return contact list
		return sequence;
	}

	public void setSequenceDone(String seqid) {

		ContentValues values = new ContentValues();
		values.put(SEQUENCE_TITLE, "Done");

		SQLiteDatabase db = this.getWritableDatabase();

		db.update(TABLE_SEQUENCE, values, SEQUENCE_ID + " = \"" + seqid + "\"",
				null);
		db.close();

		Log.v("Hadian", "TABLE_SEQUENCE Updated");
	}

	public void addGT(String id, String name, String code) {

		ContentValues values = new ContentValues();
		values.put(GT_NAME, name);
		values.put(GT_CODE, code);

		SQLiteDatabase db = this.getWritableDatabase();

		db.insert(TABLE_GT, null, values);
		db.close();

		Log.v("Hadian", "GT Added - " + name);
	}

	public void deleteGT() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DELETE FROM " + TABLE_GT);
		db.close();
		Log.v("Hadian", "GT Deleted");
	}

	public List<GameTeam> getAllGT() {
		List<GameTeam> GTList = new ArrayList<GameTeam>();
		// Select All Query
		String query = "Select * FROM " + TABLE_GT;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(query, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				GameTeam gt = new GameTeam();
				gt.setId(Integer.parseInt(cursor.getString(0)));
				gt.setName(cursor.getString(1));
				gt.setCode(cursor.getString(2));

				// Adding contact to list
				GTList.add(gt);
			} while (cursor.moveToNext());
		} else {
			GTList = null;
		}
		db.close();

		// return contact list
		return GTList;
	}

}
