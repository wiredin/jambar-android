package com.wiredin.jambar.dataclass;

public class Sequence {
	private int _id;
	private String seqid;
	private String title;
	private String notification;
	private String start_req;
	private String stop_req;
	private String start_point;
	private String stop_point;
	private String start_code;
	private String stop_code;
	private String start_lat;
	private String start_long;
	private String start_radius;
	private String stop_lat;
	private String stop_long;
	private String stop_radius;
	private String start_questionid;
	private String start_questiontime;
	private String stop_questionid;
	private String stop_questiontime;

	public Sequence() {
		
	}
	
	public Sequence(int id, String seqid, String title, String notification, String start_req,
			String stop_req, String start_point, String stop_point, String start_code,
			String stop_code, String start_lat, String start_long, String start_radius,
			String stop_lat, String stop_long, String stop_radius, String start_questionid,
			String start_questiontime, String stop_questionid, String stop_questiontime ) {
		this._id = id;
		this.seqid = seqid;
		this.title = title;
		this.notification = notification;
		this.start_req = start_req;
		this.stop_req = stop_req;
		this.start_point = start_point;
		this.stop_point = stop_point;
		this.start_code = start_code;
		this.stop_code = stop_code;
		this.start_lat = start_lat;
		this.start_long = start_long;
		this.start_radius = start_radius;
		this.stop_lat = stop_lat;
		this.stop_long = stop_long;
		this.stop_radius = stop_radius;
		this.start_questionid = start_questionid;
		this.start_questiontime = start_questiontime;
		this.stop_questionid = stop_questionid;
		this.stop_questiontime = stop_questiontime;
	}
	
	public Sequence(String seqid, String title, String notification, String start_req,
			String stop_req, String start_point, String stop_point, String start_code,
			String stop_code, String start_lat, String start_long, String start_radius,
			String stop_lat, String stop_long, String stop_radius, String start_questionid,
			String start_questiontime, String stop_questionid, String stop_questiontime) {
		this.seqid = seqid;
		this.title = title;
		this.notification = notification;
		this.start_req = start_req;
		this.stop_req = stop_req;
		this.start_point = start_point;
		this.stop_point = stop_point;
		this.start_code = start_code;
		this.stop_code = stop_code;
		this.start_lat = start_lat;
		this.start_long = start_long;
		this.start_radius = start_radius;
		this.stop_lat = stop_lat;
		this.stop_long = stop_long;
		this.stop_radius = stop_radius;
		this.start_questionid = start_questionid;
		this.start_questiontime = start_questiontime;
		this.stop_questionid = stop_questionid;
		this.stop_questiontime = stop_questiontime;
	}
	
	public void setId(int id) {
		this._id = id;
	}
	public void setSeqid(String text){
		this.seqid = text;
	}
	public void setTitle(String text){
		this.title = text;
	}
	public void setNotification(String text){
		this.notification = text;
	}
	public void setStart_req(String text){
		this.start_req = text;
	}
	public void setStop_req(String text) {
		this.stop_req = text;
	}
	public void setStart_point(String text){
		this.start_point = text;
	}
	public void setStop_point(String text){
		this.stop_point = text;
	}
	public void setStart_code(String text){
		this.start_code = text;
	}
	public void setStop_code(String text){
		this.stop_code = text;
	}
	public void setStart_lat(String text){
		this.start_lat = text;
	}
	public void setStart_long(String text){
		this.start_long = text;
	}
	public void setStart_radius(String text){
		this.start_radius = text;
	}
	public void setStop_lat(String text) {
		this.stop_lat = text;
	}
	public void setStop_long(String text){
		this.stop_long = text;
	}
	public void setStop_radius(String text){
		this.stop_radius = text;
	}
	public void setStart_questionid(String text){
		this.start_questionid = text;
	}
	public void setStart_questiontime(String text){
		this.start_questiontime = text;
	}
	public void setStop_questionid(String text){
		this.stop_questionid = text;
	}
	public void setStop_questiontime(String text){
		this.stop_questiontime = text;
	}
	
	public int getId(){
		return this._id;
	}
	public String getSeqid(){
		return this.seqid;
	}
	public String getTitle(){
		return this.title;
	}
	public String getNotification(){
		return this.notification;
	}
	public String getStart_req(){
		return this.start_req;
	}
	public String getStop_req() {
		return this.stop_req;
	}
	public String getStart_point(){
		return this.start_point;
	}
	public String getStop_point(){
		return this.stop_point;
	}
	public String getStart_code(){
		return this.start_code;
	}
	public String getStop_code(){
		return this.stop_code;
	}
	public String getStart_lat(){
		return this.start_lat;
	}
	public String getStart_long(){
		return this.start_long;
	}
	public String getStart_radius(){
		return this.start_radius;
	}
	public String getStop_lat() {
		return this.stop_lat;
	}
	public String getStop_long(){
		return this.stop_long;
	}
	public String getStop_radius(){
		return this.stop_radius;
	}
	public String getStart_questionid(){
		return this.start_questionid;
	}
	public String getStart_questiontime(){
		return this.start_questiontime;
	}
	public String getStop_questionid(){
		return this.stop_questionid;
	}
	public String getStop_questiontime(){
		return this.stop_questiontime;
	}

}