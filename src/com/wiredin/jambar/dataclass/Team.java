package com.wiredin.jambar.dataclass;

public class Team {
	private int _id;
	private String teamid;
	private String name;
	private String picture;
	private String game;

	public Team() {
		
	}
	
	public Team(int id, String teamid, String name, String picture, String game) {
		this._id = id;
		this.teamid = teamid;
		this.name = name;
		this.picture = picture;
		this.game = game;
	}
	
	public Team(String teamid, String name, String picture) {
		this.teamid = teamid;
		this.name = name;
		this.picture = picture;
	}
	
	public void setId(int id) {
		this._id = id;
	}
	public void setTeamid(String text){
		this.teamid = text;
	}
	public void setName(String text){
		this.name = text;
	}
	public void setPicture(String text){
		this.picture = text;
	}
	public void setGame(String text){
		this.game = text;
	}
	
	
	public int getId(){
		return this._id;
	}
	public String getTeamid(){
		return this.teamid;
	}
	public String getName(){
		return this.name;
	}
	public String getPicture(){
		return this.picture;
	}
	public String getGame(){
		return this.game;
	}

}