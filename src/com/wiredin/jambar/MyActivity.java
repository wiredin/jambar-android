package com.wiredin.jambar;

import com.actionbarsherlock.app.SherlockActivity;
import com.wiredin.jambar.services.GetGameTeamService;
import com.wiredin.jambar.services.GetTeamService;

import android.app.AlertDialog;
//import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MyActivity extends SherlockActivity {

	public Button enterBut;
	public EditText startCode;

//	private ProgressDialog dialog = null;

	// flag for Internet connection status
	Boolean isInternetPresent = false;

	ConnectionDetector cd;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.enter_activity);

		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		// Do your operation
		// setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);

		// this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		// text view label
		TextView txtWel = (TextView) findViewById(R.id.welcome);

		// Loading Font Face
		Typeface tf = Typeface.createFromAsset(getAssets(),
				"fonts/ProximaNova-Regular.otf");
		Typeface tf1 = Typeface.createFromAsset(getAssets(),
				"fonts/ProximaNova-Bold.otf");

		// Applying font
		txtWel.setTypeface(tf);

		getSupportActionBar().hide();

		cd = new ConnectionDetector(getApplicationContext());

		startCode = (EditText) findViewById(R.id.startCode);
		startCode.setTypeface(tf);

		enterBut = (Button) findViewById(R.id.enterCodeBut);
		enterBut.setTypeface(tf1);

		enterBut.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (startCode.getText().toString().isEmpty()) {
					Toast ts = Toast.makeText(getBaseContext(),
							"Start Game Code is Empty !", Toast.LENGTH_SHORT);
					ts.show();
				} else {
					// request team code
					isInternetPresent = cd.isConnectingToInternet();

					// check for Internet status
					if (isInternetPresent) {
						// Internet Connection is Present
						// make HTTP requests
						GetGameTeamService serviceFollowed = new GetGameTeamService(
								startCode.getText().toString(), MyActivity.this);
						serviceFollowed.execute();
					} else {
						// Internet connection is not present
						// Ask user to connect to Internet

						AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
								MyActivity.this);

						alertDialogBuilder.setTitle("Network Status");

						alertDialogBuilder
								.setMessage("Please check your network status.")
								.setCancelable(false)
								.setNegativeButton("OK",
										new DialogInterface.OnClickListener() {

											@Override
											public void onClick(
													DialogInterface dialog,
													int which) {
												// TODO Auto-generated method
												// stub

												dialog.dismiss();

											}
										});

						AlertDialog alertDialog = alertDialogBuilder.create();

						// show it
						alertDialog.show();

						// Toast ts = Toast.makeText(getBaseContext(),
						// "Sorry Please Your Internet Connection",
						// Toast.LENGTH_SHORT);
						// ts.show();
					}

				}
			}
		});

	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// TODO Auto-generated method stub
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
		return true;
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();

	}

}
