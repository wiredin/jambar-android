package com.wiredin.jambar.services;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

import com.wiredin.jambar.R;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class StartAnswerService extends AsyncTask<Void, Void, String> {
	ProgressDialog progressDialog;
	Context context;
	String teamid;
	String questionid;
	String gameid;
	String answer;
	
	public StartAnswerService(String teamid, String questionid, String gameid, String answer, Context context){
		this.context = context;
		this.teamid = teamid;
		this.questionid = questionid;
		this.gameid = gameid;
		this.answer = answer;
	}
	
	protected String getASCIIContentFromEntity(HttpEntity entity)
			throws IllegalStateException, IOException {
		InputStream in = entity.getContent();

		StringBuffer out = new StringBuffer();
		int n = 1;
		while (n > 0) {
			byte[] b = new byte[4096];
			n = in.read(b);

			if (n > 0)
				out.append(new String(b, 0, n));
		}

		return out.toString();
	}
	
	@Override
    protected void onPreExecute()
    {
        
		super.onPreExecute();
        //do initialization of required objects objects here 
		Log.d("Hadian", "Running Start Answering Question: "+questionid);
//		progressDialog = new ProgressDialog(this.context);
//		progressDialog.setTitle("Jambar"); //title
//		progressDialog.setMessage("Updating.."); // message
//		progressDialog.setCancelable(false);
//		progressDialog.show();

    };

	@Override
	protected String doInBackground(Void... params) {
		HttpClient httpClient = new DefaultHttpClient();
		HttpContext localContext = new BasicHttpContext();
		HttpPost httppost = new HttpPost(
				context.getResources().getString(R.string.webservice_domain) + "startAnswer");
		String text = null;
		try {
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
	        nameValuePairs.add(new BasicNameValuePair("teamid", this.teamid));
	        nameValuePairs.add(new BasicNameValuePair("questionid", this.questionid));
	        nameValuePairs.add(new BasicNameValuePair("gameid", this.gameid));
	        nameValuePairs.add(new BasicNameValuePair("answer", this.answer));
	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
	        
			HttpResponse response = httpClient.execute(httppost, localContext);

			HttpEntity entity = response.getEntity();

			text = getASCIIContentFromEntity(entity);

		} catch (Exception e) {
			return e.getLocalizedMessage();
		}

		return text;
	}

	protected void onPostExecute(String results) {
		if (results != null) {
			
			Log.d("Hadian", "Jadi dgn result: "+results);
//			progressDialog.dismiss();
			
		}
	}
	
}
