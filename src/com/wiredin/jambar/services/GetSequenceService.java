package com.wiredin.jambar.services;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONObject;

import com.wiredin.jambar.R;
import com.wiredin.jambar.dataclass.DatabaseHandler;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Log;

public class GetSequenceService extends AsyncTask<Void, Void, String> {
	ProgressDialog progressDialog;
	Context context;
	String id;

	public GetSequenceService(String code, Context context) {
		this.context = context;
		this.id = code;
	}

	protected String getASCIIContentFromEntity(HttpEntity entity)
			throws IllegalStateException, IOException {
		InputStream in = entity.getContent();

		StringBuffer out = new StringBuffer();
		int n = 1;
		while (n > 0) {
			byte[] b = new byte[4096];
			n = in.read(b);

			if (n > 0)
				out.append(new String(b, 0, n));
		}

		return out.toString();
	}

	@Override
	protected void onPreExecute() {

		super.onPreExecute();
		// do initialization of required objects objects here
		Log.d("Hadian", "Running Get Sequence Services");
//		progressDialog = new ProgressDialog(this.context);
//		progressDialog.setTitle("Jambar"); // title
//		progressDialog.setMessage("Installing Question.."); // message
//		progressDialog.setCancelable(false);
//		progressDialog.show();

	};

	@Override
	protected String doInBackground(Void... params) {
		HttpClient httpClient = new DefaultHttpClient();
		HttpContext localContext = new BasicHttpContext();
		HttpPost httppost = new HttpPost(context.getResources().getString(
				R.string.webservice_domain)
				+ "getSequence");
		String text = null;
		try {
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("gameid", this.id));
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			HttpResponse response = httpClient.execute(httppost, localContext);

			HttpEntity entity = response.getEntity();

			text = getASCIIContentFromEntity(entity);

		} catch (Exception e) {
			return e.getLocalizedMessage();
		}

		return text;
	}

	protected void onPostExecute(String results) {
		if (results != null) {

			Log.d("Hadian", "Jadi dgn result: " + results);

			DatabaseHandler dbHandler = new DatabaseHandler(context, null,
					null, 2);
			dbHandler.deleteAllSequence();

			// getfeaturedsales
			JSONObject[] objects = null;
			try {
				String data = results;
				JSONArray array = new JSONArray(data);
				objects = new JSONObject[array.length()];

				for (int i = 0; i < array.length(); i++) {

					objects[i] = array.getJSONObject(i);

					dbHandler.addSequence(objects[i].getString("id"),
							objects[i].getString("title"),
							objects[i].getString("notification"),
							objects[i].getString("start_req"),
							objects[i].getString("stop_req"),
							objects[i].getString("start_point"),
							objects[i].getString("stop_point"),
							objects[i].getString("start_code"),
							objects[i].getString("stop_code"),
							objects[i].getString("start_lat"),
							objects[i].getString("start_long"),
							objects[i].getString("start_radius"),
							objects[i].getString("stop_lat"),
							objects[i].getString("stop_long"),
							objects[i].getString("stop_radius"),
							objects[i].getString("start_questionid"),
							objects[i].getString("start_questiontime"),
							objects[i].getString("stop_questionid"),
							objects[i].getString("stop_questiontime"));

				}

//				Intent myIntent = new Intent((TeamSettingActivity) context,
//						OverViewGameActivity.class);
//				Log.d("fared", "open over view game activity");
//				context.startActivity(myIntent);
//				((Activity) context).finish();
				
				GetSequenceQuestionService service = new GetSequenceQuestionService(this.id,context);
				service.execute();

			} catch (Exception e) {
				Log.d("Hadian", "Error: " + e);
				progressDialog.dismiss();
				showError();
			}

		}
	}

	public void showError() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);
		// black
		alertDialogBuilder.setInverseBackgroundForced(true);

		// set title
		alertDialogBuilder.setTitle("Jambar");

		// set dialog message
		alertDialogBuilder.setMessage("Error").setCancelable(true)
				.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}
}
