package com.wiredin.jambar.services;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONObject;

import com.wiredin.jambar.ChooseTeam;
import com.wiredin.jambar.R;
import com.wiredin.jambar.TeamSettingActivity;
import com.wiredin.jambar.dataclass.DatabaseHandler;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

public class GetGameTeamService extends AsyncTask<Void, Void, String> {
	ProgressDialog progressDialog;
	Context context;
	String code;
	
	public GetGameTeamService(String code, Context context){
		this.context = context;
		this.code = code;
	}
	
	protected String getASCIIContentFromEntity(HttpEntity entity)
			throws IllegalStateException, IOException {
		InputStream in = entity.getContent();

		StringBuffer out = new StringBuffer();
		int n = 1;
		while (n > 0) {
			byte[] b = new byte[4096];
			n = in.read(b);

			if (n > 0)
				out.append(new String(b, 0, n));
		}

		return out.toString();
	}
	
	@Override
    protected void onPreExecute()
    {
        
		super.onPreExecute();
        //do initialization of required objects objects here 
		Log.d("Hadian", "Running Get Game Team Services");
		progressDialog = new ProgressDialog(this.context);
		progressDialog.setTitle("Jambar"); //title
		progressDialog.setMessage("Loading Team.."); // message
		progressDialog.setCancelable(false);
		progressDialog.show();

    };

	@Override
	protected String doInBackground(Void... params) {
		HttpClient httpClient = new DefaultHttpClient();
		HttpContext localContext = new BasicHttpContext();
		HttpPost httppost = new HttpPost(
				context.getResources().getString(R.string.webservice_domain) + "getGameTeam");
		String text = null;
		try {
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
	        nameValuePairs.add(new BasicNameValuePair("code", this.code));
	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
	        
			HttpResponse response = httpClient.execute(httppost, localContext);

			HttpEntity entity = response.getEntity();

			text = getASCIIContentFromEntity(entity);

		} catch (Exception e) {
			return e.getLocalizedMessage();
		}

		return text;
	}

	protected void onPostExecute(String results) {
		if (results != null) {
			
			Log.d("Hadian", "Jadi dgn result: "+results);
			progressDialog.dismiss();
			
			DatabaseHandler dbHandler = new DatabaseHandler(context, null, null, 1);
			dbHandler.deleteGT();

			// getfeaturedsales
			JSONObject[] objects = null;
			String id = null;
			try {
				String data = results;
				JSONArray array = new JSONArray(data);
				objects = new JSONObject[array.length()];

				for (int i = 0; i < array.length(); i++) {
					
					objects[i] = array.getJSONObject(i);
					
					dbHandler.addGT(objects[i].getString("id"), objects[i].getString("name"),
							objects[i].getString("teamcode"));
					id = objects[i].getString("id");

				}

			} catch (Exception e) {
				Log.d("Hadian", "Error: " + e);
				showError("Server Error!");
			} finally {
				if(id != null){
//					GetMemberService serviceFollowed = new GetMemberService(id, context);
//					serviceFollowed.execute();
					Intent myIntent = new Intent(context, ChooseTeam.class);
					Log.d("fared", "open team setting activity");
					context.startActivity(myIntent);
					((Activity) context).finish();
				} else {
					showError("Wrong Code Inserted!");
				}
			}
			
		}
	}
	
	public void showError(String msg) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);
		//black
		alertDialogBuilder.setInverseBackgroundForced(true);
				
		// set title
		alertDialogBuilder.setTitle("Jambar");

		// set dialog message
		alertDialogBuilder
				.setMessage(msg)
				.setCancelable(true)
				.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}
}
