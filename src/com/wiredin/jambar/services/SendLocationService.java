package com.wiredin.jambar.services;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

import com.wiredin.jambar.R;
import com.wiredin.jambar.dataclass.DatabaseHandler;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

public class SendLocationService extends AsyncTask<Void, Void, String> {
	ProgressDialog progressDialog;
	Context context;
	String latitude = null;
	String longitude = null;

	public SendLocationService(Context context, String latitude,
			String longitude) {
		this.context = context;
		this.latitude = latitude;
		this.longitude = longitude;
	}

	protected String getASCIIContentFromEntity(HttpEntity entity)
			throws IllegalStateException, IOException {
		InputStream in = entity.getContent();

		StringBuffer out = new StringBuffer();
		int n = 1;
		while (n > 0) {
			byte[] b = new byte[4096];
			n = in.read(b);

			if (n > 0)
				out.append(new String(b, 0, n));
		}

		return out.toString();
	}

	@Override
	protected void onPreExecute() {

		super.onPreExecute();
		// do initialization of required objects objects here
		// Log.d("Hadian", "Running Send Location");

	};

	@Override
	protected String doInBackground(Void... params) {

		DatabaseHandler dbHandler = new DatabaseHandler(context, null, null, 1);

		HttpClient httpClient = new DefaultHttpClient();
		HttpContext localContext = new BasicHttpContext();
		HttpPost httppost = new HttpPost(context.getResources().getString(
				R.string.webservice_domain)
				+ "updateTeamLocation");
		String text = null;
		try {
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("latitude", latitude));
			nameValuePairs.add(new BasicNameValuePair("longitude", longitude));
			nameValuePairs.add(new BasicNameValuePair("teamid", dbHandler
					.getTeam().getTeamid()));
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			HttpResponse response = httpClient.execute(httppost, localContext);

			HttpEntity entity = response.getEntity();

			text = getASCIIContentFromEntity(entity);

		} catch (Exception e) {
			return e.getLocalizedMessage();
		}

		return text;
	}

	protected void onPostExecute(String results) {
		if (results != null) {

			// Log.d("Hadian", "Jadi dgn result: " + results);

		}
	}

}
