package com.wiredin.jambar.services;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.wiredin.jambar.R;
import com.wiredin.jambar.dataclass.DatabaseHandler;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.util.Log;

public class RegisterPushNoty extends AsyncTask<Void, Void, String> {
	
	ProgressDialog progressDialog;
	Context context;
	String regid;
	GoogleCloudMessaging gcm;
	
	String SENDER_ID = "433954820812";
	
	public RegisterPushNoty(Context context,GoogleCloudMessaging gcm){
		this.context = context;
		this.gcm = gcm;
	}
	
	protected String getASCIIContentFromEntity(HttpEntity entity)
			throws IllegalStateException, IOException {
		InputStream in = entity.getContent();

		StringBuffer out = new StringBuffer();
		int n = 1;
		while (n > 0) {
			byte[] b = new byte[4096];
			n = in.read(b);

			if (n > 0)
				out.append(new String(b, 0, n));
		}

		return out.toString();
	}
	
	@Override
    protected void onPreExecute()
    {
        
		super.onPreExecute();
        //do initialization of required objects objects here 
		Log.d("GCM", "Running Register");

    };

	@Override
	protected String doInBackground(Void... params) {
		
		DatabaseHandler dbHandler = new DatabaseHandler(context, null, null, 1);
		
		HttpClient httpClient = new DefaultHttpClient();
		HttpContext localContext = new BasicHttpContext();
		HttpPost httppost = new HttpPost(
				context.getResources().getString(R.string.webservice_domain) + "registerPushNoty");
		String text = null;
		try {
			if (gcm == null) {
                gcm = GoogleCloudMessaging.getInstance(context);
            }
            regid = gcm.register(SENDER_ID);
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
	        nameValuePairs.add(new BasicNameValuePair("teamid", dbHandler.getTeam().getTeamid()));
	        nameValuePairs.add(new BasicNameValuePair("gcm_regid", this.regid));
	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
	        
			HttpResponse response = httpClient.execute(httppost, localContext);

			HttpEntity entity = response.getEntity();

			text = getASCIIContentFromEntity(entity);

		} catch (Exception e) {
			return e.getLocalizedMessage();
		}

		return text;
	}

	protected void onPostExecute(String results) {
		if (results != null) {
			
			Log.d("GCM", "Jadi dgn result: "+results+" : "+regid);
			//progressDialog.dismiss();

		}
		
		storeRegistrationId(context,regid);
	}
	
	private void storeRegistrationId(Context context, String regId) {
	    DatabaseHandler dbHandler = new DatabaseHandler(context,null,null,2);
	    dbHandler.deleteGCM();
	    dbHandler.addGCM(regId,String.valueOf(getAppVersion(context)));
	}
	
	private static int getAppVersion(Context context) {
	    try {
	        PackageInfo packageInfo = context.getPackageManager()
	                .getPackageInfo(context.getPackageName(), 0);
	        return packageInfo.versionCode;
	    } catch (NameNotFoundException e) {
	        // should never happen
	        throw new RuntimeException("Could not get package name: " + e);
	    }
	}
	
}
