package com.wiredin.jambar.services;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
//import java.util.ArrayList;
import java.util.List;

import com.wiredin.jambar.OverViewGameActivity;
import com.wiredin.jambar.R;
import com.wiredin.jambar.TeamSettingActivity;
import com.wiredin.jambar.dataclass.DatabaseHandler;
import com.wiredin.jambar.dataclass.Question2;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

public class DownloadImageService extends AsyncTask<Void, Void, String> {
	ProgressDialog progressDialog;
	Context context;
	String id;
	DatabaseHandler dbHandler;
	List<Question2> questions;
	List<Question2> sequenceQuestion;
	List<Bitmap> images;
	List<String> names;

	public DownloadImageService(String code, Context context) {
		this.context = context;
		this.id = code;
		dbHandler = new DatabaseHandler(context, null, null, 2);
	}

	@Override
	protected void onPreExecute() {

		super.onPreExecute();
		// do initialization of required objects objects here
		Log.d("Hadian", "Running Download Images");
		progressDialog = new ProgressDialog(this.context);
		progressDialog.setTitle("Jambar"); // title
		progressDialog.setMessage("Downloading.."); // message
		progressDialog.setCancelable(false);
		progressDialog.show();

	};

	@Override
	protected String doInBackground(Void... params) {
		
		questions = dbHandler.getAllQuestion2();
		
		sequenceQuestion = dbHandler.getSeqAllQuestion();
		
//		images = new ArrayList<Bitmap>();
//		names = new ArrayList<String>();
		
		Bitmap img;
		
		for (Question2 question: questions){
			if (!(question.getAnswer2().equals(""))) {
				img = getBitmapFromURL(context.getResources().getString(R.string.imageurl) +question.getAnswer2());
				if (img != null){
					saveFile(context,img,question.getAnswer2());
				}
			}
		}
		
		for (Question2 question: sequenceQuestion){
			if (!(question.getAnswer2().equals(""))) {
				img = getBitmapFromURL(context.getResources().getString(R.string.imageurl) +question.getAnswer2());
				if (img != null){
					saveFile(context,img,question.getAnswer2());
				}
			}
		}
		
		String text = null;
		return text;
	}

	protected void onPostExecute(String results) {
		
//		for(int i=0;i<images.size();i++){
//			saveFile(context,images.get(i),names.get(i));
//		}
		
		progressDialog.dismiss();
		
		Intent myIntent = new Intent((TeamSettingActivity) context, OverViewGameActivity.class);
		Log.d("fared", "open over view game activity");
		context.startActivity(myIntent);
		((Activity) context).finish();
		
	}
	
	public static Bitmap getBitmapFromURL(String src) {
	    try {
	        URL url = new URL(src);
	        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
	        connection.setDoInput(true);
	        connection.connect();
	        InputStream input = connection.getInputStream();
	        Bitmap myBitmap = BitmapFactory.decodeStream(input);
	        Log.d("Hadian", "Mendownload: " + src);
	        return myBitmap;
	    } catch (IOException e) {
	        e.printStackTrace();
	        Log.d("Hadian", "Null: " + src);
	        return null;
	    }
	}
	
	public static void saveFile(Context context, Bitmap b, String picName){ 
	    FileOutputStream fos; 
	    ByteArrayOutputStream outstream = new ByteArrayOutputStream();
	    try { 
	        fos = context.openFileOutput(picName, Context.MODE_PRIVATE); 
	        b.compress(Bitmap.CompressFormat.PNG, 100, outstream);
	        
	        Log.d("Hadian", "Saving: " + picName);
	        
	        byte[] byteArray = outstream.toByteArray();
            
	        fos.write(byteArray);
	        fos.close();
	    }  
	    catch (FileNotFoundException e) { 
	        Log.d("Hadian", "file not found"); 
	        e.printStackTrace(); 
	    }  
	    catch (IOException e) { 
	        Log.d("Hadian", "io exception"); 
	        e.printStackTrace(); 
	    } 

	}
	
}
