package com.wiredin.jambar.services;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONObject;

import com.wiredin.jambar.R;
import com.wiredin.jambar.dataclass.DatabaseHandler;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Log;

public class GetSequenceQuestionService extends AsyncTask<Void, Void, String> {
	ProgressDialog progressDialog;
	Context context;
	String id;
	
	public GetSequenceQuestionService(String code, Context context){
		this.context = context;
		this.id = code;
	}
	
	protected String getASCIIContentFromEntity(HttpEntity entity)
			throws IllegalStateException, IOException {
		InputStream in = entity.getContent();

		StringBuffer out = new StringBuffer();
		int n = 1;
		while (n > 0) {
			byte[] b = new byte[4096];
			n = in.read(b);

			if (n > 0)
				out.append(new String(b, 0, n));
		}

		return out.toString();
	}
	
	@Override
    protected void onPreExecute()
    {
        
		super.onPreExecute();
        //do initialization of required objects objects here 
		Log.d("Hadian", "Running Get Seq Question Services");
//		progressDialog = new ProgressDialog(this.context);
//		progressDialog.setTitle("Jambar"); //title
//		progressDialog.setMessage("Installing Question.."); // message
//		progressDialog.setCancelable(false);
//		progressDialog.show();

    };

	@Override
	protected String doInBackground(Void... params) {
		HttpClient httpClient = new DefaultHttpClient();
		HttpContext localContext = new BasicHttpContext();
		HttpPost httppost = new HttpPost(
				context.getResources().getString(R.string.webservice_domain) + "getSequenceQuestion");
		String text = null;
		try {
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
	        nameValuePairs.add(new BasicNameValuePair("gameid", this.id));
	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
	        
			HttpResponse response = httpClient.execute(httppost, localContext);

			HttpEntity entity = response.getEntity();

			text = getASCIIContentFromEntity(entity);

		} catch (Exception e) {
			return e.getLocalizedMessage();
		}

		return text;
	}

	protected void onPostExecute(String results) {
		if (results != null) {
			
			Log.d("Hadian", "Jadi dgn result: "+results);
//			progressDialog.dismiss();
			
			DatabaseHandler dbHandler = new DatabaseHandler(context, null, null, 2);
			dbHandler.deleteSeqQuestion();

			// getfeaturedsales
			JSONObject[] objects = null;
			try {
				String data = results;
				JSONArray array = new JSONArray(data);
				objects = new JSONObject[array.length()];

				for (int i = 0; i < array.length(); i++) {
					
					objects[i] = array.getJSONObject(i);
					
					dbHandler.addSeqQuestion(objects[i].getString("id"), objects[i].getString("type"),
							objects[i].getString("point_success"),objects[i].getString("point_fail"),
							objects[i].getString("trigger_event"),objects[i].getString("answer1"),
							objects[i].getString("answer2"),objects[i].getString("answer3"),
							objects[i].getString("answer4"), objects[i].getString("correct_answer"),
							objects[i].getString("question"),objects[i].getString("title"),
							objects[i].getString("login_id"),objects[i].getString("notify_wrong_text"),
							objects[i].getString("notify_correct_text"),objects[i].getString("timeout"),
							objects[i].getString("sequence_id"));

				}
				
				DownloadImageService service = new DownloadImageService(this.id,context);
				service.execute();
				
			} catch (Exception e) {
				Log.d("Hadian", "Error: " + e);
				showError();
			}
			
		}
	}
	
	public void showError() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);
		//black
		alertDialogBuilder.setInverseBackgroundForced(true);
				
		// set title
		alertDialogBuilder.setTitle("Jambar");

		// set dialog message
		alertDialogBuilder
				.setMessage("Error")
				.setCancelable(true)
				.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}
}
