package com.wiredin.jambar;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
//import android.widget.ImageView;
import android.widget.TextView;

public class MemberArrayAdapter extends ArrayAdapter<String> {

	private final Activity context;
	private final String[] names;
	
	int height;
	Typeface tf;
	Typeface tf1;

	public MemberArrayAdapter(Activity context, String[] names) {
		super(context, R.layout.list_member, names);
		this.context = context;
		this.names = names;
		
		
		tf = Typeface.createFromAsset(context.getAssets(), "fonts/ProximaNova-Regular.otf");
        tf1 = Typeface.createFromAsset(context.getAssets(), "fonts/ProximaNova-Bold.otf");
		

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View rowView = convertView;
		
		if (rowView == null) {
			LayoutInflater inflater = context.getLayoutInflater();
			rowView = inflater.inflate(R.layout.list_member, null);
			// ViewHolder viewHolder = new ViewHolder();
			// viewHolder.text = (TextView)
			// rowView.findViewById(R.id.secondLine);
			// viewHolder.image = (ImageView) rowView
			// .findViewById(R.id.shopicon);
			// rowView.setTag(viewHolder);
		}

		TextView firstLine = (TextView) rowView.findViewById(R.id.membersName);
		firstLine.setText(names[position]);
		firstLine.setTypeface(tf);
		ImageView selectedimage = (ImageView) rowView.findViewById(R.id.selectedImage);
		selectedimage.setVisibility(View.INVISIBLE);

		return rowView;
	}
	
	

}