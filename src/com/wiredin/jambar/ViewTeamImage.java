package com.wiredin.jambar;

import java.io.IOException;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.actionbarsherlock.app.SherlockActivity;
import com.wiredin.jambar.dataclass.DatabaseHandler;

public class ViewTeamImage extends SherlockActivity {

	DatabaseHandler dbHandler;
	Uri uriImgPath;
	String imgPath;
	BitmapFactory.Options bmOptions;
	ImageView teamImg;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_team_img);

		teamImg = (ImageView) findViewById(R.id.teamImg);

		dbHandler = new DatabaseHandler(this, null, null, 1);

		uriImgPath = Uri.parse(dbHandler.getTeamPicture(dbHandler.getTeam()
				.getTeamid()));

		imgPath = uriImgPath.toString();

		Log.d("fared", "image path string " + imgPath);

		ExifInterface exif = null;
		try {
			exif = new ExifInterface(imgPath);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String exifOrientation = exif
				.getAttribute(ExifInterface.TAG_ORIENTATION);
		Log.d("fared", "image uri " + imgPath);
		Log.d("fared", "orientation " + exifOrientation);

		int rotate = 0;

		if (exifOrientation.equals("6")) {
			rotate = 90;
			Log.d("fared", "rotate " + rotate);
		}

		teamImg.setImageURI(uriImgPath);
		teamImg.setRotation(rotate);
	}
	
	@SuppressLint("NewApi")
	private void setPic(String path, int rotate) {

		/* There isn't enough memory to open up more than a couple camera photos */
		/* So pre-scale the target bitmap into which the file is decoded */

		/* Get the size of the ImageView */
		int targetW = teamImg.getWidth();
		int targetH = teamImg.getHeight();

		Log.d("fared", "masuk setPic");

		/* Get the size of the image */
		bmOptions = new BitmapFactory.Options();
		bmOptions.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(path, bmOptions);
		int photoW = bmOptions.outWidth;
		int photoH = bmOptions.outHeight;
		Log.d("fared", "set bitmap");

		/* Figure out which way needs to be reduced less */
		int scaleFactor = 1;
		if ((targetW > 0) || (targetH > 0)) {
			scaleFactor = Math.min(photoW / targetW, photoH / targetH)+2;
		}

		/* Set bitmap options to scale the image decode target */
		bmOptions.inJustDecodeBounds = false;
		bmOptions.inSampleSize = scaleFactor;
		bmOptions.inPurgeable = true;

		/* Decode the JPEG file into a Bitmap */
		Bitmap bitmap = BitmapFactory.decodeFile(path, bmOptions);
		Log.d("fared", "bit map decode " + bitmap);

		/* Associate the Bitmap to the ImageView */
		teamImg.setImageBitmap(bitmap);
		teamImg.setRotation(rotate);
		teamImg.setVisibility(View.VISIBLE);
		
		bitmap = null;
	}

}
