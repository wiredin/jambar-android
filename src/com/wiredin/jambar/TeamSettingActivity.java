package com.wiredin.jambar;

import java.util.concurrent.atomic.AtomicInteger;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.wiredin.jambar.dataclass.DatabaseHandler;
import com.wiredin.jambar.services.RegisterPushNoty;
import com.wiredin.jambar.teamSetting.ChooseLeaderFragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.util.Log;

public class TeamSettingActivity extends SherlockFragmentActivity {

	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

	String SENDER_ID = "433954820812";
	String currentFragment = null;

	GoogleCloudMessaging gcm;
	AtomicInteger msgId = new AtomicInteger();
	SharedPreferences prefs;

	String regid;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.game_setting_activity);
		//setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		getSupportActionBar().hide();

		if (findViewById(R.id.teamSettingFrame) != null) {

			if (savedInstanceState != null) {
				return;
			}

			ChooseLeaderFragment chooseLeaderFragment = new ChooseLeaderFragment();
			// ChooseLeaderFragment.setArguments(getIntent().getExtras());
			this.overridePendingTransition(R.anim.slide_down,
					R.anim.slide_out_left);

			// Add the fragment to the 'fragment_container' FrameLayout
			getSupportFragmentManager().beginTransaction()
					.add(R.id.teamSettingFrame, chooseLeaderFragment).commit();
		}

		if (checkPlayServices()) {
			// If this check succeeds, proceed with normal processing.
			// Otherwise, prompt user to get valid Play Services APK.
			gcm = GoogleCloudMessaging.getInstance(this);
			regid = getRegistrationId(getApplicationContext());

			if (regid.isEmpty()) {
				registerInBackground();
				Log.d("Hadian", "Register Push");
			} else {
				registerInBackground();
				Log.d("Hadian", "Registered - " + regid);
			}
		} else {
			showError();
		}

		// if (savedInstanceState == null) {
		// // During initial setup, plug in the details fragment.
		// ChooseLeaderFragment cLeaderfragment = new ChooseLeaderFragment();
		// cLeaderfragment.setArguments(getIntent().getExtras());
		// getSupportFragmentManager().beginTransaction().add(R.id.game_container,
		// cLeaderfragment).commit();
		// }
	}

	private boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(this);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, this,
						PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				Log.i("Hadian", "This device is not supported.");
				finish();
			}
			return false;
		}
		return true;
	}

	public void showError() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		// black
		alertDialogBuilder.setInverseBackgroundForced(true);

		// set title
		alertDialogBuilder.setTitle("Jambar");

		// set dialog message
		alertDialogBuilder
				.setMessage(
						"Please Check Your Google Play Service to use this app")
				.setCancelable(true)
				.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						finish();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	public void setCurrentFragment(String frg) {
		this.currentFragment = frg;
	}

	@SuppressWarnings("static-access")
	private String getRegistrationId(Context context) {
		DatabaseHandler dbHandler = new DatabaseHandler(context, null, null, 2);
		String registrationId = dbHandler.getGCMRegId();
		if (registrationId == null) {
			Log.i("Hadian", "Registration not found.");
			return "";
		}

		int registeredVersion = Integer.parseInt(dbHandler.getGCMVersion());
		int currentVersion = this.getAppVersion(context);
		if (registeredVersion != currentVersion) {
			Log.i("Hadian", "App version changed.");
			return "";
		}
		return registrationId;
	}

	private static int getAppVersion(Context context) {
		try {
			PackageInfo packageInfo = context.getPackageManager()
					.getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (NameNotFoundException e) {
			// should never happen
			throw new RuntimeException("Could not get package name: " + e);
		}
	}

	private void registerInBackground() {
		RegisterPushNoty service = new RegisterPushNoty(
				getApplicationContext(), gcm);
		service.execute();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		

		if (currentFragment == "Leader") {

			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
					TeamSettingActivity.this);

			alertDialogBuilder.setTitle("Exit app "+ currentFragment);

			alertDialogBuilder
					.setMessage("Are you sure to exit this Jambar app ? :(")
					.setCancelable(false)
					.setNegativeButton("Yes :(",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method
									// stub

									dialog.dismiss();
									finish();
								}
							})
					.setPositiveButton("No :)",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method stub
									dialog.cancel();
								}
							});

			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();

		}else{
			super.onBackPressed();
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
	}

	
}
