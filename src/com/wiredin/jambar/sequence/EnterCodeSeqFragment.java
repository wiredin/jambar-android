package com.wiredin.jambar.sequence;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.wiredin.jambar.ConnectionDetector;
import com.wiredin.jambar.R;
import com.wiredin.jambar.TriggerOut;
import com.wiredin.jambar.dataclass.DatabaseHandler;
import com.wiredin.jambar.dataclass.Question2;
import com.wiredin.jambar.sequence.SequenceActivity;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.PowerManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class EnterCodeSeqFragment extends Fragment {

	Button nextBtn;
	Button skipBtn;
	EditText answerCode;
	TextView quesno;
	TextView question;
	DatabaseHandler dbHandler;
	String questid;
	int position,nextposition;
	String[] questids;
	Question2 nxtquest, currentquestion;

	// timer caller
	CountDownTimer cdt;
	int cdtStatus = 0;
	
	boolean isRunning;

	TextView chrono;
	ConnectionDetector cd;
	// flag for Internet connection status
	Boolean isInternetPresent = false;

	private final long interval = 1000;

	View layout;

	// broadcast
	BroadcastReceiver mybroadcast = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			Log.i("[BroadcastReceiver]", "MyReceiver");

			if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
				Log.i("[BroadcastReceiver]", "Screen ON");
				Toast.makeText(getActivity(), "Screen ON", Toast.LENGTH_SHORT)
						.show();
				if (cdtStatus == 1) {
					if (isRunning){
						skipAction();
					}
				}
			} else if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
				Log.i("[BroadcastReceiver]", "Screen OFF");
			}

		}
	};

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		// check internet
		cd = new ConnectionDetector(getActivity());
		isInternetPresent = cd.isConnectingToInternet();

		Bundle b = getArguments();
		if (b != null) {
			position = b.getInt("id");
			questids = b.getStringArray("questionids");
		}

		questid = questids[position];

		View view = inflater.inflate(R.layout.enter_code, container, false);

		// Toaster
		layout = inflater.inflate(R.layout.custom_toast,
				(ViewGroup) view.findViewById(R.id.toast_layout_root));

		// ScreenLock
		getActivity().registerReceiver(mybroadcast,
				new IntentFilter(Intent.ACTION_SCREEN_ON));
		getActivity().registerReceiver(mybroadcast,
				new IntentFilter(Intent.ACTION_SCREEN_OFF));

		dbHandler = new DatabaseHandler(getActivity(), null, null, 2);
		currentquestion = dbHandler.getSeqQuestionById(questid);

		// StartAnsweringQuestion
		// String totalQuestion = dbHandler.getGame().getTotal();
		//
		// if (isInternetPresent) {
		// StartAnswerService service = new StartAnswerService(dbHandler
		// .getTeam().getTeamid(), currentquestion.getQuestionid(),
		// dbHandler.getGame().getGameid(), "Ready", getActivity());
		// service.execute();
		// }

		// answering start locally
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss", Locale.US);
		Date date = new Date();

		dbHandler.addStartAnswer(currentquestion.getQuestionid(),
				dateFormat.format(date));
		Log.d("hadian", "date: " + dateFormat.format(date));

		// get next quest
		nxtquest = null;

		nextposition = position + 1;

		if (nextposition < questids.length) {
			nxtquest = dbHandler.getSeqQuestionById(questids[nextposition]);
			Log.d("Sequence", "next question: "+questids[nextposition]);
		}

		Typeface tf = Typeface.createFromAsset(getActivity().getAssets(),
				"fonts/ProximaNova-Regular.otf");
		// Typeface tf1 = Typeface.createFromAsset(getActivity().getAssets(),
		// "fonts/ProximaNova-Bold.otf");

		getActivity().getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

		chrono = (TextView) view.findViewById(R.id.chrono);

		question = (TextView) view.findViewById(R.id.question);
		question.setTypeface(tf);
		question.setText(currentquestion.getQuestion());

		quesno = (TextView) view.findViewById(R.id.ques_no);
		quesno.setGravity(Gravity.CENTER);
		quesno.setTypeface(tf);
		quesno.setText(String.valueOf(questid));

		TextView quesno = (TextView) view.findViewById(R.id.ques_no);
		quesno.setGravity(Gravity.CENTER);

		ImageView questImg = (ImageView) view.findViewById(R.id.imageView2);

		if (!currentquestion.getAnswer2().equals("")) {
			questImg.setImageBitmap(loadBitmap(getActivity(),
					currentquestion.getAnswer2()));
		} else {
			questImg.setVisibility(View.GONE);
		}

		// timer to check question timeout
		if (!currentquestion.getTimeout().equals("0")) {
			Log.d("Hadian", "Timeout: " + currentquestion.getTimeout());
			cdt = new CountDownTimer(Long.parseLong(currentquestion
					.getTimeout()) * 1000, interval) {

				@Override
				public void onTick(long millisUntilFinished) {
					// TODO Auto-generated method stub
					DecimalFormat formatter = new DecimalFormat("00");
					String minutes = formatter
							.format((int) ((millisUntilFinished / (1000 * 60)) % 60));
					String seconds = formatter
							.format((int) (millisUntilFinished / 1000) % 60);

					chrono.setText(minutes + ":" + seconds);
				}

				@SuppressWarnings("static-access")
				@Override
				public void onFinish() {
					// TODO Auto-generated method stub
					cdtStatus = 1;

					PowerManager powerManager = (PowerManager) getActivity()
							.getSystemService(getActivity().POWER_SERVICE);
					if (powerManager.isScreenOn()) {
						if (isRunning){
							skipAction();
						}
					}
				}

			};
			cdt.start();
		}

		nextBtn = (Button) ((SequenceActivity) getActivity())
				.findViewById(R.id.seq_next);
		nextBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Log.d("fared", "select from list tak null");
				endQuestion();
			}

		});

		skipBtn = (Button) ((SequenceActivity) getActivity())
				.findViewById(R.id.seq_skip);
		skipBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						getActivity());
				alertDialogBuilder.setTitle("Skip Question");

				alertDialogBuilder
						.setMessage(
								"Are you sure to skip this question? Note! if you skip you cannot answer it again")
						.setCancelable(false)
						.setPositiveButton("No",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										// TODO Auto-generated method stub
										dialog.cancel();
									}
								})
						.setNegativeButton("Yes",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										// TODO Auto-generated method stub

										skipAction();
									}
								});

				AlertDialog alertDialog = alertDialogBuilder.create();

				// show it
				alertDialog.show();
			}
		});
		return view;

	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		final InputMethodManager imm = (InputMethodManager) getActivity()
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();

	}

	private void endQuestion() {
		// answering start locally
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss", Locale.US);
		Date date = new Date();

		dbHandler.addStopAnswer(currentquestion.getQuestionid(), "", "True",
				dateFormat.format(date));

		// update total point
		int currentpoint = Integer.parseInt(dbHandler.getSummaryPoint());
		currentpoint += Integer.parseInt(currentquestion.getPoint_success());
		dbHandler.updateSummaryPoint(String.valueOf(currentpoint));

		if (currentquestion.getNotify_correct_text() != null) {
			Toast.makeText(getActivity(),
					currentquestion.getNotify_correct_text(), Toast.LENGTH_LONG)
					.show();
		}

		// if (isInternetPresent) {
		// SendAnswerService service = new SendAnswerService(dbHandler
		// .getTeam().getTeamid(), currentquestion.getQuestionid(),
		// dbHandler.getGame().getGameid(), answerCode.getText()
		// .toString(), getActivity());
		// service.execute();
		// }

		String finalQuesTime = (String) chrono.getText();

		Log.d("fared", "time stop" + finalQuesTime);

		if (cdt != null) {
			// stop the timer
			cdt.cancel();
		}
		
		TriggerOut trigger = new TriggerOut(getActivity(),
				((SequenceActivity) getActivity()).currentSeq(), questid);

		if (trigger.checkStop() == true) {
			nxtquest = null;
		}

		FragmentTransaction transaction = getFragmentManager()
				.beginTransaction();

		if (nxtquest != null) {
			String type = nxtquest.getType();

			if (type.equals("Obj")) {
				MultipleChoiceSeqFragment multiplechoiceFragment = new MultipleChoiceSeqFragment();

				Bundle args = new Bundle();
				args.putInt("id", nextposition);
				args.putStringArray("questionids", questids);
				multiplechoiceFragment.setArguments(args);

				transaction
						.replace(R.id.sequence_frame, multiplechoiceFragment);
			} else if (type.equals("Subj")) {
				SubjectiveSeqFragment subjectiveFragment = new SubjectiveSeqFragment();

				Bundle args = new Bundle();
				args.putInt("id", nextposition);
				args.putStringArray("questionids", questids);
				subjectiveFragment.setArguments(args);

				transaction.replace(R.id.sequence_frame, subjectiveFragment);
			} else if (type.equals("Pic")) {
				CompareImageSeqFragment compareImageFragment = new CompareImageSeqFragment();

				Bundle args = new Bundle();
				args.putInt("id", nextposition);
				args.putStringArray("questionids", questids);
				compareImageFragment.setArguments(args);

				transaction.replace(R.id.sequence_frame, compareImageFragment);
			} else if (type.equals("Code")) {
				EnterCodeSeqFragment enterCodeFragment = new EnterCodeSeqFragment();

				Bundle args = new Bundle();
				args.putInt("id", nextposition);
				args.putStringArray("questionids", questids);
				enterCodeFragment.setArguments(args);

				transaction.replace(R.id.sequence_frame, enterCodeFragment);
			} else if (type.equals("Vid")) {
				VideoQuesSeqFragment videoQuesFragment = new VideoQuesSeqFragment();

				Bundle args = new Bundle();
				args.putInt("id", nextposition);
				args.putStringArray("questionids", questids);
				videoQuesFragment.setArguments(args);

				transaction.replace(R.id.sequence_frame, videoQuesFragment);
			} else if (type.equals("QR")) {

			}

			transaction.addToBackStack(null);
			transaction
					.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
			transaction.commit();
			getActivity().unregisterReceiver(mybroadcast);

		} else {
			Log.d("fared", "open summary activity");

			getActivity().unregisterReceiver(mybroadcast);
			getActivity().finish();
		}

	}

	public static Bitmap loadBitmap(Context context, String picName) {
		Bitmap b = null;
		FileInputStream fis;
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = 3;
		try {
			fis = context.openFileInput(picName);
			b = BitmapFactory.decodeStream(fis, null, options);
			fis.close();

		} catch (FileNotFoundException e) {
			Log.d("Hadian", "file not found");
			e.printStackTrace();
		} catch (IOException e) {
			Log.d("Hadian", "io exception");
			e.printStackTrace();
		}
		return b;
	}

	public void skipAction() {
		// if (isInternetPresent) {
		// // Send the answer to admin for the
		// // checking
		// SendAnswerService service = new SendAnswerService(
		// dbHandler.getTeam()
		// .getTeamid(),
		// currentquestion
		// .getQuestionid(),
		// dbHandler.getGame()
		// .getGameid(),
		// "Skip", getActivity());
		// service.execute();
		// }

		// Add skip in database
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss", Locale.US);
		Date date = new Date();

		dbHandler.addStopAnswer(currentquestion.getQuestionid(), "Skip",
				"Skip", dateFormat.format(date));

		String finalQuesTime = (String) chrono.getText();

		Log.d("fared", "time stop" + finalQuesTime);

		if (cdt != null) {
			// stop the timer
			cdt.cancel();
		}

		// Add the fragment to the
		// 'fragment_container' FrameLayout
		FragmentTransaction transaction = getFragmentManager()
				.beginTransaction();

		if (nxtquest != null) {
			String type = nxtquest.getType();

			if (type.equals("Obj")) {
				MultipleChoiceSeqFragment multiplechoiceFragment = new MultipleChoiceSeqFragment();

				Bundle args = new Bundle();
				args.putInt("id", nextposition);
				args.putStringArray("questionids", questids);
				multiplechoiceFragment.setArguments(args);

				transaction
						.replace(R.id.sequence_frame, multiplechoiceFragment);
			} else if (type.equals("Subj")) {
				SubjectiveSeqFragment subjectiveFragment = new SubjectiveSeqFragment();

				Bundle args = new Bundle();
				args.putInt("id", nextposition);
				args.putStringArray("questionids", questids);
				subjectiveFragment.setArguments(args);

				transaction.replace(R.id.sequence_frame, subjectiveFragment);
			} else if (type.equals("Pic")) {
				CompareImageSeqFragment compareImageFragment = new CompareImageSeqFragment();

				Bundle args = new Bundle();
				args.putInt("id", nextposition);
				args.putStringArray("questionids", questids);
				compareImageFragment.setArguments(args);

				transaction.replace(R.id.sequence_frame, compareImageFragment);
			} else if (type.equals("Code")) {
				EnterCodeSeqFragment enterCodeFragment = new EnterCodeSeqFragment();

				Bundle args = new Bundle();
				args.putInt("id", nextposition);
				args.putStringArray("questionids", questids);
				enterCodeFragment.setArguments(args);
				transaction.replace(R.id.sequence_frame, enterCodeFragment);
			} else if (type.equals("Vid")) {
				VideoQuesSeqFragment videoQuesFragment = new VideoQuesSeqFragment();

				Bundle args = new Bundle();
				args.putInt("id", nextposition);
				args.putStringArray("questionids", questids);
				videoQuesFragment.setArguments(args);

				transaction.replace(R.id.sequence_frame, videoQuesFragment);
			} else if (type.equals("QR")) {

			}

			transaction.addToBackStack(null);
			transaction
					.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
			transaction.commit();
			getActivity().unregisterReceiver(mybroadcast);

		} else {
			Log.d("fared", "open summary activity");

			getActivity().unregisterReceiver(mybroadcast);
			getActivity().finish();
		}
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		isRunning = false;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		isRunning = true;
		
		if(cdtStatus == 1){
			skipAction();
		}
	}
	
	

}
