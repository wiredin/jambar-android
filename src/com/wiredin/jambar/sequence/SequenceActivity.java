package com.wiredin.jambar.sequence;

import java.util.List;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.wiredin.jambar.R;
import com.wiredin.jambar.dataclass.DatabaseHandler;
import com.wiredin.jambar.dataclass.Question2;

public class SequenceActivity extends SherlockFragmentActivity {

	String seqid;
	DatabaseHandler dbHandler;
	Question2 firstquestion;

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sequence_activity);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		getSupportActionBar().hide();

		dbHandler = new DatabaseHandler(this, null, null, 2);

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			seqid = extras.getString("id");
			Log.d("Hadian", "value recieve: " + seqid);
		}
		
		dbHandler.setSequenceDone(seqid);

		List<String> questionidlist = dbHandler.getSeqQuestionBySeqId(seqid);
		String[] questionids = new String[questionidlist.size()];

		for (int i = 0; i < questionidlist.size(); i++) {
			questionids[i] = questionidlist.get(i);
		}

		firstquestion = dbHandler.getSeqQuestionById(questionids[0]);

		if (findViewById(R.id.sequence_frame) != null) {

			if (savedInstanceState != null) {
				return;
			}

			if (firstquestion != null) {
				String type = firstquestion.getType();

				if (type.equals("Obj")) {
					MultipleChoiceSeqFragment multiplechoiceFragment = new MultipleChoiceSeqFragment();

					Bundle args = new Bundle();
					args.putInt("id", 0);
					args.putStringArray("questionids", questionids);
					multiplechoiceFragment.setArguments(args);

					// Add the fragment to the 'fragment_container' FrameLayout
					getSupportFragmentManager().beginTransaction()
							.add(R.id.sequence_frame, multiplechoiceFragment)
							.commit();
				} else if (type.equals("Subj")) {
					SubjectiveSeqFragment subjectiveFragment = new SubjectiveSeqFragment();

					Bundle args = new Bundle();
					args.putInt("id", 0);
					args.putStringArray("questionids", questionids);
					subjectiveFragment.setArguments(args);

					getSupportFragmentManager().beginTransaction()
							.add(R.id.sequence_frame, subjectiveFragment)
							.commit();
				} else if (type.equals("Pic")) {
					CompareImageSeqFragment compareImageFragment = new CompareImageSeqFragment();

					Bundle args = new Bundle();
					args.putInt("id", 0);
					args.putStringArray("questionids", questionids);
					compareImageFragment.setArguments(args);

					getSupportFragmentManager().beginTransaction()
							.add(R.id.sequence_frame, compareImageFragment)
							.commit();
				} else if (type.equals("Code")) {
					EnterCodeSeqFragment enterCodeFragment = new EnterCodeSeqFragment();

					Bundle args = new Bundle();
					args.putInt("id", 0);
					args.putStringArray("questionids", questionids);
					enterCodeFragment.setArguments(args);

					getSupportFragmentManager().beginTransaction()
							.add(R.id.sequence_frame, enterCodeFragment)
							.commit();
				} else if (type.equals("Vid")) {
					VideoQuesSeqFragment videoQuesFragment = new VideoQuesSeqFragment();

					Bundle args = new Bundle();
					args.putInt("id", 0);
					args.putStringArray("questionids", questionids);
					videoQuesFragment.setArguments(args);

					getSupportFragmentManager().beginTransaction()
							.add(R.id.sequence_frame, videoQuesFragment)
							.commit();
				} else if (type.equals("QR")) {

				}
			}
		}
	}
	
	public String currentSeq(){
		return seqid;
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}
	
	@Override
	public void onBackPressed() {
		
	}

}
