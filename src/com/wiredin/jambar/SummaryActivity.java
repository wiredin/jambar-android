package com.wiredin.jambar;

import java.io.IOException;
import java.util.List;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.wiredin.jambar.dataclass.Answer;
import com.wiredin.jambar.dataclass.DatabaseHandler;
import com.wiredin.jambar.services.StopGameService;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ListView;
import android.widget.TextView;

public class SummaryActivity extends SherlockActivity {

	ListView listView;
	ImageView bluredImg;
	TextView teamName, score;
	String imgPath;
	BitmapFactory.Options bmOptions;
	int screenWidth = 0;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.game_summary_activity);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		// getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		DatabaseHandler dbHandler = new DatabaseHandler(this, null, null, 1);

		imgPath = dbHandler.getTeamPicture(dbHandler.getTeam().getTeamid());

		// stop the game
		StopGameService serviceStart = new StopGameService(dbHandler.getTeam()
				.getTeamid(), this);
		serviceStart.execute();

		screenWidth = ImageUtils.getScreenWidth(this);

		listView = (ListView) findViewById(R.id.list);
//		bluredImg = (ImageView) findViewById(R.id.img_bg);
//		bluredImg.setAlpha(1f);

		teamName = (TextView) findViewById(R.id.text_team_name);

		teamName.setText(dbHandler.getTeam().getName());

		score = (TextView) findViewById(R.id.text_score);
		score.setText("Score: " + dbHandler.getSummaryPoint());
		
		int seqnum = 0;
		TextView correct = (TextView) findViewById(R.id.text_correct);
		try {
			seqnum = dbHandler.getSeqAllQuestion().size();
		} catch (Exception e){
			
		}
		String totalquestion = String.valueOf(Integer.parseInt(dbHandler
				.getGame().getTotal())+seqnum);
		correct.setText("Correct: " + dbHandler.getSummaryCorrect() + "/"
				+ totalquestion);

		List<Answer> answers = dbHandler.getAllAnswers();

		String[] values = new String[answers.size()];

		int l = 0;
		for (Answer answer : answers) {
			if (dbHandler.getQuestionByQuestionId2(answer.getQuestionid()) == null) {
				values[l] = dbHandler
						.getSeqQuestionById(answer.getQuestionid()).getTitle();
			} else {
				values[l] = dbHandler.getQuestionByQuestionId2(
						answer.getQuestionid()).getTitle();
			}
			l++;
		}

		String[] corrects = new String[answers.size()];

		int m = 0;
		for (Answer answer : answers) {
			corrects[m] = answer.getCorrect();
			m++;
		}

		SummaryArrayAdapter adapter = new SummaryArrayAdapter(this,
				R.layout.list_summary, values, corrects);

		// listView.setExpanded(true);

		int totalHeight = 0;

		for (int i = 0; i < adapter.getCount(); i++) {
			View mView = adapter.getView(i, null, listView);

			mView.measure(
					MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED),

					MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));

			totalHeight += mView.getMeasuredHeight();
			Log.w("HEIGHT" + i, String.valueOf(totalHeight));

		}

		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight + 450
				+ (listView.getDividerHeight() * (adapter.getCount() - 1));
		listView.setLayoutParams(params);
		listView.requestLayout();

		listView.setAdapter(adapter);

		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, final View view,
					int position, long id) {
				// final String item = (String)
				// parent.getItemAtPosition(position);

			}

		});

//		if (!imgPath.isEmpty()) {
//
//			Log.d("fared", "image path string :" + imgPath);
//
//			ExifInterface exif = null;
//			try {
//				exif = new ExifInterface(imgPath);
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			String exifOrientation = exif
//					.getAttribute(ExifInterface.TAG_ORIENTATION);
//			Log.d("fared", "image uri :" + imgPath);
//			Log.d("fared", "orientation :" + exifOrientation);
//
//			int rotate = 0;
//
//			if (exifOrientation.equals("6")) {
//				rotate = 90;
//				Log.d("fared", "rotate " + rotate);
//			}
//
//			// teamImg.setImageURI(uriImgPath);
//			// teamImg.setRotation(rotate);
//			if (!imgPath.equals(null)) {
//				setPic(imgPath, rotate);
//			}
//
//		} else {
//			
//		}
		
//		bluredImg.setImageResource(R.drawable.banner2);

	}

	private class SummaryArrayAdapter extends ArrayAdapter<String> {

		// HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();
		Activity ctx;
		String[] objects;
		String[] objects2;

		public SummaryArrayAdapter(Activity context, int textViewResourceId,
				String[] objects, String[] objects2) {
			super(context, textViewResourceId, objects);

			// tf = Typeface.createFromAsset(context.getAssets(), FONT);
			ctx = context;
			this.objects = objects;
			this.objects2 = objects2;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View rowView = convertView;

			if (rowView == null) {
				LayoutInflater inflater = ctx.getLayoutInflater();
				rowView = inflater
						.inflate(R.layout.list_summary, parent, false);
			}

			TextView firstLine = (TextView) rowView
					.findViewById(R.id.text_summary);
			firstLine.setText(objects[position]);
			// firstLine.setTypeface(tf);
			// TextView secondLine = (TextView) rowView
			// .findViewById(R.id.text_result);
			// secondLine.setText(objects2[position]);
			ImageView tick = (ImageView) rowView
					.findViewById(R.id.correctWrong);
			if (objects2[position].contains("True")) {
				tick.setImageResource(R.drawable.check);
			} else if (objects2[position].contains("False")) {
				tick.setImageResource(R.drawable.delete);
			} else {
				tick.setImageResource(R.drawable.skip);
			}

			return rowView;

		}

	}

	private void setPic(String path, int rotate) {

		/* There isn't enough memory to open up more than a couple camera photos */
		/* So pre-scale the target bitmap into which the file is decoded */

		/* Get the size of the ImageView */

		Log.d("fared", "masuk setPic");

		/* Get the size of the image */
		bmOptions = new BitmapFactory.Options();
		bmOptions.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(path, bmOptions);
		// int photoW = bmOptions.outWidth;
		// int photoH = bmOptions.outHeight;
		Log.d("fared", "set bitmap");

		/* Figure out which way needs to be reduced less */
		// if ((targetW > 0) || (targetH > 0)) {
		// scaleFactor = Math.min(photoW / targetW, photoH / targetH) + 2;
		// }

		/* Set bitmap options to scale the image decode target */
		bmOptions.inJustDecodeBounds = false;
		bmOptions.inSampleSize = 2;
		bmOptions.inPurgeable = true;

		/* Decode the JPEG file into a Bitmap */
		Bitmap bitmap = BitmapFactory.decodeFile(path, bmOptions);
		Log.d("fared", "bit map decode " + bitmap);

//		if (bitmap != null) {
//			Bitmap newImg = Blur.fastblur(SummaryActivity.this, bitmap, 12);
//
//			/* Associate the Bitmap to the ImageView */
//			bluredImg.setImageBitmap(bitmap);
//			bluredImg.setRotation(rotate);
//
//			Bitmap bmpBlurred = newImg;
//			bmpBlurred = Bitmap
//					.createScaledBitmap(
//							bmpBlurred,
//							screenWidth,
//							(int) (bmpBlurred.getHeight()
//									* ((float) screenWidth) / (float) bmpBlurred
//									.getWidth()), false);
//
//			bluredImg.setImageBitmap(bmpBlurred);
//			bluredImg.setScaleType(ScaleType.CENTER_CROP);
//		}
		
		bluredImg.setImageBitmap(bitmap);
		bluredImg.setScaleType(ScaleType.CENTER_CROP);

		bitmap = null;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub

		// menu.add("Print")
		// .setIcon(R.drawable.icon_print)
		// .setShowAsAction(
		// MenuItem.SHOW_AS_ACTION_IF_ROOM
		// | MenuItem.SHOW_AS_ACTION_WITH_TEXT);

		return true;
	}

	@Override
	public void onBackPressed() {

	}

}
