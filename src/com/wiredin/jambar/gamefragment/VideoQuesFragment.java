package com.wiredin.jambar.gamefragment;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import com.wiredin.jambar.ConnectionDetector;
import com.wiredin.jambar.GameActivity;
import com.wiredin.jambar.R;
import com.wiredin.jambar.SummaryActivity;
import com.wiredin.jambar.dataclass.DatabaseHandler;
import com.wiredin.jambar.dataclass.Question2;
import com.wiredin.jambar.services.SendAnswerService;
import com.wiredin.jambar.services.StartAnswerService;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class VideoQuesFragment extends Fragment {

	Button nxtBtn;
	Button skipBtn;
	TextView quesno;
	TextView question;
	DatabaseHandler dbHandler;
	int questid;
	Question2 nxtquest, currentquestion;
	ConnectionDetector cd;
	// flag for Internet connection status
	Boolean isInternetPresent = false;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		// check internet
		cd = new ConnectionDetector(getActivity());
		isInternetPresent = cd.isConnectingToInternet();

		Bundle b = getArguments();
		if (b != null)
			questid = b.getInt("id");

		// StartAnsweringQuestion
		dbHandler = new DatabaseHandler(getActivity(), null, null, 2);
		currentquestion = dbHandler.getQuestionByTableId2(questid);

		String totalQuestion = dbHandler.getGame().getTotal();
		((GameActivity) getActivity()).changeQuesLeft(questid + " / "
				+ totalQuestion);

		if (isInternetPresent) {
			StartAnswerService service = new StartAnswerService(dbHandler
					.getTeam().getTeamid(), currentquestion.getQuestionid(),
					dbHandler.getGame().getGameid(), "Ready", getActivity());
			service.execute();
		}

		// get next quest
		nxtquest = dbHandler.getQuestionByTableId2(questid + 1);

		View rootView = inflater.inflate(R.layout.video_fragment, container,
				false);

		// getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		Typeface tf = Typeface.createFromAsset(getActivity().getAssets(),
				"fonts/ProximaNova-Regular.otf");
		// Typeface tf1 = Typeface.createFromAsset(getActivity().getAssets(),
		// "fonts/ProximaNova-Bold.otf");

		TextView quesno = (TextView) rootView.findViewById(R.id.ques_no);
		quesno.setGravity(Gravity.CENTER);

		question = (TextView) rootView.findViewById(R.id.question);
		question.setTypeface(tf);
		question.setText(currentquestion.getQuestion());

		quesno = (TextView) rootView.findViewById(R.id.ques_no);
		quesno.setGravity(Gravity.CENTER);
		quesno.setTypeface(tf);
		quesno.setText(String.valueOf(questid));

		nxtBtn = (Button) ((GameActivity) getActivity())
				.findViewById(R.id.next);
		skipBtn = (Button) ((GameActivity) getActivity())
				.findViewById(R.id.skip);

		nxtBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				// Add the fragment to the 'fragment_container' FrameLayout
				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();

				if (nxtquest != null) {
					String type = nxtquest.getType();

					if (type.equals("Obj")) {
						MultipleChoiceFragment multiplechoiceFragment = new MultipleChoiceFragment();

						Bundle args = new Bundle();
						args.putInt("id", nxtquest.getId());
						multiplechoiceFragment.setArguments(args);

						transaction.replace(R.id.game_container,
								multiplechoiceFragment);
					} else if (type.equals("Subj")) {
						SubjectiveFragment subjectiveFragment = new SubjectiveFragment();

						Bundle args = new Bundle();
						args.putInt("id", nxtquest.getId());
						subjectiveFragment.setArguments(args);

						transaction.replace(R.id.game_container,
								subjectiveFragment);
					} else if (type.equals("Pic")) {
						CompareImageFragment compareImageFragment = new CompareImageFragment();

						Bundle args = new Bundle();
						args.putInt("id", nxtquest.getId());
						compareImageFragment.setArguments(args);

						transaction.replace(R.id.game_container,
								compareImageFragment);
					} else if (type.equals("Code")) {
						EnterCodeFragment enterCodeFragment = new EnterCodeFragment();

						Bundle args = new Bundle();
						args.putInt("id", nxtquest.getId());
						enterCodeFragment.setArguments(args);

						transaction.replace(R.id.game_container,
								enterCodeFragment);
					} else if (type.equals("Vid")) {
						VideoQuesFragment videoQuesFragment = new VideoQuesFragment();

						Bundle args = new Bundle();
						args.putInt("id", nxtquest.getId());
						videoQuesFragment.setArguments(args);

						transaction.replace(R.id.game_container,
								videoQuesFragment);
					} else if (type.equals("QR")) {

					}

					transaction.addToBackStack(null);
					transaction
							.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
					transaction.commit();

				} else {
					Intent myIntent = new Intent(getActivity(),
							SummaryActivity.class);
					Log.d("fared", "open summary activity");
					getActivity().startActivity(myIntent);
					((GameActivity) getActivity()).setStopLocation();
					getActivity().finish();
				}
			}
		});

		skipBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						getActivity());
				alertDialogBuilder.setTitle("Skip Question");

				alertDialogBuilder
						.setMessage(
								"Are you sure to skip this question? Note! if you skip you cannot answer it again")
						.setCancelable(false)
						.setPositiveButton("No",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										// TODO Auto-generated method stub
										dialog.cancel();
									}
								})
						.setNegativeButton("Yes",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										// TODO Auto-generated method stub

										if (isInternetPresent) {
											// Send the answer to admin for the
											// checking
											SendAnswerService service = new SendAnswerService(
													dbHandler.getTeam()
															.getTeamid(),
													currentquestion
															.getQuestionid(),
													dbHandler.getGame()
															.getGameid(),
													"Skip", getActivity());
											service.execute();
										}

										// Add the fragment to the
										// 'fragment_container' FrameLayout
										FragmentTransaction transaction = getFragmentManager()
												.beginTransaction();

										if (nxtquest != null) {
											String type = nxtquest.getType();

											if (type.equals("Obj")) {
												MultipleChoiceFragment multiplechoiceFragment = new MultipleChoiceFragment();

												Bundle args = new Bundle();
												args.putInt("id",
														nxtquest.getId());
												multiplechoiceFragment
														.setArguments(args);

												transaction.replace(
														R.id.game_container,
														multiplechoiceFragment);
											} else if (type.equals("Subj")) {
												SubjectiveFragment subjectiveFragment = new SubjectiveFragment();

												Bundle args = new Bundle();
												args.putInt("id",
														nxtquest.getId());
												subjectiveFragment
														.setArguments(args);

												transaction.replace(
														R.id.game_container,
														subjectiveFragment);
											} else if (type.equals("Pic")) {
												CompareImageFragment compareImageFragment = new CompareImageFragment();

												Bundle args = new Bundle();
												args.putInt("id",
														nxtquest.getId());
												compareImageFragment
														.setArguments(args);

												transaction.replace(
														R.id.game_container,
														compareImageFragment);
											} else if (type.equals("Code")) {
												EnterCodeFragment enterCodeFragment = new EnterCodeFragment();

												Bundle args = new Bundle();
												args.putInt("id",
														nxtquest.getId());
												enterCodeFragment
														.setArguments(args);

												transaction.replace(
														R.id.game_container,
														enterCodeFragment);
											} else if (type.equals("Vid")) {
												VideoQuesFragment videoQuesFragment = new VideoQuesFragment();

												Bundle args = new Bundle();
												args.putInt("id",
														nxtquest.getId());
												videoQuesFragment
														.setArguments(args);

												transaction.replace(
														R.id.game_container,
														videoQuesFragment);
											} else if (type.equals("QR")) {

											}

											transaction.addToBackStack(null);
											transaction
													.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
											transaction.commit();

										} else {
											Intent myIntent = new Intent(
													getActivity(),
													SummaryActivity.class);
											Log.d("fared",
													"open summary activity");
											getActivity().startActivity(
													myIntent);
											((GameActivity) getActivity())
													.setStopLocation();
											getActivity().finish();
										}
									}
								});

				AlertDialog alertDialog = alertDialogBuilder.create();

				// show it
				alertDialog.show();
			}
		});

		return rootView;
	}
	
	public static Bitmap loadBitmap(Context context, String picName) {
		Bitmap b = null;
		FileInputStream fis;
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = 3;
		try {
			fis = context.openFileInput(picName);
			b = BitmapFactory.decodeStream(fis, null, options);
			fis.close();

		} catch (FileNotFoundException e) {
			Log.d("Hadian", "file not found");
			e.printStackTrace();
		} catch (IOException e) {
			Log.d("Hadian", "io exception");
			e.printStackTrace();
		}
		return b;
	}

}
