package com.wiredin.jambar.gamefragment;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import com.wiredin.jambar.ConnectionDetector;
import com.wiredin.jambar.GameActivity;
import com.wiredin.jambar.R;
import com.wiredin.jambar.SummaryActivity;
import com.wiredin.jambar.Trigger;
import com.wiredin.jambar.dataclass.DatabaseHandler;
import com.wiredin.jambar.dataclass.Question2;
import com.wiredin.jambar.sequence.SequenceActivity;
import com.wiredin.jambar.services.SendAnswerService;
import com.wiredin.jambar.services.StartAnswerService;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.PowerManager;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class CompareImageFragment extends Fragment {

	private static final int CAMERA_REQUEST = 111;
	private static final int GALLERY_REQUEST = 112;
	private ProgressDialog dialog = null;
	public static final int MEDIA_TYPE_IMAGE = 1;
	private static final String IMAGE_DIRECTORY_NAME = "Jambar Image Question";
	String selectedImagePath;
	Button imgBtn;
	ImageView imgView;
	Uri imageUri;
	Button nxtBtn;
	Button skipBtn;
	TextView quesno;
	TextView question;
	DatabaseHandler dbHandler;
	int questid;
	Question2 nxtquest, currentquestion;
	String upLoadServerUri = null;
	private int serverResponseCode = 0;
	Trigger trigger;

	// timer caller
	CountDownTimer cdt;
	int cdtStatus = 0;
	
	boolean isRunning;

	TextView chrono;
	ConnectionDetector cd;
	// flag for Internet connection status
	Boolean isInternetPresent = false;

	private final long interval = 1000;

	View layout;
	
	ProgressDialog ringProgressDialog;

	// broadcast
	BroadcastReceiver mybroadcast = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			Log.i("[BroadcastReceiver]", "MyReceiver");

			if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
				Log.i("[BroadcastReceiver]", "Screen ON");
				Toast.makeText(getActivity(), "Screen ON", Toast.LENGTH_SHORT)
						.show();
				if (cdtStatus == 1) {
					if (isRunning){
						skipAction();
					}
				}
			} else if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
				Log.i("[BroadcastReceiver]", "Screen OFF");
			}

		}
	};

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		// check internet
		cd = new ConnectionDetector(getActivity());
		isInternetPresent = cd.isConnectingToInternet();

		Bundle b = getArguments();
		if (b != null)
			questid = b.getInt("id");

		View rootView = inflater.inflate(R.layout.compare_image, container,
				false);

		// Toaster
		layout = inflater.inflate(R.layout.custom_toast,
				(ViewGroup) rootView.findViewById(R.id.toast_layout_root));

        // ScreenLock
		getActivity().registerReceiver(mybroadcast,
				new IntentFilter(Intent.ACTION_SCREEN_ON));
		getActivity().registerReceiver(mybroadcast,
				new IntentFilter(Intent.ACTION_SCREEN_OFF));

		dbHandler = new DatabaseHandler(getActivity(), null, null, 2);
		currentquestion = dbHandler.getQuestionByTableId2(questid);

		trigger = new Trigger(getActivity(), currentquestion.getQuestionid());

		// StartAnsweringQuestion

		String totalQuestion = dbHandler.getGame().getTotal();
		((GameActivity) getActivity()).changeQuesLeft(questid + " / "
				+ totalQuestion);

		// answering start locally
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss", Locale.US);
		Date date = new Date();

		dbHandler.addStartAnswer(currentquestion.getQuestionid(),
				dateFormat.format(date));
		Log.d("hadian", "date: " + dateFormat.format(date));

		if (isInternetPresent) {
			StartAnswerService service = new StartAnswerService(dbHandler
					.getTeam().getTeamid(), currentquestion.getQuestionid(),
					dbHandler.getGame().getGameid(), "Ready", getActivity());
			service.execute();
		}

		// get next quest
		nxtquest = dbHandler.getQuestionByTableId2(questid + 1);

		// getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		Typeface tf = Typeface.createFromAsset(getActivity().getAssets(),
				"fonts/ProximaNova-Regular.otf");
		// Typeface tf1 = Typeface.createFromAsset(getActivity().getAssets(),
		// "fonts/ProximaNova-Bold.otf");

		TextView quesno = (TextView) rootView.findViewById(R.id.ques_no);
		quesno.setGravity(Gravity.CENTER);

		chrono = (TextView) rootView.findViewById(R.id.chrono);

		question = (TextView) rootView.findViewById(R.id.question);
		question.setTypeface(tf);
		question.setText(currentquestion.getQuestion());

		quesno = (TextView) rootView.findViewById(R.id.ques_no);
		quesno.setGravity(Gravity.CENTER);
		quesno.setTypeface(tf);
		quesno.setText(String.valueOf(questid));

		imgBtn = (Button) rootView.findViewById(R.id.imgBtn);
		imgView = (ImageView) rootView.findViewById(R.id.placeImg);

		nxtBtn = (Button) ((GameActivity) getActivity())
				.findViewById(R.id.next);
		skipBtn = (Button) ((GameActivity) getActivity())
				.findViewById(R.id.skip);

		ImageView img = (ImageView) rootView.findViewById(R.id.currentImg);
		RelativeLayout holder = (RelativeLayout) rootView
				.findViewById(R.id.layout_gamba1);

		if (!currentquestion.getAnswer2().equals("")) {
			img.setImageBitmap(loadBitmap(getActivity(),
					currentquestion.getAnswer2()));
		} else {
			img.setVisibility(View.GONE);
			holder.setVisibility(View.GONE);
		}

		// timer to check question timeout
		if (!currentquestion.getTimeout().equals("0")) {
			Log.d("Hadian", "Timeout: " + currentquestion.getTimeout());
			cdt = new CountDownTimer(Long.parseLong(currentquestion
					.getTimeout()) * 1000, interval) {

				@Override
				public void onTick(long millisUntilFinished) {
					// TODO Auto-generated method stub
					// Log.d("CDT",
					// "question: "+currentquestion.getId()+" __ "+millisUntilFinished);

					DecimalFormat formatter = new DecimalFormat("00");
					String minutes = formatter
							.format((int) ((millisUntilFinished / (1000 * 60)) % 60));
					String seconds = formatter
							.format((int) (millisUntilFinished / 1000) % 60);

					chrono.setText(minutes + ":" + seconds);
				}

				@Override
				public void onFinish() {
					// TODO Auto-generated method stub
					Log.d("Hadian",
							"cdt finish question: " + currentquestion.getId());

					cdtStatus = 1;

					PowerManager powerManager = (PowerManager) getActivity()
							.getSystemService(getActivity().POWER_SERVICE);
					if (powerManager.isScreenOn()) {
						if (isRunning){
							skipAction();
						}
					}

				}

			};

			cdt.start();
		}

		nxtBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				// Add the fragment to the 'fragment_container' FrameLayout
				// FragmentTransaction transaction = getFragmentManager()
				// .beginTransaction();

				endQuestion();

			}
		});

		skipBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						getActivity());
				alertDialogBuilder.setTitle("Skip Question");

				alertDialogBuilder
						.setMessage(
								"Are you sure to skip this question? Note! if you skip you cannot answer it again")
						.setCancelable(false)
						.setPositiveButton("No",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										// TODO Auto-generated method stub
										dialog.cancel();
									}
								})
						.setNegativeButton("Yes",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										// TODO Auto-generated method stub

										skipAction();
									}
								});

				AlertDialog alertDialog = alertDialogBuilder.create();

				// show it
				alertDialog.show();
			}
		});

		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

		final InputMethodManager imm = (InputMethodManager) getActivity()
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);

		upLoadServerUri = "http://wiredin.my/jambar/services/uploadToServerQuestionImage/"
				+ currentquestion.getQuestionid()
				+ "_"
				+ dbHandler.getTeam().getTeamid()
				+ "_"
				+ dbHandler.getGame().getGameid();
		this.imgBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						getActivity());
				alertDialogBuilder.setTitle("Photo Option");

				alertDialogBuilder
						.setMessage("Ready to take a picture?")
						.setCancelable(false)
						.setNegativeButton("Camera",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										// TODO Auto-generated method stub

										Intent intent = new Intent(
												MediaStore.ACTION_IMAGE_CAPTURE);

										imageUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

										intent.putExtra(
												MediaStore.EXTRA_OUTPUT,
												imageUri); // set the image file
															// name

										// start the image capture Intent
										startActivityForResult(intent,
												CAMERA_REQUEST);

									}
								})
						.setPositiveButton("cancel",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										// TODO Auto-generated method stub
										dialog.cancel();
									}
								});

				AlertDialog alertDialog = alertDialogBuilder.create();

				// show it
				alertDialog.show();
			}
		});
	}

	@SuppressLint("NewApi")
	@SuppressWarnings("static-access")
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		Log.d("fared", "result ade " + imageUri);

		// Matrix matrix=new Matrix();

		if (requestCode == CAMERA_REQUEST
				&& resultCode == getActivity().RESULT_OK) {
			Log.d("fared", "choose photo from camera");
			selectedImagePath = imageUri.getPath();
			Log.d("fared", "image path" + selectedImagePath);
			BitmapFactory.Options options = new BitmapFactory.Options();
			// downsizing image as it throws OutOfMemory Exception for larger
			// images
			options.inSampleSize = 8;
			final Bitmap bitmap = BitmapFactory.decodeFile(imageUri.getPath(),
					options);

			ExifInterface exif = null;
			try {
				exif = new ExifInterface(selectedImagePath);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} // Since API Level 5
			String exifOrientation = exif
					.getAttribute(ExifInterface.TAG_ORIENTATION);
			Log.d("fared", "orientation " + exifOrientation);

			int rotate = 0;

			if (exifOrientation.equals("6")) {
				rotate = 90;
				Log.d("fared", "rotate " + rotate);
			}

			imgView.setImageBitmap(bitmap);
			imgView.setRotation(rotate);
			//
		} else if (requestCode == GALLERY_REQUEST
				&& resultCode == getActivity().RESULT_OK && data != null) {
			Log.d("fared", "choose photo from gallery");
			Uri selectedImageUri = data.getData();
			selectedImagePath = getPath(selectedImageUri);
			imgView.setImageURI(selectedImageUri);

		}

	}

	public Uri getOutputMediaFileUri(int type) {
		return Uri.fromFile(getOutputMediaFile(type));
	}

	/*
	 * returning image / video
	 */
	private static File getOutputMediaFile(int type) {

		// External sdcard location
		File mediaStorageDir = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				IMAGE_DIRECTORY_NAME);

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
						+ IMAGE_DIRECTORY_NAME + " directory");
				return null;
			}
		}
		Log.d("fared", "create image");

		// Create a media file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
				Locale.getDefault()).format(new Date());
		File mediaFile;

		mediaFile = new File(mediaStorageDir.getPath() + File.separator
				+ "IMG_" + timeStamp + ".jpg");

		// if (type == MEDIA_TYPE_IMAGE) {
		// mediaFile = new File(mediaStorageDir.getPath() + File.separator
		// + "IMG_" + timeStamp + ".jpg");
		// } else {
		// return null;
		// }

		return mediaFile;
	}

	public String getPath(Uri uri) {
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = getActivity().getContentResolver().query(uri,
				projection, null, null, null);
		int column_index = cursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}

	public int uploadFile(String sourceFileUri) {

		if (cdt != null) {
			// stop the timer
			cdt.cancel();
		}

		String fileName = sourceFileUri;

		HttpURLConnection conn = null;
		DataOutputStream dos = null;
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary = "*****";
		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1 * 1024 * 1024;
		File sourceFile = new File(sourceFileUri);

		if (!sourceFile.isFile()) {

			dialog = ProgressDialog.show(getActivity(), "",
					"Uploading file...", true);
			dialog.dismiss();

			Log.e("uploadFile", "Source File not exist :" + selectedImagePath);

			return 0;

		} else {
			
			try {
				// open a URL connection to the Servlet
				FileInputStream fileInputStream = new FileInputStream(
						sourceFile);
				URL url = new URL(upLoadServerUri);
				Log.d("hadian", "upload to.. - " + upLoadServerUri);

				// Open a HTTP connection to the URL
				conn = (HttpURLConnection) url.openConnection();
				conn.setDoInput(true); // Allow Inputs
				conn.setDoOutput(true); // Allow Outputs
				conn.setUseCaches(false); // Don't use a Cached Copy
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Connection", "Keep-Alive");
				conn.setRequestProperty("ENCTYPE", "multipart/form-data");
				conn.setRequestProperty("Content-Type",
						"multipart/form-data;boundary=" + boundary);
				conn.setRequestProperty("uploaded_file", fileName);

				dos = new DataOutputStream(conn.getOutputStream());

				dos.writeBytes(twoHyphens + boundary + lineEnd);
				dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\""
						+ fileName + "\"" + lineEnd);

				dos.writeBytes(lineEnd);

				// create a buffer of maximum size
				bytesAvailable = fileInputStream.available();

				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				buffer = new byte[bufferSize];

				// read file and write it into form...
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);

				while (bytesRead > 0) {

					dos.write(buffer, 0, bufferSize);
					bytesAvailable = fileInputStream.available();
					bufferSize = Math.min(bytesAvailable, maxBufferSize);
					bytesRead = fileInputStream.read(buffer, 0, bufferSize);

				}

				// send multipart form data necesssary after file data...
				dos.writeBytes(lineEnd);
				dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

				// Responses from the server (code and message)
				serverResponseCode = conn.getResponseCode();
				String serverResponseMessage = conn.getResponseMessage();

				Log.i("uploadFile", "HTTP Response is : "
						+ serverResponseMessage + ": " + serverResponseCode);

				if (serverResponseCode == 200) {
					Log.d("fared", "Upload complete");
					
					if (ringProgressDialog != null){
						ringProgressDialog.dismiss();
					}

					if (nxtquest == null) {
						// answering start locally
						SimpleDateFormat dateFormat = new SimpleDateFormat(
								"yyyy-MM-dd HH:mm:ss",
								Locale.US);
						Date date = new Date();

						dbHandler.addStopAnswer(
								currentquestion
										.getQuestionid(),
								"", "True", dateFormat
										.format(date));

						// correct number of question
						int currentcorrect = Integer.parseInt(dbHandler
								.getSummaryCorrect());
						currentcorrect++;
						dbHandler
								.updateSummaryCorrect(String
										.valueOf(currentcorrect));

						// update total point
						int currentpoint = Integer
								.parseInt(dbHandler
										.getSummaryPoint());
						currentpoint += Integer.parseInt(currentquestion
								.getPoint_success());
						dbHandler.updateSummaryPoint(String
								.valueOf(currentpoint));
						
						Intent myIntent = new Intent(getActivity(),
								SummaryActivity.class);
						Log.d("fared", "open summary activity");
						getActivity().startActivity(myIntent);
						((GameActivity) getActivity()).setStopLocation();
						getActivity().unregisterReceiver(mybroadcast);
						getActivity().finish();
					}
					
					

				}

				// close the streams //
				fileInputStream.close();
				dos.flush();
				dos.close();

			} catch (MalformedURLException ex) {

				ex.printStackTrace();

				Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
			} catch (Exception e) {

				e.printStackTrace();

				Log.e("Upload file to server Exception",
						"Exception : " + e.getMessage(), e);
			}

			return serverResponseCode;

		} // End else block
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	public void endQuestion() {
		if (trigger.checkStart() == true) {
			// if there is a trigger in this question

			// alert before sequence
			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
					getActivity());
			alertDialogBuilder.setTitle("Triger Sequence");

			alertDialogBuilder
					.setMessage(
							"you have triger a bonus level !! You want to enter this level ?")
					.setCancelable(false)
					.setNegativeButton("No",
							new DialogInterface.OnClickListener() {
								// if no is selected in dialog

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method
									// stub

									dbHandler.setSequenceDone(trigger
											.currentSeq());

									FragmentTransaction transaction = getFragmentManager()
											.beginTransaction();

									if (nxtquest != null) {
										// if there is next question

										String type = nxtquest.getType();

										if (selectedImagePath != null) {
											// if the image is not null

											String finalQuesTime = (String) chrono
													.getText();

											Log.d("fared", "time stop"
													+ finalQuesTime);

											if (cdt != null) {
												// stop the timer
												cdt.cancel();
											}

											if (type.equals("Obj")) {
												MultipleChoiceFragment multiplechoiceFragment = new MultipleChoiceFragment();

												Bundle args = new Bundle();
												args.putInt("id",
														nxtquest.getId());
												multiplechoiceFragment
														.setArguments(args);

												transaction.replace(
														R.id.game_container,
														multiplechoiceFragment);
											} else if (type.equals("Subj")) {
												SubjectiveFragment subjectiveFragment = new SubjectiveFragment();

												Bundle args = new Bundle();
												args.putInt("id",
														nxtquest.getId());
												subjectiveFragment
														.setArguments(args);

												transaction.replace(
														R.id.game_container,
														subjectiveFragment);
											} else if (type.equals("Pic")) {
												CompareImageFragment compareImageFragment = new CompareImageFragment();

												Bundle args = new Bundle();
												args.putInt("id",
														nxtquest.getId());
												compareImageFragment
														.setArguments(args);

												transaction.replace(
														R.id.game_container,
														compareImageFragment);
											} else if (type.equals("Code")) {
												EnterCodeFragment enterCodeFragment = new EnterCodeFragment();

												Bundle args = new Bundle();
												args.putInt("id",
														nxtquest.getId());
												enterCodeFragment
														.setArguments(args);

												transaction.replace(
														R.id.game_container,
														enterCodeFragment);
											} else if (type.equals("Vid")) {
												VideoQuesFragment videoQuesFragment = new VideoQuesFragment();

												Bundle args = new Bundle();
												args.putInt("id",
														nxtquest.getId());
												videoQuesFragment
														.setArguments(args);

												transaction.replace(
														R.id.game_container,
														videoQuesFragment);
											} else if (type.equals("QR")) {

											}

											// answering start locally
											SimpleDateFormat dateFormat = new SimpleDateFormat(
													"yyyy-MM-dd HH:mm:ss",
													Locale.US);
											Date date = new Date();

											dbHandler.addStopAnswer(
													currentquestion
															.getQuestionid(),
													"", "True", dateFormat
															.format(date));

											// correct number of question
											int currentcorrect = Integer.parseInt(dbHandler
													.getSummaryCorrect());
											currentcorrect++;
											dbHandler
													.updateSummaryCorrect(String
															.valueOf(currentcorrect));

											// update total point
											int currentpoint = Integer
													.parseInt(dbHandler
															.getSummaryPoint());
											currentpoint += Integer.parseInt(currentquestion
													.getPoint_success());
											dbHandler.updateSummaryPoint(String
													.valueOf(currentpoint));

											if (!currentquestion
													.getNotify_correct_text()
													.isEmpty()) {
												TextView text = (TextView) layout
														.findViewById(R.id.text);
												text.setText(currentquestion
														.getNotify_correct_text());

												Toast toast = new Toast(
														getActivity()
																.getApplicationContext());
												toast.setGravity(
														Gravity.BOTTOM, 0, 200);
												toast.setDuration(Toast.LENGTH_LONG);
												toast.setView(layout);
												toast.show();
											}

											transaction.addToBackStack(null);
											transaction
													.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
											transaction.commit();
											getActivity().unregisterReceiver(
													mybroadcast);

											new Thread(new Runnable() {
												public void run() {

													uploadFile(selectedImagePath);

												}
											}).start();

										} else {
											AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
													getActivity());
											alertDialogBuilder
													.setTitle("Danger !");

											alertDialogBuilder
													.setMessage(
															"Please take picture to proceed to next question or use the skip button")
													.setCancelable(true)
													.setPositiveButton(
															"Ok",
															new DialogInterface.OnClickListener() {

																@Override
																public void onClick(
																		DialogInterface dialog,
																		int which) {
																	// TODO
																	// Auto-generated
																	// method
																	// stub
																	dialog.cancel();
																}
															});

											AlertDialog alertDialog = alertDialogBuilder
													.create();

											// show it
											alertDialog.show();
										}

									} else {

										if (selectedImagePath != null) {
											
											ringProgressDialog = ProgressDialog.show(
													getActivity(), "Please wait ...", "Uploading Last Picture", true);

											new Thread(new Runnable() {
												public void run() {

													uploadFile(selectedImagePath);

												}
											}).start();
											
											if (!currentquestion
													.getNotify_correct_text()
													.isEmpty()) {
												TextView text = (TextView) layout
														.findViewById(R.id.text);
												text.setText(currentquestion
														.getNotify_correct_text());

												Toast toast = new Toast(
														getActivity()
																.getApplicationContext());
												toast.setGravity(
														Gravity.BOTTOM, 0, 200);
												toast.setDuration(Toast.LENGTH_LONG);
												toast.setView(layout);
												toast.show();
											}
											

										} else {
											AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
													getActivity());
											alertDialogBuilder
													.setTitle("Danger !");

											alertDialogBuilder
													.setMessage(
															"Please take picture to proceed to next question or use the skip button")
													.setCancelable(true)
													.setPositiveButton(
															"Ok",
															new DialogInterface.OnClickListener() {

																@Override
																public void onClick(
																		DialogInterface dialog,
																		int which) {
																	// TODO
																	// Auto-generated
																	// method
																	// stub
																	dialog.cancel();
																}
															});

											AlertDialog alertDialog = alertDialogBuilder
													.create();

											// show it
											alertDialog.show();
										}

									}
								}
							})
					.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method
									// stub
									FragmentTransaction transaction = getFragmentManager()
											.beginTransaction();

									if (nxtquest != null) {
										String type = nxtquest.getType();

										if (selectedImagePath != null) {
											String finalQuesTime = (String) chrono
													.getText();

											Log.d("fared", "time stop"
													+ finalQuesTime);
											if (type.equals("Obj")) {
												MultipleChoiceFragment multiplechoiceFragment = new MultipleChoiceFragment();

												Bundle args = new Bundle();
												args.putInt("id",
														nxtquest.getId());
												multiplechoiceFragment
														.setArguments(args);

												transaction.replace(
														R.id.game_container,
														multiplechoiceFragment);
											} else if (type.equals("Subj")) {
												SubjectiveFragment subjectiveFragment = new SubjectiveFragment();

												Bundle args = new Bundle();
												args.putInt("id",
														nxtquest.getId());
												subjectiveFragment
														.setArguments(args);

												transaction.replace(
														R.id.game_container,
														subjectiveFragment);
											} else if (type.equals("Pic")) {
												CompareImageFragment compareImageFragment = new CompareImageFragment();

												Bundle args = new Bundle();
												args.putInt("id",
														nxtquest.getId());
												compareImageFragment
														.setArguments(args);

												transaction.replace(
														R.id.game_container,
														compareImageFragment);
											} else if (type.equals("Code")) {
												EnterCodeFragment enterCodeFragment = new EnterCodeFragment();

												Bundle args = new Bundle();
												args.putInt("id",
														nxtquest.getId());
												enterCodeFragment
														.setArguments(args);

												transaction.replace(
														R.id.game_container,
														enterCodeFragment);
											} else if (type.equals("Vid")) {
												VideoQuesFragment videoQuesFragment = new VideoQuesFragment();

												Bundle args = new Bundle();
												args.putInt("id",
														nxtquest.getId());
												videoQuesFragment
														.setArguments(args);

												transaction.replace(
														R.id.game_container,
														videoQuesFragment);
											} else if (type.equals("QR")) {

											}

											// answering start locally
											SimpleDateFormat dateFormat = new SimpleDateFormat(
													"yyyy-MM-dd HH:mm:ss",
													Locale.US);
											Date date = new Date();

											dbHandler.addStopAnswer(
													currentquestion
															.getQuestionid(),
													"", "True", dateFormat
															.format(date));

											// correct number of question
											int currentcorrect = Integer.parseInt(dbHandler
													.getSummaryCorrect());
											currentcorrect++;
											dbHandler
													.updateSummaryCorrect(String
															.valueOf(currentcorrect));

											// update total point
											int currentpoint = Integer
													.parseInt(dbHandler
															.getSummaryPoint());
											currentpoint += Integer.parseInt(currentquestion
													.getPoint_success());
											dbHandler.updateSummaryPoint(String
													.valueOf(currentpoint));

											if (!currentquestion
													.getNotify_correct_text()
													.isEmpty()) {
												TextView text = (TextView) layout
														.findViewById(R.id.text);
												text.setText(currentquestion
														.getNotify_correct_text());

												Toast toast = new Toast(
														getActivity()
																.getApplicationContext());
												toast.setGravity(
														Gravity.BOTTOM, 0, 200);
												toast.setDuration(Toast.LENGTH_LONG);
												toast.setView(layout);
												toast.show();
											}

											transaction.addToBackStack(null);
											transaction
													.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
											transaction.commit();
											getActivity().unregisterReceiver(
													mybroadcast);

											new Thread(new Runnable() {
												public void run() {

													uploadFile(selectedImagePath);

												}
											}).start();

											Log.d("fared", "masuk triger = "
													+ questid);
											Intent myIntent = new Intent(
													getActivity(),
													SequenceActivity.class);
											Log.d("fared",
													"open sequence activity");
											myIntent.putExtra("id",
													trigger.currentSeq());
											getActivity().startActivity(
													myIntent);
										} else {
											AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
													getActivity());
											alertDialogBuilder
													.setTitle("Danger !");

											alertDialogBuilder
													.setMessage(
															"Please take picture to proceed to next question or use the skip button")
													.setCancelable(true)
													.setPositiveButton(
															"Ok",
															new DialogInterface.OnClickListener() {

																@Override
																public void onClick(
																		DialogInterface dialog,
																		int which) {
																	// TODO
																	// Auto-generated
																	// method
																	// stub
																	dialog.cancel();
																}
															});

											AlertDialog alertDialog = alertDialogBuilder
													.create();

											// show it
											alertDialog.show();
										}

									} else {

										if (selectedImagePath != null) {
											
											ringProgressDialog = ProgressDialog.show(
													getActivity(), "Please wait ...", "Uploading Last Picture", true);
											
											new Thread(new Runnable() {
												public void run() {

													uploadFile(selectedImagePath);

												}
											}).start();
											
											if (!currentquestion
													.getNotify_correct_text()
													.isEmpty()) {
												TextView text = (TextView) layout
														.findViewById(R.id.text);
												text.setText(currentquestion
														.getNotify_correct_text());

												Toast toast = new Toast(
														getActivity()
																.getApplicationContext());
												toast.setGravity(
														Gravity.BOTTOM, 0, 200);
												toast.setDuration(Toast.LENGTH_LONG);
												toast.setView(layout);
												toast.show();
											}
											
										} else {
											AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
													getActivity());
											alertDialogBuilder
													.setTitle("Danger !");

											alertDialogBuilder
													.setMessage(
															"Please take picture to proceed to next question or use the skip button")
													.setCancelable(true)
													.setPositiveButton(
															"Ok",
															new DialogInterface.OnClickListener() {

																@Override
																public void onClick(
																		DialogInterface dialog,
																		int which) {
																	// TODO
																	// Auto-generated
																	// method
																	// stub
																	dialog.cancel();
																}
															});

											AlertDialog alertDialog = alertDialogBuilder
													.create();

											// show it
											alertDialog.show();
										}

									}
								}
							});

			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();

			// Add the fragment to the 'fragment_container'
			// FrameLayout

		} else {

			FragmentTransaction transaction = getFragmentManager()
					.beginTransaction();

			if (nxtquest != null) {
				// if there is next question

				String type = nxtquest.getType();

				if (selectedImagePath != null) {
					// if the image is not null

					String finalQuesTime = (String) chrono.getText();

					Log.d("fared", "time stop" + finalQuesTime);

					if (type.equals("Obj")) {
						MultipleChoiceFragment multiplechoiceFragment = new MultipleChoiceFragment();

						Bundle args = new Bundle();
						args.putInt("id", nxtquest.getId());
						multiplechoiceFragment.setArguments(args);

						transaction.replace(R.id.game_container,
								multiplechoiceFragment);
					} else if (type.equals("Subj")) {
						SubjectiveFragment subjectiveFragment = new SubjectiveFragment();

						Bundle args = new Bundle();
						args.putInt("id", nxtquest.getId());
						subjectiveFragment.setArguments(args);

						transaction.replace(R.id.game_container,
								subjectiveFragment);
					} else if (type.equals("Pic")) {
						CompareImageFragment compareImageFragment = new CompareImageFragment();

						Bundle args = new Bundle();
						args.putInt("id", nxtquest.getId());
						compareImageFragment.setArguments(args);

						transaction.replace(R.id.game_container,
								compareImageFragment);
					} else if (type.equals("Code")) {
						EnterCodeFragment enterCodeFragment = new EnterCodeFragment();

						Bundle args = new Bundle();
						args.putInt("id", nxtquest.getId());
						enterCodeFragment.setArguments(args);

						transaction.replace(R.id.game_container,
								enterCodeFragment);
					} else if (type.equals("Vid")) {
						VideoQuesFragment videoQuesFragment = new VideoQuesFragment();

						Bundle args = new Bundle();
						args.putInt("id", nxtquest.getId());
						videoQuesFragment.setArguments(args);

						transaction.replace(R.id.game_container,
								videoQuesFragment);
					} else if (type.equals("QR")) {

					}

					// answering start locally
					SimpleDateFormat dateFormat = new SimpleDateFormat(
							"yyyy-MM-dd HH:mm:ss", Locale.US);
					Date date = new Date();

					dbHandler.addStopAnswer(currentquestion.getQuestionid(),
							"", "True", dateFormat.format(date));

					// correct number of question
					int currentcorrect = Integer.parseInt(dbHandler
							.getSummaryCorrect());
					currentcorrect++;
					dbHandler.updateSummaryCorrect(String
							.valueOf(currentcorrect));

					// update total point
					int currentpoint = Integer.parseInt(dbHandler
							.getSummaryPoint());
					currentpoint += Integer.parseInt(currentquestion
							.getPoint_success());
					dbHandler.updateSummaryPoint(String.valueOf(currentpoint));

					if (!currentquestion.getNotify_correct_text().isEmpty()) {
						TextView text = (TextView) layout
								.findViewById(R.id.text);
						text.setText(currentquestion.getNotify_correct_text());

						Toast toast = new Toast(getActivity()
								.getApplicationContext());
						toast.setGravity(Gravity.BOTTOM, 0, 200);
						toast.setDuration(Toast.LENGTH_LONG);
						toast.setView(layout);
						toast.show();
					}

					transaction.addToBackStack(null);
					transaction
							.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
					transaction.commit();
					getActivity().unregisterReceiver(mybroadcast);

					new Thread(new Runnable() {
						public void run() {

							uploadFile(selectedImagePath);

						}
					}).start();
				} else {
					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
							getActivity());
					alertDialogBuilder.setTitle("Danger !");

					alertDialogBuilder
							.setMessage(
									"Please take picture to proceed to next question or use the skip button")
							.setCancelable(true)
							.setPositiveButton("Ok",
									new DialogInterface.OnClickListener() {

										@Override
										public void onClick(
												DialogInterface dialog,
												int which) {
											// TODO
											// Auto-generated
											// method
											// stub
											dialog.cancel();
										}
									});

					AlertDialog alertDialog = alertDialogBuilder.create();

					// show it
					alertDialog.show();
				}

			} else {

				if (selectedImagePath != null) {
					
					ringProgressDialog = ProgressDialog.show(
							getActivity(), "Please wait ...", "Uploading Last Picture", true);
					
					new Thread(new Runnable() {
						public void run() {

							uploadFile(selectedImagePath);

						}
					}).start();
					
					if (!currentquestion
							.getNotify_correct_text()
							.isEmpty()) {
						TextView text = (TextView) layout
								.findViewById(R.id.text);
						text.setText(currentquestion
								.getNotify_correct_text());

						Toast toast = new Toast(
								getActivity()
										.getApplicationContext());
						toast.setGravity(
								Gravity.BOTTOM, 0, 200);
						toast.setDuration(Toast.LENGTH_LONG);
						toast.setView(layout);
						toast.show();
					}
					
					
				} else {
					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
							getActivity());
					alertDialogBuilder.setTitle("Danger !");

					alertDialogBuilder
							.setMessage(
									"Please take picture to proceed to next question or use the skip button")
							.setCancelable(true)
							.setPositiveButton("Ok",
									new DialogInterface.OnClickListener() {

										@Override
										public void onClick(
												DialogInterface dialog,
												int which) {
											// TODO
											// Auto-generated
											// method
											// stub
											dialog.cancel();
										}
									});

					AlertDialog alertDialog = alertDialogBuilder.create();

					// show it
					alertDialog.show();
				}

			}
		}
	}

	public static Bitmap loadBitmap(Context context, String picName) {
		Bitmap b = null;
		FileInputStream fis;
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = 1;
		try {
			fis = context.openFileInput(picName);
			b = BitmapFactory.decodeStream(fis, null, options);
			fis.close();

		} catch (FileNotFoundException e) {
			Log.d("Hadian", "file not found");
			e.printStackTrace();
		} catch (IOException e) {
			Log.d("Hadian", "io exception");
			e.printStackTrace();
		}
		return b;
	}

	public void skipAction() {
		if (isInternetPresent) {
			// Send the answer to admin for the
			// checking
			SendAnswerService service = new SendAnswerService(dbHandler
					.getTeam().getTeamid(), currentquestion.getQuestionid(),
					dbHandler.getGame().getGameid(), "Skip", getActivity());
			service.execute();
		}

		// Add skip in database
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss", Locale.US);
		Date date = new Date();

		dbHandler.addStopAnswer(currentquestion.getQuestionid(), "Skip",
				"Skip", dateFormat.format(date));

		String finalQuesTime = (String) chrono.getText();

		Log.d("fared", "time stop" + finalQuesTime);

		if (cdt != null) {
			// stop the timer
			cdt.cancel();
		}

		// Add the fragment to the
		// 'fragment_container' FrameLayout
		FragmentTransaction transaction = getFragmentManager()
				.beginTransaction();

		if (nxtquest != null) {
			String type = nxtquest.getType();

			if (type.equals("Obj")) {
				MultipleChoiceFragment multiplechoiceFragment = new MultipleChoiceFragment();

				Bundle args = new Bundle();
				args.putInt("id", nxtquest.getId());
				multiplechoiceFragment.setArguments(args);

				transaction
						.replace(R.id.game_container, multiplechoiceFragment);
			} else if (type.equals("Subj")) {
				SubjectiveFragment subjectiveFragment = new SubjectiveFragment();

				Bundle args = new Bundle();
				args.putInt("id", nxtquest.getId());
				subjectiveFragment.setArguments(args);

				transaction.replace(R.id.game_container, subjectiveFragment);
			} else if (type.equals("Pic")) {
				CompareImageFragment compareImageFragment = new CompareImageFragment();

				Bundle args = new Bundle();
				args.putInt("id", nxtquest.getId());
				compareImageFragment.setArguments(args);

				transaction.replace(R.id.game_container, compareImageFragment);
			} else if (type.equals("Code")) {
				EnterCodeFragment enterCodeFragment = new EnterCodeFragment();

				Bundle args = new Bundle();
				args.putInt("id", nxtquest.getId());
				enterCodeFragment.setArguments(args);

				transaction.replace(R.id.game_container, enterCodeFragment);
			} else if (type.equals("Vid")) {
				VideoQuesFragment videoQuesFragment = new VideoQuesFragment();

				Bundle args = new Bundle();
				args.putInt("id", nxtquest.getId());
				videoQuesFragment.setArguments(args);

				transaction.replace(R.id.game_container, videoQuesFragment);
			} else if (type.equals("QR")) {

			}

			transaction.addToBackStack(null);
			transaction
					.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
			transaction.commit();
			getActivity().unregisterReceiver(mybroadcast);

		} else {
			Intent myIntent = new Intent(getActivity(), SummaryActivity.class);
			Log.d("fared", "open summary activity");
			getActivity().startActivity(myIntent);
			((GameActivity) getActivity()).setStopLocation();
			getActivity().unregisterReceiver(mybroadcast);
			getActivity().finish();
		}
	}
	
	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		isRunning = false;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		isRunning = true;
		
		if(cdtStatus == 1){
			skipAction();
		}
	}

}
