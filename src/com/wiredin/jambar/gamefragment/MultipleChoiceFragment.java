package com.wiredin.jambar.gamefragment;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.PowerManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.wiredin.jambar.*;
import com.wiredin.jambar.dataclass.DatabaseHandler;
import com.wiredin.jambar.dataclass.Question2;
import com.wiredin.jambar.sequence.SequenceActivity;
import com.wiredin.jambar.services.SendAnswerService;
import com.wiredin.jambar.services.StartAnswerService;

public class MultipleChoiceFragment extends ListFragment {

	String[] listAnswer;
	ArrayAdapter<String> listAdapter;
	ListView listView;
	Button nextBtn;
	TextView quesno;
	TextView question;
	TextView timerQues;
	TextView textAns;
	DatabaseHandler dbHandler;
	TextView answer;
	int questid;
	Question2 nxtquest, currentquestion;
	Trigger trigger;
	String selectedTextFromList;

	// timer caller
	CountDownTimer cdt;
	int cdtStatus = 0;
	
	boolean isRunning;

	TextView chrono;
	ConnectionDetector cd;
	// flag for Internet connection status
	Boolean isInternetPresent = false;

	String selectedFromList = null;

	View layout;

	private final long interval = 1000;

	public static final Integer[] images = { R.drawable.aobj, R.drawable.bobj,
			R.drawable.cobj, R.drawable.dobj };

	// broadcast
	BroadcastReceiver mybroadcast = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			Log.i("[BroadcastReceiver]", "MyReceiver");

			if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
				Log.i("[BroadcastReceiver]", "Screen ON");
				Toast.makeText(getActivity(), "Screen ON", Toast.LENGTH_SHORT).show();
				if (cdtStatus == 1) {
					if (isRunning){
						skipAction();
					}
				}
			} else if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
				Log.i("[BroadcastReceiver]", "Screen OFF");
			}

		}
	};

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		View rootView = inflater.inflate(R.layout.multi_choice, container,
				false);

		// Toaster
		layout = inflater.inflate(R.layout.custom_toast,
				(ViewGroup) rootView.findViewById(R.id.toast_layout_root));

        // ScreenLock
		getActivity().registerReceiver(mybroadcast, new IntentFilter(Intent.ACTION_SCREEN_ON));
		getActivity().registerReceiver(mybroadcast, new IntentFilter(Intent.ACTION_SCREEN_OFF));

		// check internet
		cd = new ConnectionDetector(getActivity());
		isInternetPresent = cd.isConnectingToInternet();

		Bundle b = getArguments();
		if (b != null)
			questid = b.getInt("id");

		dbHandler = new DatabaseHandler(getActivity(), null, null, 2);
		currentquestion = dbHandler.getQuestionByTableId2(questid);

		// StartAnsweringQuestion
		String totalQuestion = dbHandler.getGame().getTotal();
		((GameActivity) getActivity()).changeQuesLeft(questid + " / "
				+ totalQuestion);

		// answering start locally
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss", Locale.US);
		Date date = new Date();

		dbHandler.addStartAnswer(currentquestion.getQuestionid(),
				dateFormat.format(date));
		Log.d("hadian", "date: " + dateFormat.format(date));

		if (isInternetPresent) {
			StartAnswerService service = new StartAnswerService(dbHandler
					.getTeam().getTeamid(), currentquestion.getQuestionid(),
					dbHandler.getGame().getGameid(), "Ready", getActivity());
			service.execute();
		}

		// get next quest
		nxtquest = dbHandler.getQuestionByTableId2(questid + 1);

		// getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		Typeface tf = Typeface.createFromAsset(getActivity().getAssets(),
				"fonts/ProximaNova-Regular.otf");
		Typeface tf1 = Typeface.createFromAsset(getActivity().getAssets(),
				"fonts/ProximaNova-Bold.otf");

		textAns = (TextView) rootView.findViewById(R.id.answerText);
		textAns.setVisibility(View.GONE);
		textAns.setTypeface(tf1);

		answer = (TextView) rootView.findViewById(R.id.answer);
		answer.setVisibility(View.GONE);
		answer.setTypeface(tf);

		chrono = (TextView) rootView.findViewById(R.id.chrono);
		// timerQues = (TextView) rootView.findViewById(R.id.timerQues);

		quesno = (TextView) rootView.findViewById(R.id.ques_no);
		quesno.setGravity(Gravity.CENTER);
		quesno.setTypeface(tf);
		quesno.setText(String.valueOf(questid));

		question = (TextView) rootView.findViewById(R.id.question);
		question.setTypeface(tf);
		question.setText(currentquestion.getQuestion());

		listAnswer = getResources().getStringArray(R.array.list_answer);
		nextBtn = (Button) ((GameActivity) getActivity())
				.findViewById(R.id.next);

		ImageView questionImage = (ImageView) rootView
				.findViewById(R.id.imageView2);

		// if (!currentquestion.getAnswer2().equals("")) {
		// // picasso image
		// Picasso.with(getActivity())
		// .load(getActivity().getResources().getString(
		// R.string.imageurl)
		// + currentquestion.getAnswer2()).resize(50, 50)
		// .centerCrop().into(questionImage);
		// } else {
		// questionImage.setVisibility(View.GONE);
		// }

		if (!currentquestion.getAnswer2().equals("")) {
			questionImage.setImageBitmap(loadBitmap(getActivity(),
					currentquestion.getAnswer2()));
		} else {
			questionImage.setVisibility(View.GONE);
		}

		Log.d("Hadian", "Timeout: " + currentquestion.getTimeout());
		// timer to check question timeout
		if (!currentquestion.getTimeout().equals("0")) {
			cdt = new CountDownTimer(Long.parseLong(currentquestion
					.getTimeout()) * 1000, interval) {

				@Override
				public void onTick(long millisUntilFinished) {
					// TODO Auto-generated method stub
					// Log.d("CDT",
					// "question: "+currentquestion.getId()+" __ "+millisUntilFinished);
					DecimalFormat formatter = new DecimalFormat("00");
					String minutes = formatter
							.format((int) ((millisUntilFinished / (1000 * 60)) % 60));
					String seconds = formatter
							.format((int) (millisUntilFinished / 1000) % 60);

					chrono.setText(minutes + ":" + seconds);
				}

				@Override
				public void onFinish() {
					// TODO Auto-generated method stub
					Log.d("Hadian",
							"cdt finish question: " + currentquestion.getId());
					
					cdtStatus = 1;
					
					PowerManager powerManager = (PowerManager) getActivity()
							.getSystemService(getActivity().POWER_SERVICE);
					if (powerManager.isScreenOn()) {
						if (isRunning){
							skipAction();
						}
					}
				}

			};

			// start the timer
			cdt.start();
		}

		nextBtn.setTypeface(tf);
		nextBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (selectedFromList == null) {
					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
							getActivity());
					alertDialogBuilder.setTitle("Please chosse the answer.");

					alertDialogBuilder
							.setMessage(
									"You need to select one of the answer before proceed to next question.")
							.setCancelable(false)
							.setPositiveButton("Ok",
									new DialogInterface.OnClickListener() {

										@Override
										public void onClick(
												DialogInterface dialog,
												int which) {
											// TODO Auto-generated method stub
											dialog.cancel();
										}
									});
					AlertDialog alertDialog = alertDialogBuilder.create();

					// show it
					alertDialog.show();
				} else {
					Log.d("fared", "select from list tak null");

					endQuestion();
				}
			}
		});

		Button skipBtn = (Button) ((GameActivity) getActivity())
				.findViewById(R.id.skip);
		skipBtn.setTypeface(tf);
		skipBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						getActivity());
				alertDialogBuilder.setTitle("Skip Question");

				alertDialogBuilder
						.setMessage(
								"Are you sure to skip this question? Note! if you skip you cannot answer it again")
						.setCancelable(false)
						.setPositiveButton("No",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										// TODO Auto-generated method stub
										dialog.cancel();
									}
								})
						.setNegativeButton("Yes",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										// TODO Auto-generated method stub

										skipAction();
									}
								});

				AlertDialog alertDialog = alertDialogBuilder.create();

				// show it
				alertDialog.show();
			}
		});
		return rootView;

	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

		final InputMethodManager imm = (InputMethodManager) getActivity()
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);

		dbHandler = new DatabaseHandler(getActivity(), null, null, 2);
		Question2 answers = currentquestion;

		if (answers != null) {

			String[] answer1 = answers.getAnswer1().split("\\s\\|\\s");
			// String[] answer = new String[(int) (Math.round(answer1.length /
			// 2)) + 1];

			String[] letter = new String[answer1.length];

			char iletter = 'A';
			int i = 0;
			for (String value : answer1) {
				Log.d("Hadian", "val = " + value + ", letter = " + iletter);
				letter[i] = "" + iletter;
				i++;
				iletter++;
			}

			listAdapter = new AnswerArrayAdapter(getActivity(), answer1, letter);
			setListAdapter(listAdapter);
		}

		listView = this.getListView();

		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view,
					int position, long arg3) {
				// TODO Auto-generated method stub
				view.setSelected(true);
				selectedFromList = String.valueOf(position);
				selectedTextFromList = (listView
						.getItemAtPosition(position).toString());
				;
				textAns.setVisibility(View.VISIBLE);
				answer.setVisibility(View.VISIBLE);
				textAns.setText(selectedTextFromList);

				Log.d("fared", "list yang di pilih " + selectedFromList);

				Log.d("fared", "gpostion : " + position);

				// progress
				launchRingDialog();

			}
		});

	}

	public void launchRingDialog() {
		final ProgressDialog ringProgressDialog = ProgressDialog.show(
				getActivity(), "Please wait ...", "Checking Your Answer : "+selectedTextFromList, true);

		CountDownTimer cdtm = new CountDownTimer(3000,interval){

			@Override
			public void onTick(long millisUntilFinished) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onFinish() {
				// TODO Auto-generated method stub
				ringProgressDialog.dismiss();
				endQuestion();
			}
			
		};
		
		cdtm.start();

	}

	public void endQuestion() {
		String objAnswer = null;

		switch (Integer.parseInt(selectedFromList)) {
		case 0:
			objAnswer = 'A' + "";
			break;
		case 1:
			objAnswer = 'B' + "";
			break;
		case 2:
			objAnswer = 'C' + "";
			break;
		case 3:
			objAnswer = 'D' + "";
			break;
		case 4:
			objAnswer = 'E' + "";
			break;
		case 5:
			objAnswer = 'F' + "";
			break;
		case 6:
			objAnswer = 'G' + "";
			break;
		case 7:
			objAnswer = 'H' + "";
			break;
		case 8:
			objAnswer = 'I' + "";
			break;
		case 9:
			objAnswer = 'J' + "";
			break;
		case 10:
			objAnswer = 'K' + "";
			break;
		case 11:
			objAnswer = 'L' + "";
			break;
		case 12:
			objAnswer = 'M' + "";
			break;
		case 13:
			objAnswer = 'N' + "";
			break;
		case 14:
			objAnswer = 'O' + "";
			break;
		case 15:
			objAnswer = 'P' + "";
			break;
		}

		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss", Locale.US);
		Date date = new Date();

		// answer checking for multiply
		if (currentquestion.getCorrect_answer().equals(objAnswer)) {

			// Save answer locally
			dbHandler.addStopAnswer(currentquestion.getQuestionid(), objAnswer,
					"True", dateFormat.format(date));

			// correct number of question
			int currentcorrect = Integer
					.parseInt(dbHandler.getSummaryCorrect());
			currentcorrect++;
			dbHandler.updateSummaryCorrect(String.valueOf(currentcorrect));

			// update total point
			int currentpoint = Integer.parseInt(dbHandler.getSummaryPoint());
			currentpoint += Integer
					.parseInt(currentquestion.getPoint_success());
			dbHandler.updateSummaryPoint(String.valueOf(currentpoint));

			if (!currentquestion.getNotify_correct_text().isEmpty()) {
				// Toast.makeText(getActivity(),
				// currentquestion.getNotify_correct_text(),
				// Toast.LENGTH_LONG).show();

				TextView text = (TextView) layout.findViewById(R.id.text);
				text.setText(currentquestion.getNotify_correct_text());

				Toast toast = new Toast(getActivity().getApplicationContext());
				toast.setGravity(Gravity.BOTTOM, 0, 200);
				toast.setDuration(Toast.LENGTH_LONG);
				toast.setView(layout);
				toast.show();
			}

		} else {

			// Save answer locally
			dbHandler.addStopAnswer(currentquestion.getQuestionid(), objAnswer,
					"False", dateFormat.format(date));

			// update total point
			int currentpoint = Integer.parseInt(dbHandler.getSummaryPoint());
			currentpoint -= Integer.parseInt(currentquestion.getPoint_fail());
			dbHandler.updateSummaryPoint(String.valueOf(currentpoint));

			if (!currentquestion.getNotify_wrong_text().isEmpty()) {
				// Toast.makeText(getActivity(),
				// currentquestion.getNotify_wrong_text(),
				// Toast.LENGTH_LONG).show();

				TextView text = (TextView) layout.findViewById(R.id.text);
				text.setText(currentquestion.getNotify_wrong_text());

				Toast toast = new Toast(getActivity().getApplicationContext());
				toast.setGravity(Gravity.BOTTOM, 0, 200);
				toast.setDuration(Toast.LENGTH_LONG);
				toast.setView(layout);
				toast.show();
			}
		}

		// Send the answer to server
		if (isInternetPresent) {
			SendAnswerService service = new SendAnswerService(dbHandler
					.getTeam().getTeamid(), currentquestion.getQuestionid(),
					dbHandler.getGame().getGameid(), objAnswer, getActivity());
			service.execute();
		}

		String finalQuesTime = (String) chrono.getText();

		Log.d("fared", "time stop" + finalQuesTime);

		if (cdt != null) {
			// stop the timer
			cdt.cancel();
		}

		trigger = new Trigger(getActivity(), currentquestion.getQuestionid());

		if (trigger.checkStart() == true) {

			// alert before sequence
			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
					getActivity());
			alertDialogBuilder.setTitle("Triger Sequence");

			alertDialogBuilder
					.setMessage(
							"you have triger a bonus level !! You want to enter this level ?")
					.setCancelable(false)
					.setNegativeButton("No",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method
									// stub

									dbHandler.setSequenceDone(trigger
											.currentSeq());

									FragmentTransaction transaction = getFragmentManager()
											.beginTransaction();

									if (nxtquest != null) {
										String type = nxtquest.getType();

										if (type.equals("Obj")) {
											MultipleChoiceFragment multiplechoiceFragment = new MultipleChoiceFragment();

											Bundle args = new Bundle();
											args.putInt("id", nxtquest.getId());
											multiplechoiceFragment
													.setArguments(args);

											transaction.replace(
													R.id.game_container,
													multiplechoiceFragment);
										} else if (type.equals("Subj")) {
											SubjectiveFragment subjectiveFragment = new SubjectiveFragment();

											Bundle args = new Bundle();
											args.putInt("id", nxtquest.getId());
											subjectiveFragment
													.setArguments(args);

											transaction.replace(
													R.id.game_container,
													subjectiveFragment);
										} else if (type.equals("Pic")) {
											CompareImageFragment compareImageFragment = new CompareImageFragment();

											Bundle args = new Bundle();
											args.putInt("id", nxtquest.getId());
											compareImageFragment
													.setArguments(args);

											transaction.replace(
													R.id.game_container,
													compareImageFragment);
										} else if (type.equals("Code")) {
											EnterCodeFragment enterCodeFragment = new EnterCodeFragment();

											Bundle args = new Bundle();
											args.putInt("id", nxtquest.getId());
											enterCodeFragment
													.setArguments(args);

											transaction.replace(
													R.id.game_container,
													enterCodeFragment);
										} else if (type.equals("Vid")) {
											VideoQuesFragment videoQuesFragment = new VideoQuesFragment();

											Bundle args = new Bundle();
											args.putInt("id", nxtquest.getId());
											videoQuesFragment
													.setArguments(args);

											transaction.replace(
													R.id.game_container,
													videoQuesFragment);
										} else if (type.equals("QR")) {

										}

										transaction.addToBackStack(null);
										transaction
												.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
										transaction.commit();
										getActivity().unregisterReceiver(mybroadcast);

									} else {
										Intent myIntent = new Intent(
												getActivity(),
												SummaryActivity.class);
										Log.d("fared", "open summary activity");
										getActivity().startActivity(myIntent);
										((GameActivity) getActivity())
												.setStopLocation();
										getActivity().unregisterReceiver(mybroadcast);
										getActivity().finish();
									}

								}
							})
					.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method
									// stub
									FragmentTransaction transaction = getFragmentManager()
											.beginTransaction();

									if (nxtquest != null) {
										String type = nxtquest.getType();

										if (type.equals("Obj")) {
											MultipleChoiceFragment multiplechoiceFragment = new MultipleChoiceFragment();

											Bundle args = new Bundle();
											args.putInt("id", nxtquest.getId());
											multiplechoiceFragment
													.setArguments(args);

											transaction.replace(
													R.id.game_container,
													multiplechoiceFragment);
										} else if (type.equals("Subj")) {
											SubjectiveFragment subjectiveFragment = new SubjectiveFragment();

											Bundle args = new Bundle();
											args.putInt("id", nxtquest.getId());
											subjectiveFragment
													.setArguments(args);

											transaction.replace(
													R.id.game_container,
													subjectiveFragment);
										} else if (type.equals("Pic")) {
											CompareImageFragment compareImageFragment = new CompareImageFragment();

											Bundle args = new Bundle();
											args.putInt("id", nxtquest.getId());
											compareImageFragment
													.setArguments(args);

											transaction.replace(
													R.id.game_container,
													compareImageFragment);
										} else if (type.equals("Code")) {
											EnterCodeFragment enterCodeFragment = new EnterCodeFragment();

											Bundle args = new Bundle();
											args.putInt("id", nxtquest.getId());
											enterCodeFragment
													.setArguments(args);

											transaction.replace(
													R.id.game_container,
													enterCodeFragment);
										} else if (type.equals("Vid")) {
											VideoQuesFragment videoQuesFragment = new VideoQuesFragment();

											Bundle args = new Bundle();
											args.putInt("id", nxtquest.getId());
											videoQuesFragment
													.setArguments(args);

											transaction.replace(
													R.id.game_container,
													videoQuesFragment);
										} else if (type.equals("QR")) {

										}

										transaction.addToBackStack(null);
										transaction
												.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
										transaction.commit();
										getActivity().unregisterReceiver(mybroadcast);

										Log.d("fared", "masuk triger  = "
												+ questid);
										Intent myIntent = new Intent(
												getActivity(),
												SequenceActivity.class);
										Log.d("fared", "open sequence activity");
										myIntent.putExtra("id",
												trigger.currentSeq());
										getActivity().startActivity(myIntent);

									} else {
										Intent myIntent = new Intent(
												getActivity(),
												SummaryActivity.class);
										Log.d("fared", "open summary activity");
										getActivity().startActivity(myIntent);
										((GameActivity) getActivity())
												.setStopLocation();
										getActivity().unregisterReceiver(mybroadcast);
										getActivity().finish();
									}

								}
							});
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();

			// Add the fragment to the 'fragment_container'
			// FrameLayout

		} else {
			FragmentTransaction transaction = getFragmentManager()
					.beginTransaction();

			if (nxtquest != null) {
				String type = nxtquest.getType();

				if (type.equals("Obj")) {
					MultipleChoiceFragment multiplechoiceFragment = new MultipleChoiceFragment();

					Bundle args = new Bundle();
					args.putInt("id", nxtquest.getId());
					multiplechoiceFragment.setArguments(args);

					transaction.replace(R.id.game_container,
							multiplechoiceFragment);
				} else if (type.equals("Subj")) {
					SubjectiveFragment subjectiveFragment = new SubjectiveFragment();

					Bundle args = new Bundle();
					args.putInt("id", nxtquest.getId());
					subjectiveFragment.setArguments(args);

					transaction
							.replace(R.id.game_container, subjectiveFragment);
				} else if (type.equals("Pic")) {
					CompareImageFragment compareImageFragment = new CompareImageFragment();

					Bundle args = new Bundle();
					args.putInt("id", nxtquest.getId());
					compareImageFragment.setArguments(args);

					transaction.replace(R.id.game_container,
							compareImageFragment);
				} else if (type.equals("Code")) {
					EnterCodeFragment enterCodeFragment = new EnterCodeFragment();

					Bundle args = new Bundle();
					args.putInt("id", nxtquest.getId());
					enterCodeFragment.setArguments(args);

					transaction.replace(R.id.game_container, enterCodeFragment);
				} else if (type.equals("Vid")) {
					VideoQuesFragment videoQuesFragment = new VideoQuesFragment();

					Bundle args = new Bundle();
					args.putInt("id", nxtquest.getId());
					videoQuesFragment.setArguments(args);

					transaction.replace(R.id.game_container, videoQuesFragment);
				} else if (type.equals("QR")) {

				}

				transaction.addToBackStack(null);
				transaction
						.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
				transaction.commit();
				getActivity().unregisterReceiver(mybroadcast);

			} else {
				Intent myIntent = new Intent(getActivity(),
						SummaryActivity.class);
				Log.d("fared", "open summary activity");
				getActivity().startActivity(myIntent);
				((GameActivity) getActivity()).setStopLocation();
				getActivity().unregisterReceiver(mybroadcast);
				getActivity().finish();
			}
		}
	}

	public static Bitmap loadBitmap(Context context, String picName) {
		Bitmap b = null;
		FileInputStream fis;
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = 1;
		try {
			fis = context.openFileInput(picName);
			b = BitmapFactory.decodeStream(fis, null, options);
			fis.close();

		} catch (FileNotFoundException e) {
			Log.d("Hadian", "file not found");
			e.printStackTrace();
		} catch (IOException e) {
			Log.d("Hadian", "io exception");
			e.printStackTrace();
		}
		return b;
	}

	public void skipAction() {
		// Send the answer to admin for the
		// checking
		if (isInternetPresent) {
			SendAnswerService service = new SendAnswerService(dbHandler
					.getTeam().getTeamid(), currentquestion.getQuestionid(),
					dbHandler.getGame().getGameid(), "Skip", getActivity());
			service.execute();
		}

		// Add skip in database
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss", Locale.US);
		Date date = new Date();

		dbHandler.addStopAnswer(currentquestion.getQuestionid(), "Skip",
				"Skip", dateFormat.format(date));

		String finalQuesTime = (String) chrono.getText();

		Log.d("fared", "time stop" + finalQuesTime);

		if (cdt != null) {
			// stop the timer
			cdt.cancel();
		}

		// Add the fragment to the
		// 'fragment_container' FrameLayout
		FragmentTransaction transaction = getFragmentManager()
				.beginTransaction();
		if (nxtquest != null) {
			String type = nxtquest.getType();

			if (type.equals("Obj")) {
				MultipleChoiceFragment multiplechoiceFragment = new MultipleChoiceFragment();

				Bundle args = new Bundle();
				args.putInt("id", nxtquest.getId());
				multiplechoiceFragment.setArguments(args);

				transaction
						.replace(R.id.game_container, multiplechoiceFragment);
			} else if (type.equals("Subj")) {
				SubjectiveFragment subjectiveFragment = new SubjectiveFragment();

				Bundle args = new Bundle();
				args.putInt("id", nxtquest.getId());
				subjectiveFragment.setArguments(args);

				transaction.replace(R.id.game_container, subjectiveFragment);
			} else if (type.equals("Pic")) {
				CompareImageFragment compareImageFragment = new CompareImageFragment();

				Bundle args = new Bundle();
				args.putInt("id", nxtquest.getId());
				compareImageFragment.setArguments(args);

				transaction.replace(R.id.game_container, compareImageFragment);
			} else if (type.equals("Code")) {
				EnterCodeFragment enterCodeFragment = new EnterCodeFragment();

				Bundle args = new Bundle();
				args.putInt("id", nxtquest.getId());
				enterCodeFragment.setArguments(args);

				transaction.replace(R.id.game_container, enterCodeFragment);
			} else if (type.equals("Vid")) {
				VideoQuesFragment videoQuesFragment = new VideoQuesFragment();

				Bundle args = new Bundle();
				args.putInt("id", nxtquest.getId());
				videoQuesFragment.setArguments(args);

				transaction.replace(R.id.game_container, videoQuesFragment);
			} else if (type.equals("QR")) {

			}

			transaction.addToBackStack(null);
			transaction
					.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
			transaction.commit();
			getActivity().unregisterReceiver(mybroadcast);

		} else {
			Intent myIntent = new Intent(getActivity(), SummaryActivity.class);
			Log.d("fared", "open summary activity");
			getActivity().startActivity(myIntent);
			((GameActivity) getActivity()).setStopLocation();
			getActivity().unregisterReceiver(mybroadcast);
			getActivity().finish();
		}
	}
	
	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		isRunning = false;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		isRunning = true;
		
		if(cdtStatus == 1){
			skipAction();
		}
	}

}
