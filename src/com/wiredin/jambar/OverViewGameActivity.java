package com.wiredin.jambar;

import java.io.IOException;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.wiredin.jambar.dataclass.DatabaseHandler;
import com.wiredin.jambar.dataclass.Game;
import com.wiredin.jambar.dataclass.Team;

public class OverViewGameActivity extends SherlockActivity {

	private Button strtGame;
	CircularImageView teamImg;
	Uri uriImgPath;
	String imgPath;
	BitmapFactory.Options bmOptions;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.game_overview);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		getSupportActionBar().hide();

		Typeface tf = Typeface.createFromAsset(getAssets(),
				"fonts/ProximaNova-Regular.otf");
		Typeface tf1 = Typeface.createFromAsset(getAssets(),
				"fonts/ProximaNova-Bold.otf");

		strtGame = (Button) findViewById(R.id.strtGame);
		strtGame.setTypeface(tf1);

		strtGame.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent myIntent = new Intent(OverViewGameActivity.this,
						GameActivity.class);
				Log.d("fared", "open game activity");
				OverViewGameActivity.this.startActivity(myIntent);
			}
		});
		TextView textTeam = (TextView) findViewById(R.id.team);
		textTeam.setTypeface(tf1);
		TextView teamName = (TextView) findViewById(R.id.teamName);
		teamName.setTypeface(tf);
		TextView gameName = (TextView) findViewById(R.id.gameName);
		gameName.setTypeface(tf);
		TextView gameText = (TextView) findViewById(R.id.game);
		gameText.setTypeface(tf1);
		TextView questionTotal = (TextView) findViewById(R.id.questtotal);
		questionTotal.setTypeface(tf1);
		TextView gameTime = (TextView) findViewById(R.id.time);
		gameTime.setTypeface(tf1);
		TextView textQues = (TextView) findViewById(R.id.quesText);
		textQues.setTypeface(tf);

		TextView qText = (TextView) findViewById(R.id.qText);
		qText.setTypeface(tf1);

//		TextView tText = (TextView) findViewById(R.id.t_text);
//		tText.setTypeface(tf1);

		teamImg = (CircularImageView) findViewById(R.id.teamImage);

		DatabaseHandler dbHandler = new DatabaseHandler(this, null, null, 2);
		Team team = dbHandler.getTeam();
		Game game = dbHandler.getGame();

		uriImgPath = Uri.parse(dbHandler.getTeamPicture(dbHandler.getTeam()
				.getTeamid()));
		
		imgPath = uriImgPath.toString();
		
		Log.d("fared", "image path string "+imgPath);
		
		ExifInterface exif = null;
		try {
			exif = new ExifInterface(imgPath);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String exifOrientation = exif
				.getAttribute(ExifInterface.TAG_ORIENTATION);
		Log.d("fared", "image uri " + imgPath);
		Log.d("fared", "orientation " + exifOrientation);

		int rotate = 0;

		if (exifOrientation.equals("6")) {
			rotate = 90;
			Log.d("fared", "rotate " + rotate);
		}

//		teamImg.setImageURI(uriImgPath);
//		teamImg.setRotation(rotate);
		setPic(imgPath,rotate);
		
		teamImg.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(OverViewGameActivity.this, ViewTeamImage.class);
				startActivity(intent);
			}
		});

		teamName.setText(team.getName());
		gameName.setText(game.getTitle());
		questionTotal.setText(game.getTotal());
		
		String time = dbHandler.getGame().getTime();
		String[] timearray = time.split("\\.");
		String ampm = null;
		
		if (Integer.parseInt(timearray[0])<=13) {
			ampm = "am";
		} else {
			timearray[0] = String.valueOf(Integer.parseInt(timearray[0]) - 12);
			ampm = "pm";
		}
		
		gameTime.setText(timearray[0]+"."+timearray[1]+" "+ampm);
		
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		//finish();
	}
	
	@SuppressLint("NewApi")
	private void setPic(String path, int rotate) {

		/* There isn't enough memory to open up more than a couple camera photos */
		/* So pre-scale the target bitmap into which the file is decoded */

		/* Get the size of the ImageView */
		int targetW = teamImg.getWidth();
		int targetH = teamImg.getHeight();

		Log.d("fared", "masuk setPic");

		/* Get the size of the image */
		bmOptions = new BitmapFactory.Options();
		bmOptions.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(path, bmOptions);
		int photoW = bmOptions.outWidth;
		int photoH = bmOptions.outHeight;
		Log.d("fared", "set bitmap");

		/* Figure out which way needs to be reduced less */
		int scaleFactor = 1;
		if ((targetW > 0) || (targetH > 0)) {
			scaleFactor = Math.min(photoW / targetW, photoH / targetH)+2;
		}

		/* Set bitmap options to scale the image decode target */
		bmOptions.inJustDecodeBounds = false;
		bmOptions.inSampleSize = scaleFactor;
		bmOptions.inPurgeable = true;

		/* Decode the JPEG file into a Bitmap */
		Bitmap bitmap = BitmapFactory.decodeFile(path, bmOptions);
		Log.d("fared", "bit map decode " + bitmap);

		/* Associate the Bitmap to the ImageView */
		teamImg.setImageBitmap(bitmap);
		teamImg.setRotation(rotate);
		teamImg.setVisibility(View.VISIBLE);
		
		bitmap = null;
	}

}
