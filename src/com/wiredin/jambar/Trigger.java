package com.wiredin.jambar;

import java.util.List;

import android.content.Context;
import android.util.Log;

import com.wiredin.jambar.dataclass.DatabaseHandler;
import com.wiredin.jambar.dataclass.Sequence;

public class Trigger {

	DatabaseHandler dbHandler;
	String questionid;
	GPSTracker gpsTracker;
	double latitude;
	double longitude;
	List<Sequence> allSeq;
	String currentSeqId;
	String questioncode = "";

	public Trigger(Context context, String questionid) {
		dbHandler = new DatabaseHandler(context, null, null, 2);
		gpsTracker = new GPSTracker(context);
		// get all sequence inside the game
		allSeq = dbHandler.getAllSequence();

		try {

			gpsTracker.getLocation();
			gpsTracker.updateGPSCoordinates();

			if (gpsTracker.canGetLocation()) {
				latitude = gpsTracker.getLatitude();
				longitude = gpsTracker.getLongitude();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		this.questionid = questionid;
	}

	public void setCode(String code) {
		questioncode = code;
	}

	public boolean checkStart() {

		Log.d("Hadian t", "Check trigger");

		boolean check1 = false;
		boolean check2 = false;
		boolean check3 = false;
		boolean check4 = false;

		boolean finalcheck = false;

		String seqid = null;

		if (allSeq != null) {
			for (Sequence seq : allSeq) {

				Log.d("Hadian t", "trigger:" + seq.getTitle());

				String reqStart = seq.getStart_req();
				// check if the sequence ade done or not
				if (!seq.getTitle().equals("Done")) {
					if (reqStart.equals("1") || reqStart.equals("3")
							|| reqStart.equals("5") || reqStart.equals("7")
							|| reqStart.equals("9") || reqStart.equals("11")
							|| reqStart.equals("13") || reqStart.equals("15")) {
						check1 = checkPointStart(seq.getStart_point());
					}
					if (reqStart.equals("2") || reqStart.equals("3")
							|| reqStart.equals("6") || reqStart.equals("7")
							|| reqStart.equals("10") || reqStart.equals("14")
							|| reqStart.equals("15")) {
						check2 = checkCodeStart(seq.getStart_code());
					}
					if (reqStart.equals("4") || reqStart.equals("5")
							|| reqStart.equals("6") || reqStart.equals("7")
							|| reqStart.equals("12") || reqStart.equals("14")
							|| reqStart.equals("15")) {
						check3 = checkLocationStart(seq.getStart_lat(),
								seq.getStart_long(), seq.getStart_radius());
					}
					if (reqStart.equals("8") || reqStart.equals("9")
							|| reqStart.equals("10") || reqStart.equals("11")
							|| reqStart.equals("12") || reqStart.equals("13")
							|| reqStart.equals("14") || reqStart.equals("15")) {
						check4 = checkQuestionStart(seq.getStart_questionid(),
								seq.getStart_questiontime());
					}
				}
				seqid = seq.getSeqid();

				if (check1 || check2 || check3 || check4) {
					currentSeqId = seqid;
					if (dbHandler.getSeqQuestionBySeqId(seqid) != null) {
						finalcheck = true;
						break;
					}
				}
			}

		}

		return finalcheck;

	}

	public String currentSeq() {
		return currentSeqId;
	}

	// ######################

	public boolean checkPointStart(String point) {

		int currentpoint = Integer.parseInt(dbHandler.getSummaryPoint());

		Log.d("Hadian t",
				"Point: " + point + " current: " + dbHandler.getSummaryPoint());

		if (Integer.parseInt(point) <= currentpoint) {
			return true;
		} else {
			return false;
		}

	}

	public boolean checkCodeStart(String code) {

		if (questioncode.equals(code)) {
			return true;
		} else {
			return false;
		}

	}

	public boolean checkLocationStart(String lat, String lon, String radius) {

		double theta = longitude - Double.parseDouble(lon);
		double dist = Math.sin(deg2rad(latitude))
				* Math.sin(deg2rad(Double.parseDouble(lat)))
				+ Math.cos(deg2rad(latitude))
				* Math.cos(deg2rad(Double.parseDouble(lat)))
				* Math.cos(deg2rad(theta));
		dist = Math.acos(dist);
		dist = rad2deg(dist);
		dist = dist * 60 * 1.1515;

		dist = dist * 1609.34;

		Log.d("Hadian t", "distance : " + dist + " m");

		if (dist < Double.parseDouble(radius)) {
			return true;
		} else {
			return false;
		}

	}

	public boolean checkQuestionStart(String questionid, String questiontime) {
		if (this.questionid == questionid) {
			return true;
		} else {
			return false;
		}
	}

	// ######################

	private double deg2rad(double deg) {
		return (deg * Math.PI / 180.0);
	}

	private double rad2deg(double rad) {
		return (rad * 180 / Math.PI);
	}

}
