package com.wiredin.jambar;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
//import android.widget.ImageView;
import android.widget.TextView;

public class AnswerArrayAdapter extends ArrayAdapter<String> {

	private final Activity context;
	private final String[] text;
	private final String[] letter;
	
	int height;
	Typeface tf;
	Typeface tf1;
	
//	public static final Integer[] images = { R.drawable.aobj,
//        R.drawable.bobj, R.drawable.cobj, R.drawable.dobj, R.drawable.dobj };

	public AnswerArrayAdapter(Activity context, String[] text, String[] letter) {
		super(context, R.layout.list_answer, text);
		this.context = context;
		this.text = text;
		this.letter = letter;
		
		tf = Typeface.createFromAsset(context.getAssets(), "fonts/ProximaNova-Regular.otf");
        tf1 = Typeface.createFromAsset(context.getAssets(), "fonts/ProximaNova-Bold.otf");
		
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View rowView = convertView;
		
		if (rowView == null) {
			LayoutInflater inflater = context.getLayoutInflater();
			rowView = inflater.inflate(R.layout.list_answer, null);
			// ViewHolder viewHolder = new ViewHolder();
			// viewHolder.text = (TextView)
			// rowView.findViewById(R.id.secondLine);
			// viewHolder.image = (ImageView) rowView
			// .findViewById(R.id.shopicon);
			// rowView.setTag(viewHolder);
		}

		TextView firstLine = (TextView) rowView.findViewById(R.id.text_answer);
		firstLine.setText(text[position]);
		firstLine.setTypeface(tf);
		//ImageView answerSymbol = (ImageView) rowView.findViewById(R.id.image_answer);
		TextView letterView = (TextView) rowView.findViewById(R.id.letter_answer);
		letterView.setText(letter[position]);
		
//		Drawable icon = context.getResources().getDrawable(images[position]);
		
//		answerSymbol.setImageDrawable(icon);

		return rowView;
	}

}