package com.wiredin.jambar;

import java.util.List;

import android.content.Context;
import android.util.Log;

import com.wiredin.jambar.dataclass.DatabaseHandler;
import com.wiredin.jambar.dataclass.Sequence;

public class TriggerOut {

	DatabaseHandler dbHandler;
	String questionid;
	GPSTracker gpsTracker;
	double latitude;
	double longitude;
	Sequence seq;
	String currentSeqId;
	String questioncode = "";

	public TriggerOut(Context context, String seqid, String questionid) {
		dbHandler = new DatabaseHandler(context, null, null, 2);
		gpsTracker = new GPSTracker(context);
		// get all sequence inside the game

		seq = dbHandler.getSequenceBySeqid(seqid);

		try {

			gpsTracker.getLocation();
			gpsTracker.updateGPSCoordinates();

			if (gpsTracker.canGetLocation()) {
				latitude = gpsTracker.getLatitude();
				longitude = gpsTracker.getLongitude();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		this.questionid = questionid;
	}

	public void setCode(String code) {
		questioncode = code;
	}

	public boolean checkStop() {

		Log.d("Hadian t", "Check trigger");

		boolean check1 = false;
		boolean check2 = false;
		boolean check3 = false;
		boolean check4 = false;

		boolean finalcheck = false;

		String seqid = null;

		Log.d("Hadian t", "trigger:" + seq.getTitle());

		String reqStop = seq.getStart_req();
		// check if the sequence ade done or not
		if (!seq.getTitle().equals("Done")) {
			if (reqStop.equals("1") || reqStop.equals("3")
					|| reqStop.equals("5") || reqStop.equals("7")
					|| reqStop.equals("9") || reqStop.equals("11")
					|| reqStop.equals("13") || reqStop.equals("15")) {
				check1 = checkPointStop(seq.getStart_point());
			}
			if (reqStop.equals("2") || reqStop.equals("3")
					|| reqStop.equals("6") || reqStop.equals("7")
					|| reqStop.equals("10") || reqStop.equals("14")
					|| reqStop.equals("15")) {
				check2 = checkCodeStop(seq.getStart_code());
			}
			if (reqStop.equals("4") || reqStop.equals("5")
					|| reqStop.equals("6") || reqStop.equals("7")
					|| reqStop.equals("12") || reqStop.equals("14")
					|| reqStop.equals("15")) {
				check3 = checkLocationStop(seq.getStart_lat(),
						seq.getStart_long(), seq.getStart_radius());
			}
			if (reqStop.equals("8") || reqStop.equals("9")
					|| reqStop.equals("10") || reqStop.equals("11")
					|| reqStop.equals("12") || reqStop.equals("13")
					|| reqStop.equals("14") || reqStop.equals("15")) {
				check4 = checkQuestionStop(seq.getStart_questionid(),
						seq.getStart_questiontime());
			}
		}
		seqid = seq.getSeqid();

		if (check1 || check2 || check3 || check4) {
			currentSeqId = seqid;
			if (dbHandler.getSeqQuestionBySeqId(seqid) != null) {
				finalcheck = true;
			}
		}

		return finalcheck;

	}


	public String currentSeq() {
		return currentSeqId;
	}

	// ######################

	public boolean checkPointStop(String point) {

		Log.d("Hadian t", "Point current: " + dbHandler.getSummaryPoint());
		int currentpoint = Integer.parseInt(dbHandler.getSummaryPoint());

		if (Integer.parseInt(point) <= currentpoint) {
			return true;
		} else {
			return false;
		}

	}

	public boolean checkCodeStop(String code) {

		if ("code".equals(code)) {
			return true;
		} else {
			return false;
		}

	}

	public boolean checkLocationStop(String lat, String lon, String radius) {

		double theta = longitude - Double.parseDouble(lon);
		double dist = Math.sin(deg2rad(latitude))
				* Math.sin(deg2rad(Double.parseDouble(lat)))
				+ Math.cos(deg2rad(latitude))
				* Math.cos(deg2rad(Double.parseDouble(lat)))
				* Math.cos(deg2rad(theta));
		dist = Math.acos(dist);
		dist = rad2deg(dist);
		dist = dist * 60 * 1.1515;

		dist = dist * 1609.34;

		Log.d("Hadian t", "distance : " + dist + " m");

		if (dist < Double.parseDouble(radius)) {
			return true;
		} else {
			return false;
		}

	}

	public boolean checkQuestionStop(String questionid, String questiontime) {
		if (this.questionid == questionid) {
			return true;
		} else {
			return false;
		}
	}

	// ######################

	private double deg2rad(double deg) {
		return (deg * Math.PI / 180.0);
	}

	private double rad2deg(double rad) {
		return (rad * 180 / Math.PI);
	}

}
