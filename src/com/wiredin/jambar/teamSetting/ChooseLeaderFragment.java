package com.wiredin.jambar.teamSetting;

import java.util.List;

import com.wiredin.jambar.*;
import com.wiredin.jambar.dataclass.DatabaseHandler;
import com.wiredin.jambar.dataclass.Member;
import com.wiredin.jambar.services.SetLeaderService;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class ChooseLeaderFragment extends ListFragment {

	public static final String ARG_SECTION_NUMBER = "section_number";
	String[] listMembers;
	MemberArrayAdapter listAdapter;
	ListView listView;
	String selectedLeader;
	String leaderid;
	DatabaseHandler dbHandler;
	String memberId[];
	int gposition;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.team_fragment, container,
				false);

		Typeface tf = Typeface.createFromAsset(getActivity().getAssets(),
				"fonts/ProximaNova-Regular.otf");
//		Typeface tf1 = Typeface.createFromAsset(getActivity().getAssets(),
//				"fonts/ProximaNova-Bold.otf");

		//listMembers = getResources().getStringArray(R.array.list_members);

		TextView text = (TextView) rootView.findViewById(R.id.leader);

		text.setGravity(Gravity.CENTER);
		text.setTypeface(tf);
		Button nxtBtn = (Button) rootView.findViewById(R.id.next);

		nxtBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				dbHandler = new DatabaseHandler(getActivity(), null, null, 2);

				SetLeaderService service = new SetLeaderService(dbHandler
						.getTeam().getTeamid(), leaderid, getActivity());

				service.execute();

				// TODO Auto-generated method stub
				TeamPhotoFragment teamPhotoFragment = new TeamPhotoFragment();

				// Add the fragment to the 'fragment_container' FrameLayout
				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				transaction.setCustomAnimations(R.anim.slide_in_right,
						R.anim.slide_out_left);
				transaction.replace(R.id.teamSettingFrame, teamPhotoFragment);
				transaction.addToBackStack(null);
				// transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
				transaction.commit();
			}
		});

		((TeamSettingActivity) getActivity()).setCurrentFragment("Leader");

		return rootView;

	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		Log.d("fared", "masuk onactivitycreate "+savedInstanceState);

		dbHandler = new DatabaseHandler(getActivity(), null, null, 2);
		List<Member> members = dbHandler.getAllMember();

		if (members != null) {
			String[] memberName = new String[members.size()];
			memberId = new String[members.size()];

			int counter = 0;
			for (Member member : members) {
				memberName[counter] = member.getName();
				memberId[counter] = member.getMemberid();
				counter++;
			}

			listAdapter = new MemberArrayAdapter(getActivity(), memberName);
			setListAdapter(listAdapter);
		}

		listView = this.getListView();
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view,
					int position, long arg3) {
				// TODO Auto-generated method stub
				//view.setSelected(true);
				selectedLeader = (String) (listView.getItemAtPosition(position)
						.toString());
				Log.d("fared", "Leader yang dipilih " + selectedLeader +" "
						+ memberId[position]);
				leaderid = memberId[position];

				gposition = position;
				Log.d("fared", "gpostion : " + position);

				ImageView selectedimage;

				View newView;

				for (int i = 0; i < listView.getCount(); i++) {
					newView = listView.getChildAt(i);
					selectedimage = (ImageView) newView
							.findViewById(R.id.selectedImage);
					if (position == i) {
						selectedimage.setVisibility(View.VISIBLE);
					} else {
						selectedimage.setVisibility(View.INVISIBLE);
					}
				}

			}

		});
		
		

		if (savedInstanceState != null) {
			// Restore last state for checked position.
			Log.d("fared", "check save instante");
			gposition = savedInstanceState.getInt("curChoice", gposition);
			String[] values = savedInstanceState.getStringArray("myKey");
	        if (values != null) {
	        	Log.d("fared", "value list not null");
	        	listAdapter = new MemberArrayAdapter(getActivity(), values);
	        }
		}

	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt("curChoice", gposition);
		Log.d("fared", "save instance outstate : " +outState);
		
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		listView.setSelection(gposition);
		Log.d("fared", "gposition onresume : " +gposition);
		
		
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		Log.d("fared", "onpuase");
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		Log.d("fared", "onStop");
	}

}
