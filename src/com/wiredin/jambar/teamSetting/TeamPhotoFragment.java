package com.wiredin.jambar.teamSetting;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.wiredin.jambar.R;
import com.wiredin.jambar.TeamSettingActivity;
import com.wiredin.jambar.dataclass.DatabaseHandler;


public class TeamPhotoFragment extends Fragment {

	private static final int ACTION_TAKE_PHOTO = 1888;

	private static final String BITMAP_STORAGE_KEY = "viewbitmap";
	private static final String IMAGEVIEW_VISIBILITY_STORAGE_KEY = "imageviewvisibility";
//	private ImageView mImageView;
	private Bitmap mImageBitmap;
	
	int rotate;

	String picPath = null;

	private String mCurrentPhotoPath;

	private static final String JPEG_FILE_PREFIX = "IMG_";
	private static final String JPEG_FILE_SUFFIX = ".jpg";

	private AlbumStorageDirFactory mAlbumStorageDirFactory = null;

	public static final String ARG_SECTION_NUMBER = "section_number";

	private static final int GALLERY_REQUEST = 1889;
	public static final int MEDIA_TYPE_IMAGE = 1;

	private ProgressDialog dialog = null;
	String selectedImagePath;
	Uri imageUri;
	ImageView imagePreview;
	Button photoBtn;
	Button next;
	Button previous;
	String upLoadServerUri = null;
	private int serverResponseCode = 0;
	DatabaseHandler dBhandler;
	BitmapFactory.Options bmOptions;

	private String getAlbumName() {
		return getString(R.string.album_name);
	}

	private File getAlbumDir() {
		File storageDir = null;

		if (Environment.MEDIA_MOUNTED.equals(Environment
				.getExternalStorageState())) {

			storageDir = mAlbumStorageDirFactory
					.getAlbumStorageDir(getAlbumName());

			if (storageDir != null) {
				if (!storageDir.mkdirs()) {
					if (!storageDir.exists()) {
						Log.d("CameraSample", "failed to create directory");
						return null;
					}
				}
			}

		} else {
			Log.v(getString(R.string.app_name),
					"External storage is not mounted READ/WRITE.");
		}

		return storageDir;
	}

	@SuppressLint("SimpleDateFormat")
	private File createImageFile() throws IOException {
		// Create an image file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
				.format(new Date());
		String imageFileName = JPEG_FILE_PREFIX + timeStamp + "_";
		File albumF = getAlbumDir();
		File imageF = File.createTempFile(imageFileName, JPEG_FILE_SUFFIX,
				albumF);
		return imageF;
	}

	private File setUpPhotoFile() throws IOException {

		File f = createImageFile();
		mCurrentPhotoPath = f.getAbsolutePath();

		return f;
	}

	@SuppressLint("NewApi")
	private void setPic(String path, int rotate) {

		/* There isn't enough memory to open up more than a couple camera photos */
		/* So pre-scale the target bitmap into which the file is decoded */

		// dBhandler = new DatabaseHandler(getActivity(),null, null, 1);
		// String picPath =
		// dBhandler.getTeamPicture(dBhandler.getTeam().getTeamid());
		//
		// Log.d("Fared", "dpt dari db picture " +picPath);

		/* Get the size of the ImageView */
		int targetW = imagePreview.getWidth();
		int targetH = imagePreview.getHeight();

		Log.d("fared", "masuk setPic");

		/* Get the size of the image */
		bmOptions = new BitmapFactory.Options();
		bmOptions.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(path, bmOptions);
		int photoW = bmOptions.outWidth;
		int photoH = bmOptions.outHeight;
		Log.d("fared", "set bitmap");

		/* Figure out which way needs to be reduced less */
		int scaleFactor = 1;
		if ((targetW > 0) || (targetH > 0)) {
			scaleFactor = Math.min(photoW / targetW, photoH / targetH)+2;
			Log.d("fared", "scaleFactor= "+scaleFactor);
		}

		/* Set bitmap options to scale the image decode target */
		bmOptions.inJustDecodeBounds = false;
		bmOptions.inSampleSize = scaleFactor;
		bmOptions.inPurgeable = true;

		/* Decode the JPEG file into a Bitmap */
		Bitmap bitmap = BitmapFactory.decodeFile(path, bmOptions);
		Log.d("fared", "bit map decode " + bitmap);
		
		if (bitmap == null){
			Log.d("fared", "running picasso: "+getActivity().getResources().getString(
					R.string.imageurlteam)
					+ picPath);
			Picasso.with(getActivity())
			.load(getActivity().getResources().getString(
					R.string.imageurlteam)
					+ picPath).resize(50, 50)
			.centerCrop().into(imagePreview);
			
			imagePreview.setVisibility(View.VISIBLE);
		} else {
			/* Associate the Bitmap to the ImageView */
			imagePreview.setImageBitmap(bitmap);
			imagePreview.setRotation(rotate);
			imagePreview.setVisibility(View.VISIBLE);
		}
		
		bitmap = null;
	}

	private void galleryAddPic() {
		Intent mediaScanIntent = new Intent(
				"android.intent.action.MEDIA_SCANNER_SCAN_FILE");
		File f = new File(mCurrentPhotoPath);
		imageUri = Uri.fromFile(f);
		mediaScanIntent.setData(imageUri);
		getActivity().sendBroadcast(mediaScanIntent);
	}

	public String getPath(Uri uri) {
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = getActivity().getContentResolver().query(uri,
				projection, null, null, null);
		int column_index = cursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}

	private void dispatchTakePictureIntent(int actionCode) {

		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		switch (actionCode) {
		case ACTION_TAKE_PHOTO:
			File f = null;

			try {
				f = setUpPhotoFile();
				mCurrentPhotoPath = f.getAbsolutePath();
				takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
						Uri.fromFile(f));
			} catch (IOException e) {
				e.printStackTrace();
				f = null;
				mCurrentPhotoPath = null;
			}
			break;

		default:
			break;
		} // switch

		startActivityForResult(takePictureIntent, actionCode);
	}

	private void handleBigCameraPhoto() throws IOException {

		if (mCurrentPhotoPath != null) {

			selectedImagePath = mCurrentPhotoPath;
			ExifInterface exif = new ExifInterface(selectedImagePath); // Since
																		// API
																		// Level
																		// 5
			String exifOrientation = exif
					.getAttribute(ExifInterface.TAG_ORIENTATION);
			Log.d("fared", "image uri " + selectedImagePath);
			Log.d("fared", "orientation " + exifOrientation);

			rotate = 0;

			if (exifOrientation.equals("6")) {
				rotate = 90;
				Log.d("fared", "rotate " + rotate);
			}

			setPic(mCurrentPhotoPath, rotate);
			galleryAddPic();

			mCurrentPhotoPath = null;
		}

	}

	Button.OnClickListener mTakePicOnClickListener = new Button.OnClickListener() {
		@Override
		public void onClick(View v) {
			dispatchTakePictureIntent(ACTION_TAKE_PHOTO);
		}
	};

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		picPath = dBhandler.getTeamPicture(dBhandler.getTeam().getTeamid());

		Log.d("Fared", "dpt dari db picture " + picPath);
		
		if (picPath != null){
			if (!picPath.equals("")) {
				Log.d("fared", "pic Path tak null " + picPath +" "+rotate);
				setPic(picPath, rotate);
			} else {
				Log.d("fared", "pic Path null " + picPath);
			}
		}

	}
	
	public static Bitmap getBitmapFromURL(String src) {
	    try {
	        URL url = new URL(src);
	        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
	        connection.setDoInput(true);
	        connection.connect();
	        InputStream input = connection.getInputStream();
	        Bitmap myBitmap = BitmapFactory.decodeStream(input);
	        Log.d("Hadian", "Mendownload: " + src);
	        return myBitmap;
	    } catch (IOException e) {
	        e.printStackTrace();
	        Log.d("Hadian", "Null: " + src);
	        return null;
	    }
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.team_photo, container, false);

		Typeface tf = Typeface.createFromAsset(getActivity().getAssets(),
				"fonts/ProximaNova-Regular.otf");
//		Typeface tf1 = Typeface.createFromAsset(getActivity().getAssets(),
//				"fonts/ProximaNova-Bold.otf");

		TextView text = (TextView) rootView.findViewById(R.id.takePhoto);
		text.setTypeface(tf);
		text.setGravity(Gravity.CENTER);

		dBhandler = new DatabaseHandler(getActivity(), null, null, 1);

		photoBtn = (Button) rootView.findViewById(R.id.photoBtn);
		imagePreview = (ImageView) rootView.findViewById(R.id.teamPhoto);
		next = (Button) rootView.findViewById(R.id.next);
		previous = (Button) rootView.findViewById(R.id.prev);

		next.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (selectedImagePath != null) {
					Log.d("fared", "ade gamba");

					dialog = new ProgressDialog(getActivity());
					dialog.setTitle("Jambar"); // title
					dialog.setMessage("Upload image.."); // message
					dialog.setCancelable(false);

					GameRulesFragment gameRulesFragment = new GameRulesFragment();

					// Add the fragment to the 'fragment_container' FrameLayout
					FragmentTransaction transaction = getFragmentManager()
							.beginTransaction();
					transaction.setCustomAnimations(R.anim.slide_in_right,
							R.anim.slide_out_left);
					transaction.replace(R.id.teamSettingFrame,
							gameRulesFragment);
					transaction.addToBackStack(null);
					// transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
					transaction.commit();

					new Thread(new Runnable() {
						public void run() {

							uploadFile(selectedImagePath);

						}
					}).start();
				} else {
					Log.d("fared", "takde gamba");
					GameRulesFragment gameRulesFragment = new GameRulesFragment();

					// Add the fragment to the 'fragment_container' FrameLayout
					FragmentTransaction transaction = getFragmentManager()
							.beginTransaction();
					transaction.setCustomAnimations(R.anim.slide_in_right,
							R.anim.slide_out_left);
					transaction.replace(R.id.teamSettingFrame,
							gameRulesFragment);
					transaction.addToBackStack(null);
					// transaction
					// .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
					transaction.commit();
				}

			}

		});

		previous.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getFragmentManager().popBackStack();
			}
		});

		((TeamSettingActivity) getActivity()).setCurrentFragment("Photo");

		return rootView;
	}

	@SuppressWarnings("static-access")
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		Log.d("fared", "result ade" + imageUri);
		getActivity();
		if (requestCode == ACTION_TAKE_PHOTO
				&& resultCode == Activity.RESULT_OK) {
			Log.d("fared", "choose photo from camera");
			try {
				handleBigCameraPhoto();
				dBhandler = new DatabaseHandler(getActivity(), null, null, 1);
				dBhandler.updateTeamPhoto(dBhandler.getTeam().getTeamid(),
						selectedImagePath);
				Log.d("fared", "masuk dlm db " + selectedImagePath);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if (requestCode == GALLERY_REQUEST
				&& resultCode == getActivity().RESULT_OK && data != null) {
			Log.d("fared", "choose photo from gallery");
			Uri selectedImageUri = data.getData();
			selectedImagePath = getPath(selectedImageUri);
			imagePreview.setImageURI(selectedImageUri);
		}

	}

	public static boolean isIntentAvailable(Context context, String action) {
		final PackageManager packageManager = context.getPackageManager();
		final Intent intent = new Intent(action);
		List<ResolveInfo> list = packageManager.queryIntentActivities(intent,
				PackageManager.MATCH_DEFAULT_ONLY);
		return list.size() > 0;
	}

	public int uploadFile(String sourceFileUri) {

		String fileName = sourceFileUri;

		HttpURLConnection conn = null;
		DataOutputStream dos = null;
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary = "*****";
		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1 * 1024 * 1024;
		File sourceFile = new File(sourceFileUri);

		if (!sourceFile.isFile()) {

			dialog = ProgressDialog.show(getActivity(), "",
					"Uploading file...", true);
			dialog.dismiss();

			Log.e("uploadFile", "Source File not exist :" + selectedImagePath);

			return 0;

		} else {
			try {

				// open a URL connection to the Servlet
				FileInputStream fileInputStream = new FileInputStream(
						sourceFile);
				URL url = new URL(upLoadServerUri);

				// Open a HTTP connection to the URL
				conn = (HttpURLConnection) url.openConnection();
				conn.setDoInput(true); // Allow Inputs
				conn.setDoOutput(true); // Allow Outputs
				conn.setUseCaches(false); // Don't use a Cached Copy
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Connection", "Keep-Alive");
				conn.setRequestProperty("ENCTYPE", "multipart/form-data");
				conn.setRequestProperty("Content-Type",
						"multipart/form-data;boundary=" + boundary);
				conn.setRequestProperty("uploaded_file", fileName);

				dos = new DataOutputStream(conn.getOutputStream());

				dos.writeBytes(twoHyphens + boundary + lineEnd);
				dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\""
						+ fileName + "\"" + lineEnd);

				dos.writeBytes(lineEnd);

				// create a buffer of maximum size
				bytesAvailable = fileInputStream.available();

				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				buffer = new byte[bufferSize];

				// read file and write it into form...
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);

				while (bytesRead > 0) {

					dos.write(buffer, 0, bufferSize);
					bytesAvailable = fileInputStream.available();
					bufferSize = Math.min(bytesAvailable, maxBufferSize);
					bytesRead = fileInputStream.read(buffer, 0, bufferSize);

				}

				// send multipart form data necesssary after file data...
				dos.writeBytes(lineEnd);
				dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

				// Responses from the server (code and message)
				serverResponseCode = conn.getResponseCode();
				String serverResponseMessage = conn.getResponseMessage();

				Log.i("uploadFile", "HTTP Response is : "
						+ serverResponseMessage + ": " + serverResponseCode);

				if (serverResponseCode == 200) {
					Log.d("fared", "Upload complete");

					dialog.dismiss();

					// runOnUiThread(new Runnable() {
					// public void run() {
					// String msg =
					// "File Upload Completed.\n\n See uploaded file here : \n\n"
					// +" F:/wamp/wamp/www/uploads";
					// messageText.setText(msg);
					// Toast.makeText(MainActivity.this,
					// "File Upload Complete.", Toast.LENGTH_SHORT).show();
					// }
					// });
				}

				// close the streams //
				fileInputStream.close();
				dos.flush();
				dos.close();

			} catch (MalformedURLException ex) {

				ex.printStackTrace();

				// runOnUiThread(new Runnable() {
				// public void run() {
				// messageText.setText("MalformedURLException Exception : check script url.");
				// Toast.makeText(MainActivity.this, "MalformedURLException",
				// Toast.LENGTH_SHORT).show();
				// }
				// });

				Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
			} catch (Exception e) {

				e.printStackTrace();

				// runOnUiThread(new Runnable() {
				// public void run() {
				// messageText.setText("Got Exception : see logcat ");
				// Toast.makeText(MainActivity.this,
				// "Got Exception : see logcat ", Toast.LENGTH_SHORT).show();
				// }
				// });
				Log.e("Upload file to server Exception",
						"Exception : " + e.getMessage(), e);
			}

			return serverResponseCode;

		} // End else block
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		outState.putParcelable(BITMAP_STORAGE_KEY, mImageBitmap);
		outState.putBoolean(IMAGEVIEW_VISIBILITY_STORAGE_KEY,
				(mImageBitmap != null));
		super.onSaveInstanceState(outState);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

		//

		upLoadServerUri = "http://wiredin.my/jambar/services/UploadToServerTeam/"
				+ dBhandler.getTeam().getTeamid();
		this.photoBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						getActivity());

				alertDialogBuilder.setTitle("Photo Option");

				alertDialogBuilder
						.setMessage("Choose using camera or cancel")
						.setCancelable(false)
						.setNegativeButton("Camera",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										// TODO Auto-generated method stub

										// Intent intent = new
										// Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

										// start the image capture Intent
										dispatchTakePictureIntent(ACTION_TAKE_PHOTO);

									}
								})
						.setPositiveButton("cancel",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										// TODO Auto-generated method stub
										dialog.cancel();
									}
								});

				AlertDialog alertDialog = alertDialogBuilder.create();

				// show it
				alertDialog.show();
			}
		});
		mAlbumStorageDirFactory = new BaseAlbumDirFactory();

		// mImageBitmap = savedInstanceState.getParcelable(BITMAP_STORAGE_KEY);
		// mImageView.setImageBitmap(mImageBitmap);
		// mImageView
		// .setVisibility(savedInstanceState
		// .getBoolean(IMAGEVIEW_VISIBILITY_STORAGE_KEY) ? ImageView.VISIBLE
		// : ImageView.INVISIBLE);
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

}
