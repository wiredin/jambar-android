package com.wiredin.jambar.teamSetting;

import com.wiredin.jambar.*;
import com.wiredin.jambar.dataclass.DatabaseHandler;
import com.wiredin.jambar.services.GetGameService;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class GameRulesFragment extends Fragment {

	public static final String ARG_SECTION_NUMBER = "section_number";

	private Boolean isCheck = false; 
	private Button readyBtn;
	Button previous;
	ImageView agree;
	ImageView agreeClicked;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.rules_game, container, false);

		agree = (ImageView) rootView.findViewById(R.id.agree);
		agreeClicked = (ImageView) rootView.findViewById(R.id.agreeClick);

		Typeface tf = Typeface.createFromAsset(getActivity().getAssets(),
				"fonts/ProximaNova-Regular.otf");
		Typeface tf1 = Typeface.createFromAsset(getActivity().getAssets(),
				"fonts/ProximaNova-Bold.otf");

		TextView text = (TextView) rootView.findViewById(R.id.textView2);
		text.setTypeface(tf1);

		TextView rule = (TextView) rootView.findViewById(R.id.rule);
		rule.setTypeface(tf);

		readyBtn = (Button) rootView.findViewById(R.id.readyBtn);
		readyBtn.getBackground().setAlpha(128);
		readyBtn.setEnabled(false);
		readyBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				DatabaseHandler dbHandler = new DatabaseHandler(getActivity(),
						null, null, 2);

				GetGameService service = new GetGameService(dbHandler.getTeam()
						.getGame(), getActivity());
				service.execute();

			}
		});
		previous = (Button) rootView.findViewById(R.id.previous);

		previous.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getFragmentManager().popBackStack();
			}
		});
		
		((TeamSettingActivity)getActivity()).setCurrentFragment("Rules");

		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

		agreeClicked.setVisibility(View.GONE);
		
		agree.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				
				if (isCheck == false){
					agreeClicked.setVisibility(View.VISIBLE);
					isCheck = true;
					readyBtn.getBackground().setAlpha(255);
					readyBtn.setEnabled(true);
					
				} else {
					agreeClicked.setVisibility(View.GONE);
					isCheck = false;
					readyBtn.getBackground().setAlpha(128);
					readyBtn.setEnabled(false);
				}
			}
		});

	}

}
