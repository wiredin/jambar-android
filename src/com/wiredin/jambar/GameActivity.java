package com.wiredin.jambar;

import java.util.Calendar;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.wiredin.jambar.dataclass.DatabaseHandler;
import com.wiredin.jambar.dataclass.Game;
import com.wiredin.jambar.dataclass.Question2;
import com.wiredin.jambar.dataclass.Team;
import com.wiredin.jambar.gamefragment.CompareImageFragment;
import com.wiredin.jambar.gamefragment.EnterCodeFragment;
import com.wiredin.jambar.gamefragment.MultipleChoiceFragment;
import com.wiredin.jambar.gamefragment.SubjectiveFragment;
import com.wiredin.jambar.gamefragment.VideoQuesFragment;
import com.wiredin.jambar.services.SendLocationService;
import com.wiredin.jambar.services.StartGameService;

public class GameActivity extends SherlockFragmentActivity {

	ConnectionDetector cd;
	// flag for Internet connection status
	Boolean isInternetPresent = false;

	DatabaseHandler dbHandler;
	GPSTracker gpsTracker;
	TextView questionLeft;
	TextView timeRemain;

	CountDownTimer waitTimer;
	Thread timer;
	private Boolean stopLocationTracker = false;

	Team team;
	Game game;

	// private String lengthTime;
	private long lengthTimeMili = 0;
	private final long interval = 1000;

	public String formatTime(long millis) {
		String output = "00:00:00";
		long seconds = millis / 1000;
		long minutes = seconds / 60;
		long hour = minutes / 60;

		seconds = seconds % 60;
		minutes = minutes % 60;
		hour = hour % 24;

		String sec = String.valueOf(seconds);
		String min = String.valueOf(minutes);
		String hou = String.valueOf(hour);

		if (seconds < 10)
			sec = "0" + seconds;
		if (minutes < 10)
			min = "0" + minutes;

		output = hou + " : " + min + " : " + sec;
		return output;
	}

	public int setDuration() {
		Calendar timeStart = Calendar.getInstance();
		// Calendar endTime = Calendar.getInstance();

		int duration = 0;

		int sHour = timeStart.get(Calendar.HOUR_OF_DAY);
		int sMinute = timeStart.get(Calendar.MINUTE);
		
		String time = dbHandler.getGame().getTime();
		String[] timearray = time.split("\\.");
		
		int eMinutes = Integer.parseInt(timearray[1]);
		int eHour = Integer.parseInt(timearray[0]);

		eHour = ((eHour - sHour) * 60) + eMinutes;
		duration = eHour - sMinute;

		Log.d("fared", Integer.toString(duration));

		return duration;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.game_activity);
		getSupportActionBar().hide();

		dbHandler = new DatabaseHandler(this, null, null, 2);

		team = dbHandler.getTeam();
		game = dbHandler.getGame();
		
		// clear all summary and create new one
		dbHandler.deleteSummary();
		dbHandler.addSummary();

		// clear the answer database
		dbHandler.deleteAllAnswer();

		// check internet
		cd = new ConnectionDetector(this);
		isInternetPresent = cd.isConnectingToInternet();

		// this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		if (isInternetPresent) {
			// start game if this is first question
			StartGameService serviceStart = new StartGameService(dbHandler
					.getTeam().getTeamid(), this);
			serviceStart.execute();
		}

		// lengthTime = dbHandler.getGame().getTime();
		//
		lengthTimeMili = setDuration() * 60000;
		Log.d("fared", "time in mili " + lengthTimeMili);

		Typeface tf = Typeface.createFromAsset(getAssets(),
				"fonts/ProximaNova-Regular.otf");
		Typeface tf1 = Typeface.createFromAsset(getAssets(),
				"fonts/ProximaNova-Bold.otf");

		TextView textTeam = (TextView) findViewById(R.id.team);
		textTeam.setTypeface(tf1);
		TextView teamName = (TextView) findViewById(R.id.teamName);
		teamName.setTypeface(tf);
		teamName.setText(team.getName());
		TextView gameName = (TextView) findViewById(R.id.gameName);
		gameName.setTypeface(tf);
		gameName.setText(game.getTitle());
		TextView gameText = (TextView) findViewById(R.id.game);
		gameText.setTypeface(tf1);

		// countDownTimer = new GameCountDownTimer(startTime, interval);
		// countDownTimer.start();
		// Log.d("fared", "start timer");

		questionLeft = (TextView) this.findViewById(R.id.questionLeft);
		timeRemain = (TextView) this.findViewById(R.id.timeRemain);
		timeRemain.setTypeface(tf);

		if (findViewById(R.id.game_container) != null) {

			if (savedInstanceState != null) {
				return;
			}

			Question2 quest = dbHandler.getQuestionByTableId2(1);

			if (quest != null) {
				String type = quest.getType();

				if (type.equals("Obj")) {
					MultipleChoiceFragment multiplechoiceFragment = new MultipleChoiceFragment();

					Bundle args = new Bundle();
					args.putInt("id", 1);
					multiplechoiceFragment.setArguments(args);

					// Add the fragment to the 'fragment_container' FrameLayout
					getSupportFragmentManager().beginTransaction()
							.add(R.id.game_container, multiplechoiceFragment)
							.commit();
				} else if (type.equals("Subj")) {
					SubjectiveFragment subjectiveFragment = new SubjectiveFragment();

					Bundle args = new Bundle();
					args.putInt("id", 1);
					subjectiveFragment.setArguments(args);

					getSupportFragmentManager().beginTransaction()
							.add(R.id.game_container, subjectiveFragment)
							.commit();
				} else if (type.equals("Pic")) {
					CompareImageFragment compareImageFragment = new CompareImageFragment();

					Bundle args = new Bundle();
					args.putInt("id", 1);
					compareImageFragment.setArguments(args);

					getSupportFragmentManager().beginTransaction()
							.add(R.id.game_container, compareImageFragment)
							.commit();
				} else if (type.equals("Code")) {
					EnterCodeFragment enterCodeFragment = new EnterCodeFragment();

					Bundle args = new Bundle();
					args.putInt("id", 1);
					enterCodeFragment.setArguments(args);

					getSupportFragmentManager().beginTransaction()
							.add(R.id.game_container, enterCodeFragment)
							.commit();
				} else if (type.equals("Vid")) {
					VideoQuesFragment videoQuesFragment = new VideoQuesFragment();

					Bundle args = new Bundle();
					args.putInt("id", 1);
					videoQuesFragment.setArguments(args);

					getSupportFragmentManager().beginTransaction()
							.add(R.id.game_container, videoQuesFragment)
							.commit();
				} else if (type.equals("QR")) {

				}

			}

		}

		gpsTracker = new GPSTracker(GameActivity.this);

		timer = new Thread() {

			public void run() {
				// Looper.prepare();

				try {

					gpsTracker.getLocation();
					gpsTracker.updateGPSCoordinates();

					if (gpsTracker.canGetLocation()) {

						String latitude = String.valueOf(gpsTracker
								.getLatitude());
						String longitude = String.valueOf(gpsTracker
								.getLongitude());
						// Log.d("Hadian", "lat: " + latitude + "long: "
						// + longitude);

						if (isInternetPresent) {
							SendLocationService service = new SendLocationService(
									GameActivity.this, latitude, longitude);
							service.execute();
						}
					} else {
						// can't get location
						// GPS or Network is not enabled
						// Ask user to enable GPS/network in settings
						gpsTracker.showSettingsAlert();
					}
					sleep(3000);

				} catch (InterruptedException e) {
					e.printStackTrace();
				} finally {
					if (stopLocationTracker == false) {
						run();
					}
				}

			};
		};
		timer.start();

		waitTimer = new CountDownTimer(lengthTimeMili, interval) {
//			String timing;

			public void onTick(long millisUntilFinished) {

//				timing = formatTime(millisUntilFinished);
				// Log.d("fared", "timing = " + timing);
				timeRemain.setText("Time remaining : "
						+ formatTime(millisUntilFinished));
			}

			public void onFinish() {
				stopLocationTracker = true;

				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						GameActivity.this);

				alertDialogBuilder.setTitle("Game End");

				alertDialogBuilder
						.setMessage(
								"Game Have Ended.")
						.setCancelable(false)
						.setNegativeButton("OK",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										// TODO Auto-generated method
										// stub

										dialog.dismiss();
										Intent myIntent = new Intent(
												GameActivity.this,
												SummaryActivity.class);
										Log.d("fared", "open summary activity");
										GameActivity.this
												.startActivity(myIntent);
										GameActivity.this.finish();

									}
								});

				AlertDialog alertDialog = alertDialogBuilder.create();

				// show it
				alertDialog.show();

			}
		}.start();

	}


	@Override
	public void onBackPressed() {
	}

	public void changeQuesLeft(String text) {
		questionLeft.setText(text);
	}

	public void setStopLocation() {
		Log.d("Hadian", "stop tracking");
		stopLocationTracker = true;
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		waitTimer.cancel();
	}

}
