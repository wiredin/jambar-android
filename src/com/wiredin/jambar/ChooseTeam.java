package com.wiredin.jambar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.actionbarsherlock.app.SherlockActivity;
import com.wiredin.jambar.dataclass.DatabaseHandler;
import com.wiredin.jambar.dataclass.GameTeam;
import com.wiredin.jambar.services.GetGameTeamService;
import com.wiredin.jambar.services.GetTeamService;

import android.app.AlertDialog;
//import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ChooseTeam extends SherlockActivity {

	public Button enterBut;
	public EditText startCode;
	Typeface tf;

	// private ProgressDialog dialog = null;

	// flag for Internet connection status
	Boolean isInternetPresent = false;

	ConnectionDetector cd;

	List<GameTeam> listTeam;
	ArrayList<String> list;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.enter_team);

		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		// Do your operation
		// setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);

		// this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		// text view label
		TextView txtWel = (TextView) findViewById(R.id.teamtext);

		// Loading Font Face
		Typeface tf = Typeface.createFromAsset(getAssets(),
				"fonts/ProximaNova-Regular.otf");
		Typeface tf1 = Typeface.createFromAsset(getAssets(),
				"fonts/ProximaNova-Bold.otf");

		// Applying font
		txtWel.setTypeface(tf);

		getSupportActionBar().hide();

		cd = new ConnectionDetector(getApplicationContext());

		final ListView listview = (ListView) findViewById(R.id.listTeam);

		DatabaseHandler dbHandler = new DatabaseHandler(this, null, null, 1);

		listTeam = dbHandler.getAllGT();

		list = new ArrayList<String>();
		final ArrayList<String> codes = new ArrayList<String>();
		for (GameTeam team : listTeam) {
			list.add(team.getName());
			codes.add(team.getCode());
		}
		final StableArrayAdapter adapter = new StableArrayAdapter(this,list);
		listview.setAdapter(adapter);

		listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, final View view,
					int position, long id) {
				// final String item = (String)
				// parent.getItemAtPosition(position);
				Toast.makeText(ChooseTeam.this, codes.get(position),
						Toast.LENGTH_SHORT).show();
				// request team code
				isInternetPresent = cd.isConnectingToInternet();

				// check for Internet status
				if (isInternetPresent) {
					// Internet Connection is Present
					// make HTTP requests
					GetTeamService serviceFollowed = new GetTeamService(codes
							.get(position), ChooseTeam.this);
					serviceFollowed.execute();
				} else {
					// Internet connection is not present
					// Ask user to connect to Internet

					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
							ChooseTeam.this);

					alertDialogBuilder.setTitle("Network Status");

					alertDialogBuilder
							.setMessage("Please check your network status.")
							.setCancelable(false)
							.setNegativeButton("OK",
									new DialogInterface.OnClickListener() {

										@Override
										public void onClick(
												DialogInterface dialog,
												int which) {
											// TODO Auto-generated method
											// stub

											dialog.dismiss();

										}
									});

					AlertDialog alertDialog = alertDialogBuilder.create();

					// show it
					alertDialog.show();

				}
			}

		});

	}

	private class StableArrayAdapter extends ArrayAdapter<String> {

		HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();
		
		String[] text;

		public StableArrayAdapter(Context context,
				List<String> objects) {
			super(context,R.layout.list_member, objects);
			text = new String[objects.size()];
			for (int i = 0; i < objects.size(); ++i) {
				mIdMap.put(objects.get(i), i);
				text[i] = objects.get(i);
			}
			tf = Typeface.createFromAsset(context.getAssets(), "fonts/ProximaNova-Regular.otf");
			
		}
		
		

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			View rowView = convertView;
			
			if (rowView == null) {
				LayoutInflater inflater = getLayoutInflater();
				rowView = inflater.inflate(R.layout.list_member, null);
				// ViewHolder viewHolder = new ViewHolder();
				// viewHolder.text = (TextView)
				// rowView.findViewById(R.id.secondLine);
				// viewHolder.image = (ImageView) rowView
				// .findViewById(R.id.shopicon);
				// rowView.setTag(viewHolder);
			}

			TextView firstLine = (TextView) rowView.findViewById(R.id.membersName);
			firstLine.setText(text[position]);
			firstLine.setTypeface(tf);
			ImageView sideImage = (ImageView) rowView.findViewById(R.id.imageView1);
			sideImage.setVisibility(View.INVISIBLE);
			ImageView selectedimage = (ImageView) rowView.findViewById(R.id.selectedImage);
			selectedimage.setVisibility(View.INVISIBLE);

			return rowView;
		}



		@Override
		public long getItemId(int position) {
			String item = getItem(position);
			return mIdMap.get(item);
		}

		@Override
		public boolean hasStableIds() {
			return true;
		}

	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// TODO Auto-generated method stub
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
		return true;
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();

	}

}
